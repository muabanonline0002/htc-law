<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
    <div class="sidebar sidebar-left" style="min-height: 100%;">

        <!-- Logo Start -->
        <div class="logo-container">
            <a href="/"><h1>DWG</h1></a>
        </div>
        <!-- Logo End -->
        <div class="menu-container">
            <!-- User Profile Start -->
            <div class="sidebar-user-profile">
                <div class="avatar">
                    <img width="70" src="<?php echo Yii::app()->request->baseUrl; ?>/images/profile.jpg" alt="Jane Doe">
                </div>
                <div class="ul-icons">
                    <span class="user-info"><?php echo Yii::app()->user->name;?></span>
                    <ul class="icon-list">
                        <li><a href="<?php echo Yii::app()->createUrl('/user/logout');?>"><i class="fa fa-power-off"></i></a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('/user/profile/edit');?>"><i class="fa fa-cog"></i></a></li>
                        <li><a href="#"><i class="fa fa-comments"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- User Profile End -->
        <!-- Menu Start -->
        <?php //include 'menu_apps.php';?>
        <?php include 'menu_admin.php';?>
        </div>
    </div>
    <div class="top-bar">
        <div class="main-container">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-sm-6 hidden-xs">
                        <ul class="left-icons icon-list">
                            <li><a href="javascript:;" class="sidebar-collapse"><i class="fa fa-bars"></i></a></li>
                            <!-- <li>
                                <a href="/"><i class="fa fa-bell"></i></a>
                                <div class="notification-box">
                                    <div class="inner">
                                        <ul class="notification-list">
                                            <li class="warning">John added new task</li>
                                            <li class="info">Customer support called</li>
                                            <li class="error">Updated to version 2.0.7</li>
                                            <li class="success">Updated to version 2.0.2</li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="/">
                                    <i class="fa fa-envelope"></i>
                                    <span class="notification"></span>
                                </a>
                                <div class="notification-box">
                                    <div class="inner">
                                        <ul class="mail-list">
                                            <li class="starred">Subject example <span>1 hour ago</span></li>
                                            <li>Hey! <span>2 hours ago</span></li>
                                            <li>Welcome back! <span>7 days ago</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </li> -->
                            <li>
                                <div class="btn-group">
                                  <a type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-plus"></i></span>
                                  </a>
                                  <ul class="dropdown-menu">
                                    <li class="starred"><a href="<?php echo Yii::app()->createUrl('/news/news/create')?>"><i class="fa fa-plus-circle"></i>&nbsp;Thêm mới bài viết</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('/news/categories/create')?>"><i class="fa fa-plus-circle"></i>&nbsp;Thêm mới chủ đề</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('/news/pages/create')?>"><i class="fa fa-plus-circle"></i>&nbsp;Thêm mới trang</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('/shop/products/create')?>"><i class="fa fa-plus-circle"></i>&nbsp;Thêm mới sản phẩm</a></li>
                                  </ul>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="col-lg-6 col-sm-6 col-xs-12">
                        <ul class="right-icons icon-list">
                            <li><a href="<?php echo Yii::app()->createUrl('/site/flushCache');?>"><i class="fa fa-magic"></i></a></li>
                            <li><a href="<?php echo SITE_URL;?>" target="_blank"><i class="fa fa-paper-plane"></i></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="content" id="content-container">
        <div class="main-container">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Box Start -->
                        <div id="main-content" class="box">
                            <!-- Content Start -->
                            <?php echo $content;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->endContent(); ?>