<?php 
$module = Yii::app()->controller->module->id;
$controller = Yii::app()->controller->getId();
$menu2=$menu3=$menu4='';
if(in_array($module,array('news','files','menu','gallery','polls','blog','comment','widget','translate'))){
	$menu2='active';
}
if(in_array($module,array('shop'))){
	$menu3='active';
}
if(in_array($module,array('settings','srbac'))){
	$menu4='active';
}
?>
<ul class="main-menu">
	<li>
		<a href="<?php echo Yii::app()->createUrl('/dashboard');?>">
			<i class="fa fa-home"></i>
			<span>Dashboard</span>
		</a>
	</li>
	<li class="has-submenu <?php if($menu2=='active') echo 'active';?>">
		<a href="#" <?php if($menu2=='active') echo 'class="close-child"';?>>
			<i class="fa fa-database"></i>
			<span>Quản trị nội dung</span>
		</a>
		<ul class="submenu" <?php if($menu2=='active') echo 'style="display:block"';?>>
			<li class="<?php if($module=='news' && $controller=='news') echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/news/news/admin');?>">
					<i class="fa fa-newspaper-o"></i>
					<span>Quản lý bài viết</span>
				</a>
			</li>
			<li class="<?php if($module=='news' && $controller=='categories') echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/news/categories/admin');?>">
					<i class="fa fa-cube"></i>
					<span>Quản lý chủ đề</span></a>
			</li>
			<li class="<?php if($module=='files') echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/files/files/admin');?>"><i class="fa fa-files-o"></i><span>Quản lý File</span></a>
			</li>
			<li class="<?php if($module=='news' && $controller=='pages') echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/news/pages/admin');?>"><i class="fa fa-pencil-square-o"></i><span><?php echo Yii::t('main','Pages Manager')?></span></a>
			</li>
			<li class="<?php if($module=='menu') echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/menu/menus/admin');?>"><i class="fa fa-align-justify"></i><span><?php echo Yii::t('main','Menu Manager')?></span></a>
			</li>
			<li class="<?php if($module=='gallery') echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/gallery/galleryItems/admin');?>">
					<i class="fa fa-photo"></i>
					<span><?php echo Yii::t('main','Gallery Manager')?></span>
				</a>
			</li>
			<li class="<?php if($module=='polls') echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/polls/poll/admin');?>">
					<i class="fa fa-tasks"></i>
					<span><?php echo Yii::t('main','Polls Manager')?></span>
				</a>
			</li>
			<li class="<?php if($module=='blog') echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/blog/post/admin');?>">
					<i class="fa fa-sticky-note"></i>
					<span><?php echo Yii::t('main','Blog Manager')?></span>
				</a>
			</li>
			<li class="<?php if($module=='comment') echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/comment/manage/admin');?>">
					<i class="fa fa-comments"></i>
					<span><?php echo Yii::t('main','Comment Manager')?></span>
				</a>
			</li>
			<li class="<?php if($module=='widget') echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/widget/manage/admin');?>">
					<i class="fa fa-cubes"></i>
					<span><?php echo Yii::t('main','Widgets Manager')?></span>
				</a>
			</li>
			<li class="<?php if($module=='translate') echo 'active';?>">
				<a href="<?php echo Yii::app()->createUrl('/translate/filterTranslate/Filterlayout');?>">
					<i class="fa fa-globe"></i>
					<span><?php echo Yii::t('main','Languages Manager')?></span>
				</a>
			</li>
		</ul>
	</li>
	<li class="has-submenu">
		<a href="javascript:;">
			<i class="fa fa-shopping-basket"></i>
			<span><?php echo Yii::t('main','Shop Manager')?></span>
		</a>
		<ul class="submenu" <?php if($menu3=='active') echo 'style="display:block"';?>>
			<li class="<?php if($module=='shop' && $controller=='products') echo 'active';?>">
			<a href="<?php echo Yii::app()->createUrl('/shop/products/admin');?>">
				<i class="fa fa-globe"></i>&nbsp;<span><?php echo Yii::t('main','Products Manager')?></span>
			</a>
			</li>
			<li class="<?php if($module=='shop' && $controller=='category') echo 'active';?>">
			<a href="<?php echo Yii::app()->createUrl('/shop/category/admin');?>">
				<i class="fa fa-globe"></i>&nbsp;<span><?php echo Yii::t('main','Category Manager')?></span>
			</a>
			</li>
			<li class="<?php if($module=='shop' && $controller=='shopManufactor') echo 'active';?>">
			<a href="<?php echo Yii::app()->createUrl('/shop/shopManufactor/admin');?>">
				<i class="fa fa-globe"></i>&nbsp;<span><?php echo Yii::t('main','Manufactor Manager')?></span>
			</a>
			</li>
			<li class="<?php if($module=='shop' && $controller=='order') echo 'active';?>">
			<a href="<?php echo Yii::app()->createUrl('/shop/shopOrderInfo/admin');?>">
				<i class="fa fa-globe"></i>&nbsp;<span><?php echo Yii::t('main','Orders Manager')?></span>
			</a>
			</li>
		</ul>
	</li>
	<li class="has-submenu">
		<a href="<?php echo Yii::app()->createUrl('/settings');?>">
			<i class="fa fa-gears"></i>
			<span><?php echo Yii::t('main','Settings')?></span>
		</a>
		<ul class="submenu" <?php if($menu4=='active') echo 'style="display:block"';?>>
			<li class="<?php if($module =='settings') echo 'active';?>">
			<a href="<?php echo Yii::app()->createUrl('/settings/system');?>">
					<i class="fa fa-info-circle"></i>
					<span><?php echo Yii::t('main','General Setting')?></span>
				</a></li>
			<li class="<?php if($module =='srbac') echo 'active';?>">
			<a class="" href="<?php echo Yii::app()->createUrl('/srbac');?>">
					<i class="fa fa-key"></i>
					<span><?php echo Yii::t('main','Quyền truy cập')?></span>
				</a>
			</li>
		</ul>
	</li>
</ul>
<!--<ul class="menu-top-level">
	<li class="menu-section">
		<div class="menu-section-item">
		<ul class="apps-link">
			<li><a class="yt-valign" href="<?php /*echo Yii::app()->createUrl('/dashboard');*/?>"><i class="glyphicon glyphicon-home"></i>&nbsp;<?php /*echo Yii::t('main','Dashboard')*/?></a></li>
			<li><a class="yt-valign <?php /*if($module =='news' && $controller=='news') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/news/news/admin');*/?>"><i class="glyphicon glyphicon-list-alt"></i>&nbsp;<?php /*echo Yii::t('main','Articles Manager')*/?></a>
				<?php /*if($module =='news' && ($controller=='news' || $controller=='categories') || $module=='files'){*/?>
				<ul class="sub-menu">
					<li><a class="yt-valign <?php /*if($module =='news' && $controller=='categories') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/news/categories/admin');*/?>">+&nbsp;<?php /*echo Yii::t('main','Categories Manager')*/?></a></li>
					<li><a class="yt-valign <?php /*if($module =='files') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/files/files/admin');*/?>">+&nbsp;File Manager</a></li>
				</ul>
				<?php /*}*/?>
			</li>
			<li><a class="yt-valign <?php /*if($module =='news' && $controller=='pages') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/news/pages/admin');*/?>"><i class="glyphicon glyphicon-book icon-blue"></i>&nbsp;<?php /*echo Yii::t('main','Pages Manager')*/?></a></li>
			<li><a class="yt-valign <?php /*if($module=='media') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/media/manage');*/?>"><i class="glyphicon glyphicon-folder-open"></i>&nbsp;<?php /*echo Yii::t('main','Media Manager')*/?></a></li>
			<li><a class="yt-valign <?php /*if($module=='polls') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/polls/poll/admin');*/?>"><i class="glyphicon glyphicon-tasks"></i>&nbsp;<?php /*echo Yii::t('main','Polls Manager')*/?></a></li>
			<li><a class="yt-valign <?php /*if($module=='menu') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/menu/menus/admin');*/?>"><i class="glyphicon glyphicon-align-justify"></i>&nbsp;<?php /*echo Yii::t('main','Menus Manager')*/?></a></li>
			<li><a class="yt-valign <?php /*if($module=='gallery') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/gallery/galleryItems/admin');*/?>"><i class="glyphicon glyphicon-picture"></i>&nbsp;<?php /*echo Yii::t('main','Gallery Manager')*/?></a></li>
			<li><a class="yt-valign <?php /*if($module=='blog' && $controller=='post') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/blog/post/admin');*/?>"><i class="glyphicon glyphicon-pencil"></i>&nbsp;<?php /*echo Yii::t('main','Blog Manager')*/?></a>
				<?php /*if($module =='blog' ){*/?>
				<ul class="sub-menu">
					<li><a class="yt-valign <?php /*if($module =='blog' && $controller=='topic') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/blog/topic/admin');*/?>">+&nbsp;<?php /*echo Yii::t('main','Topic Manager')*/?></a></li>
				</ul>
				<?php /*}*/?>
			</li>
			<li><a class="yt-valign <?php /*if($module=='comment') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/comment/manage/admin');*/?>"><i class="glyphicon glyphicon-comment"></i>&nbsp;<?php /*echo Yii::t('main','Comment Manager')*/?></a></li>
			<li><a class="yt-valign <?php /*if($module=='widget') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/widget/manage/admin');*/?>"><i class="glyphicon glyphicon-th-large"></i>&nbsp;Wigets Manager</a></li>
			<li><a class="yt-valign <?php /*if($module=='translate') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/translate/filterTranslate/Filterlayout');*/?>"><i class="glyphicon glyphicon-globe"></i>&nbsp;<?php /*echo Yii::t('main','Languages Translate')*/?></a></li>
			<li><a class="yt-valign <?php /*if($module=='settings' && $controller=='default') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/settings');*/?>"><i class="glyphicon glyphicon-cog"></i>&nbsp;<?php /*echo Yii::t('main','Settings')*/?></a>
				<?php /*if($module=='settings'){*/?>
				<ul class="sub-menu">
					<li><a class="yt-valign <?php /*if($module =='settings' && $controller=='system') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/settings/system');*/?>"><?php /*echo Yii::t('main','General Setting')*/?></a></li>
				</ul>
				<?php /*}*/?>
			</li>
			<li><a class="yt-valign <?php /*if($module=='MogCategory') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/MogCategory/manager/admin');*/?>"><i class="glyphicon glyphicon-globe"></i>&nbsp;<?php /*echo Yii::t('main','TShirt Category')*/?></a></li>
			<li><a class="yt-valign <?php /*if($module=='MogTshirt') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/MogTshirt/manager/admin');*/?>"><i class="glyphicon glyphicon-globe"></i>&nbsp;<?php /*echo Yii::t('main','TShirt Items')*/?></a></li>
			<li><a class="yt-valign <?php /*if($module=='MogVideo') echo 'actived';*/?>" href="<?php /*echo Yii::app()->createUrl('/MogVideo/manager/admin');*/?>"><i class="glyphicon glyphicon-globe"></i>&nbsp;<?php /*echo Yii::t('main','Quản lý Video')*/?></a></li>
		</ul>
		</div>
	</li>
</ul>-->