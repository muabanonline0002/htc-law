<?php
/**
 * Action to handle image uploads from imperavi-redactor-widget
 *
 * @author Bogdan Savluk <savluk.bogdan@gmail.com>
 *
 * For examples see image_upload_readme.md
 */
class RedactorImageManagerAction extends CAction
{
    /**
     * Path to directory where to save uploaded files(relative path from webroot)
     * Should be either string or callback that will return it
     * @var string
     */
    public $storage;
    public $directory;
    public $storagePath;

    /**
     * Callback for function to implement own saving mechanism
     * The only argument passed to callback is CUploadedFile, callback should return url to file
     * @var callable
     */
    public $saveCallback;

    private $_validator = array( // default options
    );

    public function getValidator()
    {
        return $this->_validator;
    }

    public function setValidator($v)
    {
        $this->_validator = array_merge($this->_validator, $v);
    }

    /**
     * Function used to save image by default
     * @param CUploadedFile $file
     * @return string[] url to uploaded file and file name to insert in redactor by default
     * @throws CException
     */
    public function save($file)
    {
        if (is_string($this->directory)) {
            $dir = $this->directory;
        } elseif (is_callable($this->directory, true)) {
            $dir = call_user_func($this->directory);
        } else {
            throw new CException(Yii::t('imperavi-redactor-widget.main', '$directory property, should be either string or callable'));
        }

        //$webroot = Yii::getPathOfAlias('root');
        $webroot = $this->storage;

        $id = time();
        $sub = substr($id, -2);
        $id = substr($id, 0, -2);
        $dstDir = $dir . DS . $sub . DS;
        if (!is_dir($webroot . $dstDir)) {
            mkdir($webroot . $dstDir, 0777, true);
        }

        $ext = $file->getExtensionName();
        $name = $file->name;
        if (strlen($ext)) $name = substr($name, 0, -1 - strlen($ext));

        for ($i = 1, $filePath = $dstDir . $name . '.' . $ext; file_exists($webroot . $filePath); $i++) {
            $filePath = $dstDir . $name . " ($i)." . $ext;
        }

        $file->saveAs($webroot . $filePath);
        return array(Yii::app()->params['cdn_url'] . $filePath, $file->name);
    }


    public function run()
    {
        //$dir = Yii::app()->request->getParam('dir','E:/data_storage');
        /*$img1 = new stdClass();
        $img1->title = 'add';
        $img1->name='adf.jpg';
        $img1->thumb='http://cdn.vandieuhay.com/uploads/2016/3/19/56ec4257dd6382d81038ae78-thumb.jpg';
        $img1->image='http://cdn.vandieuhay.com/uploads/2016/3/19/56ec4257dd6382d81038ae78-thumb.jpg';
        $img2 = new stdClass();
        $img2->title = 'add';
        $img2->name='adf.jpg';
        $img2->thumb='http://cdn.vandieuhay.com/uploads/2016/3/19/56ec4257dd6382d81038ae78-thumb.jpg';
        $img2->image='http://cdn.vandieuhay.com/uploads/2016/3/19/56ec4257dd6382d81038ae78-thumb.jpg';*/
        //$
        $dir = $this->directory;
        $storage = $this->storagePath;
        $FilesList = array();
        $directoryUrl = Yii::app()->params['storage_url'];
        $directories[]  = $dir;
        $dirpath = array();

        $expdir = explode(DS,$dir);
        array_pop($expdir);
        array_pop($expdir);
        $FilesList[] = array(
            'name'=>'..',
            'thumb'=>'',
            'type'=>'folder',
            'path'=>implode(DS,$expdir).DS,
        );

        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if ($file == '.' || $file == '..') {
                        continue; 
                    }
                    $filePath = $dir . $file;
                    if (is_dir($filePath)) {
                        $directory_path = $filePath . DS;
                        array_push($directories, $directory_path);
                        array_push($dirpath, $file);
                        $FilesList[] = array(
                            'name' => $file,
                            'type' => 'folder',
                            'path' => $directory_path
                        );
                    } elseif ($this->ValidExtFile($file)) {
                        $dirFile = str_replace($storage,'',$dir);
                        $FilesList[] = array(
                            'name' => $file,
                            'thumb' => $directoryUrl . '/' . $dirFile . $file,
                            'image' => $directoryUrl . '/' . $dirFile . $file,
                            'type' => 'file'
                        );
                    }
                }
                closedir($dh);
            }
        }
        //var_dump($FilesList);
        $result = new stdClass;
        $result->path = $directories;
        $result->data = $FilesList;
        echo CJSON::encode($result);
    }
    private function ValidExtFile($fileName)
    {
        $validFileExt=array('jpg','png','gif');
        $ext = explode('.', $fileName);
        $extFile = $ext[count($ext)-1];
        if(in_array($extFile, $validFileExt)){
            return true;
        }
        return false;
    }
}

class UploadedImage extends CModel
{
    protected $validator;

    /** @var CUploadedFile */
    public $file;

    /**
     * Returns the list of attribute names of the model.
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return array(
            'file' => Yii::t('imperavi-redactor-widget.main', "File"),
        );
    }

    function __construct($validator = array())
    {
        $this->validator = $validator;
    }

    public function rules()
    {
        $validator = array('file', 'file') + $this->validator;
        return array(
            $validator,
        );
    }
}