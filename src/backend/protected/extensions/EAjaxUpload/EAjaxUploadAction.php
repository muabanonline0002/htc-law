<?php

Yii::import("ext.EAjaxUpload.qqFileUploader");

class EAjaxUploadAction extends CAction
{
        public $dirpath;
        public $validator;
        public function run()
        {
                // list of valid extensions, ex. array("jpeg", "xml", "bmp")
                $allowedExtensions = array("jpg");
                // min file size in bytes
                // post_max_size>=sizeLimit || upload_max_filesize>=sizeLimit
                $sizeLimit = 10 * 1024*1024;
                $validator = $this->validator;
                /*$maxSize = ini_get('post_max_size');
                $maxUpload = ini_get('upload_max_filesize');
                echo 'maxSize:'.$maxSize.'/'.'maxUpload:'.$maxUpload;
                exit;*/
                if(is_array($validator)){
                        $allowedExtensions = isset($validator['types'])?$validator['types']:array('jpg','png');
                }
                $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
                $path = $this->dirpath;
                $result = $uploader->handleUpload($path);
                // to pass data through iframe you will need to encode all html tags
                $result=htmlspecialchars(json_encode($result), ENT_NOQUOTES);
                echo $result;
        }
}
