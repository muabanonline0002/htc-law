<?php
class AjaxController extends CController{
	public function actionIndex()
	{
		die('dcm');
	}
	public function actionForceDelete()
	{
		try{
			$errorCode = 1;
			$errorMsg = 'default';
			$ids = Yii::app()->request->getParam('id');
			$modelName = Yii::app()->request->getParam('model');
			$idArr = explode(',', $ids);
			$criteria=new CDbCriteria;
			$criteria->addInCondition('id', $idArr);
			$res = $modelName::model()->updateAll(array('status'=>2), $criteria);
			if($res){
				$errorCode = 0;
				$errorMsg='success';
			}
		}catch(Exception $e)
		{
			$errorCode = 1;
			$errorMsg = 'Exception:'.$e->getMessage();
		}
		$object = new stdClass();
		$object->errorCode = $errorCode;
		$object->errorMsg = $errorMsg;
		echo json_encode($object);
		Yii::app()->end();
	}
}