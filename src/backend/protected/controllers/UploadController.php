<?php
class UploadController extends CController
{
	public function actions()
    {
        $tmpPath = Yii::app()->params['tmp_path'];
        $storagePath = Yii::app()->params['storage'];
        $dir = Yii::app()->request->getParam('dir',$storagePath);
        return array(
            'uploadThumb'=>array(
                'class' => 'ext.EAjaxUpload.EAjaxUploadAction',
                'dirpath'=>$tmpPath,
                'validator'=>array(
                    'types' => array('png','jpg'),
                )
            ),
            'fileUpload'=>array(
                'class' => 'ext.RedactorUploadAction',
                'storage'=>$storagePath,
                'directory'=>'media/files',
                'validator'=>array(
                    //'types' => 'txt, pdf, doc, docx',
                    'types' => array('jpg','png'),
                )
            ),
            'fileManager'=>array(
                'class' => 'ext.RedactorImageManagerAction',
                'directory'=>$dir,
                'storagePath'=>$storagePath
            ),
        );
    }
}