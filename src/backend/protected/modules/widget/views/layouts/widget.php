<?php $this->beginContent('//layouts/body'); ?>
<?php
$controller = Yii::app()->controller->id;
$action = Yii::app()->controller->action->id;
?>
<div class="content">
    <div class="inner">
        <div class="row border-bottom">
            <div class="col-lg-6">
                <h3><?php 
                    switch ($action) {
                        case 'admin':
                            echo Yii::app()->getModule('widget')->title;
                            break;
                        case 'create':
                            echo Yii::t('main','Thêm mới widget');
                            break;
                        case 'update':
                            echo Yii::t('main','Sửa widget');
                            break;
                        case 'view':
                            echo Yii::t('main','Chi tiết widget');
                            break;
                        default:
                            echo Yii::app()->getModule('widget')->title;
                            break;
                    }
                    $name = Yii::app()->request->getParam('name');
                    echo !empty($name)?': '.$name:'';
                ?></h3>
            </div>
            <div class="control col-lg-6 text-right">
                <div class="actions">
                    <ul>
                        <li><a href="javascript:;" class="search-button btn btn-primary" data-toggle="tooltip" data-original-title="<?php echo Yii::t('main','Tìm kiếm');?>"><i class="fa fa-search"></i>&nbsp;<?php echo Yii::t('main','Search');?></a></li>
                        <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('/widget/manage/admin');?>" data-toggle="tooltip" data-original-title="Danh sách"><i class="fa fa-list"></i>&nbsp;<?php echo Yii::t('main','List');?></a></li>
                        <li data-toggle="tooltip" data-original-title="Thêm mới">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-plus"></i>&nbsp;<?php echo Yii::t('main','Create');?><span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <?php 
                                    $widgets = Yii::app()->params['widgets'];
                                    $html="";
                                    foreach ($widgets as $key => $value){
                                        $data[]=    array('label'=>$value['title'], 'url'=>Yii::app()->createUrl('/widget/manage/create', array('name'=>$value['class'])));
                                        $html .= "<li><a href='".Yii::app()->createUrl('/widget/manage/create', array('name'=>$value['class']))."'>{$value['title']}</a></li>";
                                    }
                                    echo $html;
                                    ?>
                                </ul>
                            </div>
                        </li>
                        <?php if($action=='view'){?>
                            <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('/widget/manage/update', array('id'=>$_GET['id']));?>" data-toggle="tooltip" data-original-title="Cập nhật"><i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t('main','Update');?></a></li>
                        <?php }?>
                        <?php if($action=='update'){?>
                            <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('/widget/manage/view', array('id'=>$_GET['id']));?>" data-toggle="tooltip" data-original-title="Thông tin"><i class="fa fa-eye"></i>&nbsp;<?php echo Yii::t('main','Info');?></a></li>
                        <?php }?>
                        <li><a class="btn btn-primary" id="delete-all" href="javascript:;" data-toggle="tooltip" data-original-title="Xóa"><i class="fa fa-trash-o"></i>&nbsp;<?php echo Yii::t('main','Delete');?></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <?php echo $content;?>
    </div>
</div>
<?php $this->endContent();?>
