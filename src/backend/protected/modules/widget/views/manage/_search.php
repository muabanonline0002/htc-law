<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
	'htmlOptions'=>array('name'=>'widgets-search','class'=>'basic-form inline-form'),
)); ?>

	<div class="row">
		<div class="col-md-2">
		<?php echo $form->label($model, 'title'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'title'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-2">
		<?php echo $form->label($model, 'published'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->dropDownList($model, 'published', array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), array('prompt' => Yii::t('app', 'All'))); ?>
		</div>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search'), array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
