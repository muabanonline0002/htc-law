<div class="form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'widgets-form',
	'enableAjaxValidation' => false,
	'htmlOptions'=>array('name'=>'widgets-form','class'=>'basic-form inline-form'),
));
?>
		<?php echo $form->errorSummary($model); ?>
		<?php echo $form->error($model,'widget_name'); ?>
		<div class="row row-s1">
			<div class="col-md-2">
			<?php echo $form->labelEx($model,'published'); ?>
			</div>
			<div class="col-md-10">
			<?php echo $form->checkBox($model, 'published',array('class'=>'icheck-blue')); ?>
			</div>
		</div>
		<div class="row row-s1">
			<div class="col-md-2">
			<?php echo $form->labelEx($model,'ordering'); ?>
			</div>
			<div class="col-md-10">
			<?php echo $form->textField($model, 'ordering'); ?>
			</div>
		</div>
		<div class="row row-s1">
			<div class="col-md-2">
			<?php echo $form->labelEx($model,'show_title'); ?>
			</div>
			<div class="col-md-10">
			<?php echo $form->checkBox($model, 'show_title',array('class'=>'icheck-blue')); ?>
			</div>
		</div>
		<div class="row row-s1">
			<div class="col-md-2">
			<?php echo $form->labelEx($model,'position'); ?>
			</div>
			<div class="col-md-10">
			<?php 
			$data = CHtml::listData(BackendWidgetsPositionModel::model()->published()->findAll(), 'id', 'name');
			echo $form->dropDownList($model,'position', $data);
			?>
			</div>
		</div>
		<div class="row row-s1">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'title'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'title', array('maxlength' => 255, 'class'=>'textField-l')); ?>
		<?php echo $form->error($model,'title'); ?>
		</div>
		</div><!-- row -->
		<div class="row" style="display: none;">
		<?php 
			$model->widget_name = ($model->widget_name!='')?$model->widget_name:$widget_name;
			echo $form->textField($model, 'widget_name',array('maxlength' => 255));
		?>
		</div><!-- row -->
		
		<?php if($model->widget_name!=''):?>
		<div class="row el-dialogform">
			<fieldset><legend>Params</legend>
			<?php 
				$wd = new $model->widget_name;
				$wd->buildParams($model->params);
			?>
			</fieldset>
		</div>
		<?php else:?>
			<div class="row">
				<fieldset><legend>Params</legend>
				<?php 
					if($widget_name!=''){
						$wd = new $widget_name;
						$wd->buildParams();
					}
				?>
				</fieldset>
			</div>
		<?php endif;?>
		<div class="row">
			<div class="col-md-12">
				<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save');?></button>
				<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save & Continue');?></button>
				<button type="button" onclick="window.location.href='<?php echo Yii::app()->createUrl('/widget/manage/admin');?>'" class="btn btn-sm btn-default" ><i class="fa fa-close"></i>&nbsp;<?php echo Yii::t('main','Close');?></button>
			</div>
		</div>
<input type="hidden" name="apply" id="apply" value="0" />
<?php
$this->endWidget();
?>
</div><!-- form -->