<div class="form">
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'gallery-category-form',
	'enableAjaxValidation' => false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data', 'name'=>'gallery-category-form','class'=>'basic-form inline-form'),
));
?>
	<?php echo $form->errorSummary($model); ?>
	<div class="row">
	<div class="col-md-2">
	<?php echo $form->labelEx($model,'name'); ?>
	</div>
	<div class="col-md-10">
	<?php echo $form->textField($model, 'name', array('maxlength' => 255)); ?>
	<?php echo $form->error($model,'name'); ?>
	</div>
	</div><!-- row -->
	<div class="row">
	<div class="col-md-2">
	<?php echo $form->labelEx($model,'image'); ?>
	</div>
	<div class="col-md-10">
	<?php 
	if($model->image!=''){
		echo '<img width="180" src="'.Yii::app()->params['gallery_url'].'/thumb/'.$model->image.'" />';
	}
	?>
	<input type="file" name="image" />
	<?php echo $form->error($model,'image'); ?>
	</div>
	</div><!-- row -->
	<div class="row">
	<div class="col-md-2">
	<?php echo $form->labelEx($model,'description'); ?>
	</div>
	<div class="col-md-10">
	<?php echo $form->textArea($model, 'description',array('class'=>'textarea-m')); ?>
	<?php echo $form->error($model,'description'); ?>
	</div>
	</div><!-- row -->
	<div class="row">
	<div class="col-md-12">
		<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save');?></button>
		<button type="button" id="btn_apply" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save & Continue');?></button>
		<button type="button" onclick="window.location.href='<?php echo Yii::app()->createUrl('/gallery/galleryCategory/admin');?>'" class="btn btn-sm btn-default" ><i class="fa fa-close"></i>&nbsp;<?php echo Yii::t('main','Close');?></button>
	</div>
</div>
<input type="hidden" name="apply" id="apply" value="0" />
<?php
$this->endWidget();
?>
</div><!-- form -->