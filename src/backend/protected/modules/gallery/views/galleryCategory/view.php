<?php 
Yii::app()->getModule('gallery')->setTitle('Chi tiết Album');
$this->widget('application.widgets.iDetailView', array(
	'data' => $model,
	'attributes' => array(
	'id',
	'name',
	'image',
	'description',
	'time_modified',
	),
));
?>

