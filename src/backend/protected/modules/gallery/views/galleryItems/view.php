<?php 
Yii::app()->getModule('gallery')->setTitle('Chi tiết ảnh');
$this->widget('application.widgets.iDetailView', array(
	'data' => $model,
	'attributes' => array(
	'id',
	'catid',
	'title',
	'description',
	'image',
	),
));
?>

