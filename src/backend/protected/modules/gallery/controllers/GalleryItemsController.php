<?php
/**
 * @name GalleryItemsController
 * @version 2.0
 * @author Nguyen Van Phuong, <phuong.nguyen.itvn@gmail.com>
 * @copyright 2011 PN68 CMS
 */
class GalleryItemsController extends BackendApplicationController {

	public $layout = 'application.modules.gallery.views.layouts.gallery';
	public function init()
	{
		parent::init();
		$this->pageTitle = Yii::t("main","Gallery Manager");
	}
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'BackendGalleryItemsModel'),
		));
	}

	public function actionCreate() {
		$model = new BackendGalleryItemsModel;

		if (isset($_POST['BackendGalleryItemsModel'])) {
			$model->attributes = $_POST['BackendGalleryItemsModel'];

			if ($model->save()) {
				if(isset($_POST['image_thumb']) && $_POST['image_thumb']!=''){
					$filePath = Yii::app()->params['tmp_path'].DS.$_POST['image_thumb'];
					if(file_exists($filePath)){
						if($_POST['BackendGalleryItemsModel']['catid']==12){
							AvatarHelper::processThumb($model->id, $filePath, 'partner');
						}elseif($_POST['BackendGalleryItemsModel']['catid']==1){
							AvatarHelper::processThumb($model->id, $filePath, 'slideshow');
						}else{
							AvatarHelper::processThumb($model->id, $filePath, 'gallery');
						}
						
					}
				}
				if (Yii::app()->request->isAjaxRequest)
					Yii::app()->end();
				else{
					if(isset($_POST['apply']) && $_POST['apply']==1)
						$this->redirect(array('update', 'id' => $model->id));
					else 
						$this->redirect(array('view', 'id' => $model->id));
				}
			}
		}

		$this->render('create', array( 'model' => $model));
	}
	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'BackendGalleryItemsModel');

		if (isset($_POST['BackendGalleryItemsModel'])) {
			$model->attributes = $_POST['BackendGalleryItemsModel'];
			if ($model->save()) {
				Yii::app()->user->setFlash('success',Yii::t('main','Updated success!'));
				if(isset($_POST['image_thumb']) && $_POST['image_thumb']!=''){
					$filePath = Yii::app()->params['tmp_path'].DS.$_POST['image_thumb'];
					if(file_exists($filePath)){
						if($_POST['BackendGalleryItemsModel']['catid']==12){
							AvatarHelper::processThumb($model->id, $filePath, 'partner');
						}elseif($_POST['BackendGalleryItemsModel']['catid']==1){
							AvatarHelper::processThumb($model->id, $filePath, 'slideshow');
						}else{
							AvatarHelper::processThumb($model->id, $filePath, 'gallery');
						}
					}
				}
				if(isset($_POST['apply']) && $_POST['apply']==1)
					$this->redirect(array('update', 'id' => $model->id));
				else
					$this->redirect(array('view', 'id' => $model->id));
			}else{
				Yii::app()->user->setFlash('error',Yii::t('main','Updated fail!'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->request->isPostRequest) {
			$model = $this->loadModel($id, 'BackendGalleryItemsModel');
			$image = $model->image;
			if($model->delete()){
			@unlink(Yii::app()->params['gallery_path'].'/thumb/'.$image);
			@unlink(Yii::app()->params['gallery_path'].'/full/'.$image);
			}
			if (!Yii::app()->request->isAjaxRequest)
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}
	public function actionDelMulti()
	{
		if(Yii::app()->request->isPostRequest){
			$arr_Id = Yii::app()->request->getParam('data');
			if(strpos($arr_Id,':')!==false){
				$arrid = explode(':', $arr_Id);
			}else{
				$arrid[]=$arr_Id;
			}
				
				foreach ($arrid as $id){
					if(is_numeric($id)){
						$model = $this->loadModel($id, 'BackendGalleryItemsModel');
						$image = $model->image;
						if($model->delete()){
							@unlink(Yii::app()->params['gallery'].'/thumb/'.$image);
							@unlink(Yii::app()->params['gallery'].'/full/'.$image);
						}
					}
				}
		}else 
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}
	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('BackendGalleryItemsModel');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new BackendGalleryItemsModel('search');
		$model->unsetAttributes();
		if (isset($_GET['BackendGalleryItemsModel']))
			$model->attributes = $_GET['BackendGalleryItemsModel'];
		$this->render('admin', array(
			'model' => $model,
		));
	}

}