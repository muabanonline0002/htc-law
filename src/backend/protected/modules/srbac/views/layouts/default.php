<?php
$css='srbac.css';
$cssFile = Yii::getPathOfAlias("application.modules.srbac.css") . DS . $css;
$cssDir = Yii::getPathOfAlias("application.modules.srbac.css");
if (is_file($cssFile)) {
	$published = Yii::app()->assetManager->publish($cssDir,false, -1, YII_DEBUG);
	$cssFile = $published . "/" . $css;
	Yii::app()->clientScript->registerCssFile($cssFile);
}

?>
<?php $this->beginContent('//layouts/body'); ?>
<?php
$controller = Yii::app()->controller->id;
$action = Yii::app()->controller->action->id;
?>
<div class="content">
    <div class="inner">
        <div class="row border-bottom">
            <div class="col-lg-6">
                <h3><?php echo Yii::t('main','Phân quyền truy cập');?></h3>
            </div>
        </div>
        <div class="row">
        <?php echo $content;?>
        </div>
    </div>
</div>
<?php $this->endContent();?>
