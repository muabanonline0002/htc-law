<?php
/**
 * frontpage.php
 *
 * @author Spyros Soldatos <spyros@valor.gr>
 * @link http://code.google.com/p/srbac/
 */

/**
 * Srbac main administration page
 *
 * @author Spyros Soldatos <spyros@valor.gr>
 * @package srbac.views.authitem
 * @since 1.0.2
 */
 ?>
 <?php 
 $action = $this->action->id;
 ?>
<div class="marginBottom">
  <div class="iconSet">
  	<ul class="nav nav-tabs">
	  <li <?php if($action=='manage') echo 'class="active"';?>>
	  	<?php echo SHtml::link(
            "<i class='fa fa-crosshairs'></i>&nbsp;" .
                ($this->module->iconText ?
                Helper::translate('srbac','Managing auth items') :
                ""),
            array('authitem/manage'), array('class'=>'add-btn button'))
    	?>
	  </li>
	  <li <?php if($action=='assign') echo 'class="active"';?>><?php echo SHtml::link(
            "<i class='fa fa-magic'></i>&nbsp;" .
                ($this->module->iconText ?
                Helper::translate('srbac','Assign to users') :
                ""),
            array('authitem/assign'), array('class'=>'add-btn button'));?>
            </li>
	  <li <?php if($action=='assignments') echo 'class="active"';?>><?php echo SHtml::link(
            "<i class='fa fa-users'></i>&nbsp;".
                ($this->module->iconText ?
                Helper::translate('srbac','User\'s assignments') :
                ""),
            array('authitem/assignments'), array('class'=>'add-btn button'));?></li>
	</ul>
  </div>
    <div class="reset"></div>
</div>