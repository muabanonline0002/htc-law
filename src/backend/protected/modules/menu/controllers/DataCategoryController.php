<?php
class DataCategoryController extends BackendApplicationController {
	public $layout = 'application.modules.menu.views.layouts.ajax';
	public function actionList()
	{
		$model = new BackendCategoriesModel('search');
		$model->unsetAttributes();

		if (isset($_GET['DataCategory']))
			$model->attributes = $_GET['DataCategory'];
		$this->render('list', array(
			'model' => $model,
		));
	}
	public function actionGetCategory($id)
	{
		$data = BackendCategoriesModel::model()->findByPk($id);
		$news['id'] = $data->id;
		$news['title'] = $data->title;
		echo CJSON::encode($news);
	}
}