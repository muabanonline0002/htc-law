<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'category-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		array(
			'name'	=>	'title',
			'type'	=>	'raw',
			'value'	=>	'CHtml::link(CHtml::encode($data->title), "javascript:;", array("onclick"=>"window.parent.selectNews(\'$data->id\')"))'
		),
		array(
			'name'	=>	'id',
			'htmlOptions'	=>	array(
				'width'	=>	'30',
				'align'	=>	'center'
			),
		),
	),
));
?>
