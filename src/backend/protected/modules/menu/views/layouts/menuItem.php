<?php $this->beginContent('//layouts/body'); ?>
<?php
$controller = Yii::app()->controller->id;
$action = Yii::app()->controller->action->id;
?>
<div class="content">
    <div class="inner">
        <div class="row border-bottom">
            <div class="col-lg-12">
                <h3><?php echo Yii::app()->getModule('menu')->title;?></h3>
            </div>
            <div class="control col-lg-12 text-right">
                <div class="actions">
                    <ul>
                        <li><a href="javascript:;" class="search-button btn btn-primary" data-toggle="tooltip" data-original-title="Refresh"><i class="fa fa-search"></i>&nbsp;<?php echo Yii::t('main','Search');?></a></li>
                        <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('menu/menuItems/admin');?>" data-toggle="tooltip" data-original-title="Danh sách Menu Items"><i class="fa fa-list"></i>&nbsp;<?php echo Yii::t('main','List');?></a></li>
                        <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('menu/menuItems/create');?>" data-toggle="tooltip" data-original-title="Thêm mới Menu Items"><i class="fa fa-plus"></i>&nbsp;<?php echo Yii::t('main','Create');?></a></li>
                        <?php if($action=='view'){?>
                            <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('menu/menuItems/update', array('id'=>$_GET['id']));?>" data-toggle="tooltip" data-original-title="Cập nhật"><i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t('main','Update');?></a></li>
                        <?php }?>
                        <?php if($controller=='manager' && $action=='update'){?>
                            <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('menu/menuItems/view', array('id'=>$_GET['id']));?>" data-toggle="tooltip" data-original-title="Thông tin"><i class="fa fa-eye"></i>&nbsp;<?php echo Yii::t('main','Info');?></a></li>
                        <?php }?>
                        <li><a class="btn btn-primary" id="delete-all" href="javascript:;" data-toggle="tooltip" data-original-title="Xóa"><i class="fa fa-trash-o"></i>&nbsp;<?php echo Yii::t('main','Delete');?></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <?php echo $content;?>
    </div>
</div>
<?php $this->endContent();?>
