<?php 
if(isset($data->content_id) && !empty($data->content_id))
	$title = BackendCategoriesModel::model()->findByPk($data->content_id)->title;
?>
<div class="row" style="overflow: hidden;">
	<div class="col-md-12">
		<div class="input-group"> 
		<input class="form-control" readonly="readonly" disabled="disabled" id="slnews_title" placeholder="Chọn chủ đề ..." value="<?php if(isset($title)) echo $title;?>" /> <span class="input-group-btn">
		<input type="hidden" name="BackendMenuItemsModel[content_id]" id="slnews_id" value="<?php if(isset($data->content_id)) echo $data->content_id;?>" />
		<button id="newDialog" class="btn btn-default btn-reset" type="button">Chọn</button> </span>
		</div>
		<div id="dialog-news"></div>
	</div>
<script type="text/javascript">
    $("#newDialog").click(function ()    {
        $('#dialog-news').html('<iframe frameborder="0" width="800" height="450" src="<?php echo Yii::app()->createUrl('/menu/dataCategory/list')?>"></iframe>')
        .dialog({
            modal: true,
            dialogClass: 'dialog-chose',
            //buttons: {"Chose":function(){alert('chosed')}},
            height: 550,
            width: 830,
            title: 'Chủ đề bài viết'
        });
    });
</script>
<script>
function selectNews(id) {
	$.ajax({
	  url: "<?php echo Yii::app()->createUrl('/menu/dataCategory/getCategory')?>",
	  data: "id="+id,
	  dataType: 'JSON',
	  success: function(data){
		  $("#slnews_id").attr("value",id);
		  $("#slnews_title").attr("value",data.title);
	  }
	});
	$("#dialog-news").dialog("close");
}
</script>
</div>