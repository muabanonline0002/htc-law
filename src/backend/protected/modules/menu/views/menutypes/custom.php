<?php 
$u = (isset($data->params) && $data->params!='')?unserialize($data->params):NULL;
if(!empty($u) && count($u['p'])>0){
	foreach ($u['p'] as $key => $value){
		$p_array[] = "$key=$value";
	}
	$param = implode('&',$p_array);
}
?>
<div class="row">
	<input type="text" placeholder="<?php echo Yii::t('app', 'Router');?>" id="BackendMenuItemsModel_params_r" name="BackendMenuItemsModel[params][r]" value="<?php if(count($u)>0) echo $u['r'];?>" />
	<p style="text-align: right;color: #777;font-size: 14px;"><i>Example: /module/controller/action</i></p>
</div>
<div class="row">
	<input type="text" placeholder="<?php echo Yii::t('app', 'Params');?>" name="BackendMenuItemsModel[params][p]" value="<?php if(isset($param) && !empty($param)) echo $param;?>" />
	<p style="text-align: right;color: #777;font-size: 14px;"><i>Example: a=1&b=2&c=3</i></p>
</div>
