<div class="form">
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'system-settings-model-form',
	'enableAjaxValidation' => false,
	'htmlOptions'=>array('name'=>'system-settings-model-form','class'=>'basic-form inline-form'),
));
?>
<div class="row">
	<div class="col-md-2">
	<label><?php echo Yii::t('main','Site Name');?></label>
	</div>
	<div class="col-md-10">
	<?php echo CHtml::textField('System[SYS_SITE_NAME]', $settings['SYS_SITE_NAME']['value_code'])?>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
	<label><?php echo Yii::t('main','Frontend Language');?></label>
	</div>
	<div class="col-md-10">
	<?php 
	$data = array(
			'vi'=>'Tiếng Việt',
			'en'=>'Tiếng Anh',
	);
		echo CHtml::dropDownList('System[SYS_FRONTEND_LANG]', $settings['SYS_FRONTEND_LANG']['value_code'], $data)
	?>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
	<label><?php echo Yii::t('main','HotLine');?></label>
	</div>
	<div class="col-md-10">
	<?php echo CHtml::textField('System[SYS_HOTLINE]', $settings['SYS_HOTLINE']['value_code']);?>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
	<label><?php echo $settings['SYS_ADDRESS']['label'];?></label>
	</div>
	<div class="col-md-10">
	<?php
	$this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
		'name' => 'System[SYS_ADDRESS]',
		'value'=>$settings['SYS_ADDRESS']['value_code'],
		'attribute' => 'System[SYS_ADDRESS]',
		'options' => array(
			'buttons'=>array('html','formatting','fontfamily','fontcolor',
				'bold', 'italic', 'underline', 'deleted','unorderedlist','orderedlist','outdent','indent','link','alignment'
			,'horizontalrule','image'
			),
			'lang' => 'en',
			'imageUpload'=> Yii::app()->createUrl('/upload/fileUpload'),
			'imageUploadErrorCallback'=>'js:function(json){ alert(json.error); }',
			//'fileUpload'=>$this->createUrl('fileUpload'),
			//'fileUploadErrorCallback'=>'js:function(obj, json){ alert(json.error); }',
			'counterCallback'=>'js:function(data){
			var counter_text = data.words+" words"+", "+data.characters+" characters";
			$("#full_text_counter_address").html(counter_text);
			}
		',
			'imageManagerJson'=>Yii::app()->createUrl('/upload/fileManager'),
			'minHeight'=>300,
			'placeholder'=>Yii::t('main','Nhập nội dung')
		),
		'plugins' => array(
			'fontfamily'=>array(
				'js'=>array('fontfamily.js')
			),
			'fontcolor'=>array(
				'js'=>array('fontcolor.js')
			),
			'fontsize'=>array(
				'js'=>array('fontsize.js')
			),
			'counter'=>array(
				'js'=>array('counter.js')
			),
			'cleantext'=>array(
				'js'=>array('cleantext.js')
			),
			'clips' => array(
				// You can set base path to assets
				//'basePath' => 'application.components.imperavi.my_plugin',
				// or url, basePath will be ignored.
				// Defaults is url to plugis dir from assets
				//'baseUrl' => '/js/my_plugin',
				'css' => array('clips.css',),
				'js' => array('clips.js',),
				// add depends packages
				'depends' => array('imperavi-redactor',),
			),
			'imagemanager' => array(
				'js' => array('imagemanager.js'),
			),
			'video'=>array(
				'js'=>array('video.js')
			),
			'table'=>array(
				'js'=>array('table.js')
			),
			/*'definedlinks'=>array(
                'js'=>array('definedlinks.js')
            ),*/
			'fullscreen' => array(
				'js' => array('fullscreen.js',),
			),
		),
	));
	?>
<div id="full_text_counter_address"></div>
	</div>
</div>
    <div class="row">
        <div class="col-md-2">
            <label><?php echo $settings['SYS_ADDRESS_EN']['label'];?></label>
        </div>
        <div class="col-md-10">
            <?php
            $this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
                'name' => 'System[SYS_ADDRESS_EN]',
                'value'=>$settings['SYS_ADDRESS_EN']['value_code'],
                'attribute' => 'System[SYS_ADDRESS_EN]',
                'options' => array(
                    'buttons'=>array('html','formatting','fontfamily','fontcolor',
                        'bold', 'italic', 'underline', 'deleted','unorderedlist','orderedlist','outdent','indent','link','alignment'
                    ,'horizontalrule','image'
                    ),
                    'lang' => 'en',
                    'imageUpload'=> Yii::app()->createUrl('/upload/fileUpload'),
                    'imageUploadErrorCallback'=>'js:function(json){ alert(json.error); }',
                    //'fileUpload'=>$this->createUrl('fileUpload'),
                    //'fileUploadErrorCallback'=>'js:function(obj, json){ alert(json.error); }',
                    'counterCallback'=>'js:function(data){
			var counter_text = data.words+" words"+", "+data.characters+" characters";
			$("#full_text_counter_address_en").html(counter_text);
			}
		',
                    'imageManagerJson'=>Yii::app()->createUrl('/upload/fileManager'),
                    'minHeight'=>300,
                    'placeholder'=>Yii::t('main','Nhập nội dung')
                ),
                'plugins' => array(
                    'fontfamily'=>array(
                        'js'=>array('fontfamily.js')
                    ),
                    'fontcolor'=>array(
                        'js'=>array('fontcolor.js')
                    ),
                    'fontsize'=>array(
                        'js'=>array('fontsize.js')
                    ),
                    'counter'=>array(
                        'js'=>array('counter.js')
                    ),
                    'cleantext'=>array(
                        'js'=>array('cleantext.js')
                    ),
                    'clips' => array(
                        // You can set base path to assets
                        //'basePath' => 'application.components.imperavi.my_plugin',
                        // or url, basePath will be ignored.
                        // Defaults is url to plugis dir from assets
                        //'baseUrl' => '/js/my_plugin',
                        'css' => array('clips.css',),
                        'js' => array('clips.js',),
                        // add depends packages
                        'depends' => array('imperavi-redactor',),
                    ),
                    'imagemanager' => array(
                        'js' => array('imagemanager.js'),
                    ),
                    'video'=>array(
                        'js'=>array('video.js')
                    ),
                    'table'=>array(
                        'js'=>array('table.js')
                    ),
                    /*'definedlinks'=>array(
                        'js'=>array('definedlinks.js')
                    ),*/
                    'fullscreen' => array(
                        'js' => array('fullscreen.js',),
                    ),
                ),
            ));
            ?>
            <div id="full_text_counter_address_en"></div>
        </div>
    </div>
    <div class="row">
	<div class="col-md-2">
	<label><?php echo $settings['SYS_ADDRESS_CONTACT']['label'];?></label>
	</div>
	<div class="col-md-10">
<?php
	$this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
		'name' => 'System[SYS_ADDRESS_CONTACT]',
		'value'=>$settings['SYS_ADDRESS_CONTACT']['value_code'],
		'attribute' => 'System[SYS_ADDRESS_CONTACT]',
		'options' => array(
			'buttons'=>array('html','formatting','fontfamily','fontcolor',
				'bold', 'italic', 'underline', 'deleted','unorderedlist','orderedlist','outdent','indent','link','alignment'
			,'horizontalrule','image'
			),
			'lang' => 'en',
			'imageUpload'=> Yii::app()->createUrl('/upload/fileUpload'),
			'imageUploadErrorCallback'=>'js:function(json){ alert(json.error); }',
			//'fileUpload'=>$this->createUrl('fileUpload'),
			//'fileUploadErrorCallback'=>'js:function(obj, json){ alert(json.error); }',
			'counterCallback'=>'js:function(data){
			var counter_text = data.words+" words"+", "+data.characters+" characters";
			$("#full_text_counter").html(counter_text);
			}
		',
			'imageManagerJson'=>Yii::app()->createUrl('/upload/fileManager'),
			'minHeight'=>300,
			'placeholder'=>Yii::t('main','Nhập nội dung')
		),
		'plugins' => array(
			'fontfamily'=>array(
				'js'=>array('fontfamily.js')
			),
			'fontcolor'=>array(
				'js'=>array('fontcolor.js')
			),
			'fontsize'=>array(
				'js'=>array('fontsize.js')
			),
			'counter'=>array(
				'js'=>array('counter.js')
			),
			'cleantext'=>array(
				'js'=>array('cleantext.js')
			),
			'clips' => array(
				// You can set base path to assets
				//'basePath' => 'application.components.imperavi.my_plugin',
				// or url, basePath will be ignored.
				// Defaults is url to plugis dir from assets
				//'baseUrl' => '/js/my_plugin',
				'css' => array('clips.css',),
				'js' => array('clips.js',),
				// add depends packages
				'depends' => array('imperavi-redactor',),
			),
			'imagemanager' => array(
				'js' => array('imagemanager.js'),
			),
			'video'=>array(
				'js'=>array('video.js')
			),
			'table'=>array(
				'js'=>array('table.js')
			),
			/*'definedlinks'=>array(
                'js'=>array('definedlinks.js')
            ),*/
			'fullscreen' => array(
				'js' => array('fullscreen.js',),
			),
		),
	));
	?>
<div id="full_text_counter"></div>
</div>
</div>
<div class="row">
	<div class="col-md-2">
	<label><?php echo Yii::t('main','Keywords Meta');?></label>
	</div>
	<div class="col-md-10">
	<?php echo CHtml::textArea('System[SYS_META_KEYWORDS]', $settings['SYS_META_KEYWORDS']['value_code'], array('style'=>'width: 500px; height: 150px'))?>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
	<label><?php echo Yii::t('main','Description Meta');?></label>
	</div>
	<div class="col-md-10">
	<?php echo CHtml::textArea('System[SYS_META_DESCRIPTION]', $settings['SYS_META_DESCRIPTION']['value_code'], array('style'=>'width: 500px; height: 150px'))?>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
		<?php echo CHtml::submitButton(Yii::t('main','Save'), array('class'=>'btn btn-primary btn-sm'))?>
	</div>
</div>
<?php
$this->endWidget();
?>
</div><!-- form -->