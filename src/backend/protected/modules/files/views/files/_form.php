<div class="form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'files-form',
	'enableAjaxValidation' => false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data', 'name'=>'files-form', 'class'=>'basic-form inline-form'),
));
?>
	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'name'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'name', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'name'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'source_path'); ?>
		</div>
		<div class="col-md-10">
		<?php 
			if(isset($model->source_path)) echo $model->source_path;
		?>
		<input type="file" name="file" value="" />
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'download'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'download'); ?>
		<?php echo $form->error($model,'download'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'view'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'view'); ?>
		<?php echo $form->error($model,'view'); ?>
		</div>
		</div><!-- row -->
	<div class="row">
		<div class="col-md-12">
			<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save');?></button>
			<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save & Continue');?></button>
			<button type="button" onclick="window.location.href='<?php echo Yii::app()->createUrl('/files/files/admin');?>'" class="btn btn-sm btn-default" ><i class="fa fa-close"></i>&nbsp;<?php echo Yii::t('main','Close');?></button>
		</div>
	</div>
<?php
$this->endWidget();
?>
</div><!-- form -->