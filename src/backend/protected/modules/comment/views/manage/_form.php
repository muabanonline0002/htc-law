<div class="form">
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'comment-form',
	'enableAjaxValidation' => false,
	'htmlOptions'=>array('name'=>'comment-form','class'=>'basic-form inline-form'),
));
?>

	<?php echo $form->errorSummary($model); ?>
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'post_id'); ?>
		</div>
		<div class="col-md-10">
		<?php 
		if($model->post_id>0 && $title = BackendNewsModel::model()->findByPk($model->post_id)) echo $title;
			else
		echo $form->textField($model, 'post_id'); ?>
		<?php echo $form->error($model,'post_id'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'content'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textArea($model, 'content', array('class'=>'textarea-m')); ?>
		<?php echo $form->error($model,'content'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'status'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->dropDownList($model, 'status', BackendLookupModel::items('CommentStatus')); ?>
		<?php echo $form->error($model,'status'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'email'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'email', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'email'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'url'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'url', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'url'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'type'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'type', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'type'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'create_time'); ?>
		</div>
		<div class="col-md-10">
		<?php 
		if($model->create_time>0) echo date('d-m-Y H:i:s', $model->create_time);
			else
				echo $form->textField($model, 'create_time'); ?>
		<?php echo $form->error($model,'create_time'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'author'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'author', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'author'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
			<div class="col-md-12">
				<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save');?></button>
				<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save & Continue');?></button>
				<button type="button" onclick="window.location.href='<?php echo Yii::app()->createUrl('/comment/manage/admin');?>'" class="btn btn-sm btn-default" ><i class="fa fa-close"></i>&nbsp;<?php echo Yii::t('main','Close');?></button>
			</div>
		</div>
<?php
$this->endWidget();
?>
</div><!-- form -->