<div class="login-box">
	<div class="login-box-title red">Đăng nhập hệ thống</div>
	<div class="login-box-content">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'login-form',
			'enableAjaxValidation'=>true,
		)); ?>
		<?php echo $form->errorSummary($model); ?>
		<br />
		<?php echo $form->textField($model,'username', array('placeholder'=>'Tên đăng nhập')); ?>
		<?php echo $form->passwordField($model,'password', array('placeholder'=>'Mật khẩu')); ?>
		<?php echo $form->checkBox($model,'rememberMe', array('id'=>'remember','class'=>'icheck-blue')); ?>
		<label for="remember">Ghi nhớ đăng nhập</label>
		<input type="submit" name="submit" id="submit" value="Go!">
		<?php $this->endWidget(); ?>
	</div>
</div>