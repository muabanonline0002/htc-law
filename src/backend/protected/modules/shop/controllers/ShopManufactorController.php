<?php

class ShopManufactorController extends BackendApplicationController {

	public $layout = 'application.modules.shop.views.layouts.manufactor';
	public function beforeAction($action) {
		return parent::beforeAction($action);
	}
	
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'BackendShopManufactorModel'),
		));
	}

	public function actionCreate() {
		$model = new BackendShopManufactorModel;


		if (isset($_POST['BackendShopManufactorModel'])) {
			$model->setAttributes($_POST['BackendShopManufactorModel']);

			if ($model->save()) {
				if(!empty($_POST['file'])){
					$filePath = Yii::app()->params['tmp_path'].$_POST['file'];
					if(file_exists($filePath)){
						AvatarHelper::processThumb($model->id, $filePath, 'shopmanufactor');
					}
				}
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'BackendShopManufactorModel');


		if (isset($_POST['BackendShopManufactorModel'])) {
			$model->setAttributes($_POST['BackendShopManufactorModel']);

			if ($model->save()) {
				if(!empty($_POST['file'])){
					$filePath = Yii::app()->params['tmp_path'].$_POST['file'];
					if(file_exists($filePath)){
						AvatarHelper::processThumb($model->id, $filePath, 'shopmanufactor');
					}
				}
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'BackendShopManufactorModel')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('BackendShopManufactorModel');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new BackendShopManufactorModel('search');
		$model->unsetAttributes();

		if (isset($_GET['BackendShopManufactorModel']))
			$model->setAttributes($_GET['BackendShopManufactorModel']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}