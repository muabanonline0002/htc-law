<?php

	/*$this->widget('application.widgets.iNavbox', array(
			'type'=>'', // null or 'inverse'
			'collapse'=>true, // requires bootstrap-responsive.css
			'brand'=>Shop::t('Settings'),
			'items'=>array(
					array(
						'class'=>'bootstrap.widgets.TbMenu',
						'htmlOptions'=>array('class'=>'flat-list'),
						'items'=>array(
								array('label'=>Shop::t('Specifications'), 'url'=>Yii::app()->createUrl('/shop/productSpecification/admin'), 'visible'=>!Yii::app()->user->isGuest, 'active'=>($controller=='productSpecification')?true:false),
								array('label'=>Shop::t('Shipping Methods'), 'url'=>Yii::app()->createUrl('/shop/shippingMethod/admin'), 'visible'=>!Yii::app()->user->isGuest, 'active'=>($controller=='shippingMethod')?true:false),
								array('label'=>Shop::t('Payment Methods'), 'url'=>Yii::app()->createUrl('/shop/paymentMethod/admin'), 'visible'=>!Yii::app()->user->isGuest, 'active'=>($controller=='paymentMethod')?true:false),
								array('label'=>Shop::t('Tax'), 'url'=>Yii::app()->createUrl('/shop/tax/admin'), 'visible'=>!Yii::app()->user->isGuest, 'active'=>($controller=='tax')?true:false),
								array('label'=>Shop::t('Orders'), 'url'=>Yii::app()->createUrl('/shop/order/admin'), 'visible'=>!Yii::app()->user->isGuest, 'active'=>($controller=='order')?true:false),
						),
					),
			),
	));*/
?>

<?php $this->beginContent('//layouts/body'); ?>
<?php
$controller = Yii::app()->controller->id;
$action = Yii::app()->controller->action->id;
?>
<div class="content">
    <div class="inner">
        <div class="row border-bottom">
            <div class="col-lg-6">
                <h3><?php echo Yii::app()->getModule('shop')->title;?></h3>
            </div>
            <div class="control col-lg-6 text-right">
                <div class="actions">
                    <ul>
                        <li><a href="javascript:;" class="search-button btn btn-primary" data-toggle="tooltip" data-original-title="<?php echo Yii::t('main','Tìm kiếm');?>"><i class="fa fa-search"></i>&nbsp;<?php echo Yii::t('main','Search');?></a>
                        </li>
                        <!-- <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('/shop/shippingMethod/admin');?>" data-toggle="tooltip" data-original-title="Danh sách"><i class="fa fa-list"></i>&nbsp;<?php echo Yii::t('main','List');?></a>
                        </li>
                        <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('/shop/productSpecification/admin');?>" data-toggle="tooltip" data-original-title="Thêm mới"><i class="fa fa-plus"></i>&nbsp;<?php echo Yii::t('main','Create');?></a>
                        </li>
                        <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('/shop/paymentMethod/admin');?>" data-toggle="tooltip" data-original-title="Thêm mới"><i class="fa fa-plus"></i>&nbsp;<?php echo Yii::t('main','Create');?></a>
                        </li>
                        <li><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('/shop/tax/admin');?>" data-toggle="tooltip" data-original-title="Thêm mới"><i class="fa fa-plus"></i>&nbsp;<?php echo Yii::t('main','Create');?></a>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
        <?php echo $content;?>
    </div>
</div>
<?php $this->endContent();?>