<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'backend-shop-order-info-model-form',
	'enableAjaxValidation' => false,
));
?>
	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'customer_name'); ?>
		<?php echo $form->textField($model, 'customer_name', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'customer_name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'customer_email'); ?>
		<?php echo $form->textField($model, 'customer_email', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'customer_email'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'customer_phone'); ?>
		<?php echo $form->textField($model, 'customer_phone', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'customer_phone'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'customer_address_ship'); ?>
		<?php echo $form->textArea($model, 'customer_address_ship'); ?>
		<?php echo $form->error($model,'customer_address_ship'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'ship_type'); ?>
		<?php echo $form->textField($model, 'ship_type', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'ship_type'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'customer_data'); ?>
		<?php echo $form->textArea($model, 'customer_data'); ?>
		<?php echo $form->error($model,'customer_data'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'customer_comment'); ?>
		<?php echo $form->textArea($model, 'customer_comment'); ?>
		<?php echo $form->error($model,'customer_comment'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total_money'); ?>
		<?php echo $form->textField($model, 'total_money', array('maxlength' => 50)); ?>
		<?php echo $form->error($model,'total_money'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_datetime'); ?>
		<?php echo $form->textField($model, 'created_datetime'); ?>
		<?php echo $form->error($model,'created_datetime'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'updated_datetime'); ?>
		<?php echo $form->textField($model, 'updated_datetime'); ?>
		<?php echo $form->error($model,'updated_datetime'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->checkBox($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('main', 'Save'), array('class'=>'button p0 size1of1'));
$this->endWidget();
?>
</div><!-- form -->