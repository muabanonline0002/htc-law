<?php
Yii::app()->getModule('shop')->setTitle(Yii::t('ShopModule.shop','Order Info Manager'));
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('backend-shop-order-info-model-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('application.widgets.iGridView', array(
	'id' => 'backend-shop-order-info-model-grid',
	'dataProvider' => $model->search(),
	//'filter' => $model,
	'columns' => array(
		array(
			'header'=>CHtml::checkBox("all",false,array("id"=>"check_all","class"=>"icheck-all")),
			'value'=>'CHtml::checkBox("rad_ID[]",false,array("value"=>$data->id,"class"=>"icheck-blue"))',
			'type'=>'raw'
		),
		'customer_name',
		'customer_email',
		'customer_phone',
		'customer_address_ship',
		'ship_type',
		/*
		'customer_data',
		'customer_comment',
		'total_money',
		'created_datetime',
		'updated_datetime',
		array(
					'name' => 'status',
					'value' => '($data->status === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		*/
		'id',
		array(
			'class'=>'application.widgets.iButtonColumn',
			'htmlOptions'=>array('class'=>'actions'),
		),
	),
)); ?>