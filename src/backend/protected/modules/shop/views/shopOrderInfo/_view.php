<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('customer_name')); ?>:
	<?php echo GxHtml::encode($data->customer_name); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('customer_email')); ?>:
	<?php echo GxHtml::encode($data->customer_email); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('customer_phone')); ?>:
	<?php echo GxHtml::encode($data->customer_phone); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('customer_address_ship')); ?>:
	<?php echo GxHtml::encode($data->customer_address_ship); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ship_type')); ?>:
	<?php echo GxHtml::encode($data->ship_type); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('customer_data')); ?>:
	<?php echo GxHtml::encode($data->customer_data); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('customer_comment')); ?>:
	<?php echo GxHtml::encode($data->customer_comment); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('total_money')); ?>:
	<?php echo GxHtml::encode($data->total_money); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('created_datetime')); ?>:
	<?php echo GxHtml::encode($data->created_datetime); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('updated_datetime')); ?>:
	<?php echo GxHtml::encode($data->updated_datetime); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('status')); ?>:
	<?php echo GxHtml::encode($data->status); ?>
	<br />
	*/ ?>

</div>