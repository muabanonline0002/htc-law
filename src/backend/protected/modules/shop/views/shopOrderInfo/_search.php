<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'customer_name'); ?>
		<?php echo $form->textField($model, 'customer_name', array('maxlength' => 255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'customer_email'); ?>
		<?php echo $form->textField($model, 'customer_email', array('maxlength' => 255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'customer_phone'); ?>
		<?php echo $form->textField($model, 'customer_phone', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'customer_address_ship'); ?>
		<?php echo $form->textArea($model, 'customer_address_ship'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ship_type'); ?>
		<?php echo $form->textField($model, 'ship_type', array('maxlength' => 255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'customer_data'); ?>
		<?php echo $form->textArea($model, 'customer_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'customer_comment'); ?>
		<?php echo $form->textArea($model, 'customer_comment'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'total_money'); ?>
		<?php echo $form->textField($model, 'total_money', array('maxlength' => 50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'created_datetime'); ?>
		<?php echo $form->textField($model, 'created_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'updated_datetime'); ?>
		<?php echo $form->textField($model, 'updated_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'status'); ?>
		<?php echo $form->dropDownList($model, 'status', array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('main', 'Search'), array('class'=>'button p0 size1of1')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
