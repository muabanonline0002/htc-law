<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'htmlOptions'=>array('class'=>'basic-form inline-form')
)); ?>


	<div class="row">
		<div class="col-md-2">
		<?php echo $form->label($model,'title'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model,'title',array('size'=>45,'maxlength'=>45)); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
		<?php echo $form->label($model,'parent'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model,'parent'); ?>
		</div>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('main','Search'),array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->