<?php
Yii::app()->getModule('shop')->setTitle(Yii::t('shop','View Category'));
$this->widget('application.widgets.iDetailView', array(
	'data' => $model,
	'attributes' => array(
		'category_id',
		'parent_id',
		'title',
		'alias',
		'description',
		'language',
		'position',
		'ordering',
		'level',
		'published',
		'update_time'
	),
));
?>

