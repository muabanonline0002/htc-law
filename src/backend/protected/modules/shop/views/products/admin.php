
<?php 
Yii::app()->getModule('shop')->setTitle(Yii::t('ShopModule.shop','Products Manager'));
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('products-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
		'model' => $model,
)); ?>
</div><!-- search-form -->
<?php 
$this->widget('application.widgets.iGridView', array(
	'id'=>'products-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array(
			'header'=>CHtml::checkBox("all",false,array("id"=>"check_all","class"=>"icheck-all")),
			'value'=>'CHtml::checkBox("rad_ID[]",false,array("value"=>$data->product_id,"class"=>"icheck-blue"))',
			'type'=>'raw'
		),
		array(
			'header'=>'Image',
			'type'=>'raw',
			'value'=>'CHtml::image(AvatarHelper::getThumbUrl($data->product_id, "products","sm"),"",array("width"=>"50"))'
		),
		array(
			'name'	=>	'title',
			'type'	=>	'raw',
			'value'	=>	'CHtml::link(CHtml::encode($data->title), array("/shop/products/update","id"=>$data->product_id))'
		),
		array(
			'type'	=>	'raw',
			'name'	=>	'category_id',
			'value'	=>	'$data->category->title',
		),
		'price',
		array(
            'class'=>'JToggleColumn',
            'name'=>'published', // boolean model attribute (tinyint(1) with values 0 or 1)
            'htmlOptions'=>array('width'=>80),
		),
		array(
			'header'=>'Lượt xem',
			'value'=>'isset($data->statistic->views)?$data->statistic->views:0'
		),
		array(
			'class'=>'application.widgets.iButtonColumn',
			'htmlOptions'=>array('class'=>'actions'),
		),
	)
)
); 

?>