
<?php 
Yii::app()->getModule('shop')->setTitle(Yii::t('main','View ').' #'.$model->id);
$this->widget('application.widgets.iDetailView', array(
	'data' => $model,
		'attributes' => array(
		'id',
		'name',
		'description',
		'image',
	),
)); 
?>

