<div class="form">
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'backend-shop-manufactor-model-form',
	'enableAjaxValidation' => false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data', 'name'=>'manufactor-form', 'class'=>'basic-form inline-form'),
));
?>

	<?php echo $form->errorSummary($model); ?>
		<div class="row">
			<div class="col-md-2">
			<?php echo $form->labelEx($model,'image'); ?>
			</div>
			<div class="col-md-10">
			<input type="hidden" id="file" name="file" value="" />
			<div id="img_tmp" style="margin-bottom: 5px;">
				<img style="border: 1px solid #ccc;" width="200" height="150" src="<?php echo ($model->isNewRecord)?Yii::app()->theme->baseUrl."/images/no_image.png":AvatarHelper::getThumbUrl($model->id, "shopmanufactor","sm")?>" />
			</div>
				<?php $this->widget('ext.EAjaxUpload.EAjaxUpload',
					array(
					        'id'=>'uploadFile',
					        'config'=>array(
					               'action'=>Yii::app()->createUrl('/upload/uploadThumb'),
					               'allowedExtensions'=>array("jpg"),//array("jpg","jpeg","gif","exe","mov" and etc...
					               'sizeLimit'=>1000*1024*1024,// maximum file size in bytes
					               'minSizeLimit'=>1024,// minimum file size in bytes
					               'onComplete'=>"js:function(id, fileName, responseJSON){ 
					               		if(responseJSON.success==true){
					               			$('#img_tmp img').attr('src','".Yii::app()->params['tmp_url']."/'+responseJSON.filename)
					               			$('#file').attr('value',fileName);
					               		}else{
					               			alert(fileName);
					               		}
					                }",
					              )
					)); 
				?>
			</div>
		</div>
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'name'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'name'); ?>
		<?php echo $form->error($model,'name'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'position'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'position'); ?>
		<?php echo $form->error($model,'position'); ?>
		</div>
		</div><!-- row -->
		
		<div class="row">
			<div class="col-md-2">
			<?php echo $form->labelEx($model,'description'); ?>
			</div>
			<div class="col-md-10">
				<?php
				$this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
					'model' => $model,
					'attribute' => 'description',
					'options' => array(
						'buttons'=>array('html','formatting','fontfamily','fontcolor',
							'bold', 'italic', 'underline', 'deleted','unorderedlist','orderedlist','outdent','indent','link','alignment'
						,'horizontalrule','image'
						),
						'lang' => 'en',
						'imageUpload'=> Yii::app()->createUrl('/upload/fileUpload'),
						'imageUploadErrorCallback'=>'js:function(json){ alert(json.error); }',
						//'fileUpload'=>$this->createUrl('fileUpload'),
						//'fileUploadErrorCallback'=>'js:function(obj, json){ alert(json.error); }',
						'counterCallback'=>'js:function(data){
						var counter_text = data.words+" words"+", "+data.characters+" characters";
						$("#full_text_counter").html(counter_text);
						}
					',
						'imageManagerJson'=>Yii::app()->createUrl('/upload/fileManager'),
						'minHeight'=>300,
						'placeholder'=>Yii::t('main','Nhập nội dung')
					),
					'plugins' => array(
						'fontfamily'=>array(
							'js'=>array('fontfamily.js')
						),
						'fontcolor'=>array(
							'js'=>array('fontcolor.js')
						),
						'fontsize'=>array(
							'js'=>array('fontsize.js')
						),
						'counter'=>array(
							'js'=>array('counter.js')
						),
						'cleantext'=>array(
							'js'=>array('cleantext.js')
						),
						'clips' => array(
							// You can set base path to assets
							//'basePath' => 'application.components.imperavi.my_plugin',
							// or url, basePath will be ignored.
							// Defaults is url to plugis dir from assets
							//'baseUrl' => '/js/my_plugin',
							'css' => array('clips.css',),
							'js' => array('clips.js',),
							// add depends packages
							'depends' => array('imperavi-redactor',),
						),
						'imagemanager' => array(
							'js' => array('imagemanager.js'),
						),
						'video'=>array(
							'js'=>array('video.js')
						),
						'table'=>array(
							'js'=>array('table.js')
						),
						/*'definedlinks'=>array(
                            'js'=>array('definedlinks.js')
                        ),*/
						'fullscreen' => array(
							'js' => array('fullscreen.js',),
						),
					),
				));
				?>
				<div id="full_text_counter"></div>
			<?php echo $form->error($model,'fulltext'); ?>
			</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-12">
			<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save');?></button>
			<button type="button" id="btn_apply" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save & Continue');?></button>
			<button type="button" onclick="window.location.href='<?php echo Yii::app()->createUrl('/shop/shopManufactor/admin');?>'" class="btn btn-sm btn-default" ><i class="fa fa-close"></i>&nbsp;<?php echo Yii::t('main','Close');?></button>
		</div>
	</div>
<input type="hidden" name="apply" id="apply" value="0" />
<?php
$this->endWidget();
?>
</div><!-- form -->