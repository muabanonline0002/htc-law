<?php 
Yii::app()->getModule('shop')->setTitle(Yii::t('main','View'));
$this->widget('application.widgets.iDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'username',
		'phone',
		'ship_address',
		array(
			'name'=>'payment_method',
			'value'=>BackendShopPaymentMethodModel::model()->findByPk($model->payment_method)->title
		),
		array(
			'name'=>'ship_method',
			'value'=>BackendShopShippingMethodModel::model()->findByPk($model->ship_method)->title,
		),
		array(
			'name'=>'user_id',
			'value'=>BackendUsersAccountModel::model()->findByPk($model->user_id)->username
		),
		array(
			'name'=>'cart',
			'type'=>'raw',
			'value'=>ShopHelpers::getCartContent($model->cart)
		),
		array(
			'name'=>'status',
			'value'=>BackendShopOrderStatusModel::model()->findByPk($model->status)->name,
		),
	),
)); ?>