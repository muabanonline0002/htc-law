<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
  'action'=>Yii::app()->createUrl($this->route),
  'method'=>'get',
  'htmlOptions'=>array('name'=>'poll-search','class'=>'basic-form inline-form'),
)); ?>

  <div class="row">
    <div class="col-md-2">
    <?php echo $form->label($model,'title'); ?>
    </div>
    <div class="col-md-10">
    <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
    </div>
  </div>

  <div class="row buttons">
  <div class="col-md-2"><?php echo CHtml::submitButton('Search', array('class' => 'btn btn-primary' )); ?></div>
  <div class="col-md-10"></div>
  </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
