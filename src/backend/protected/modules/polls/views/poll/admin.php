<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
  $('.search-form').toggle();
  return false;
});
$('.search-form form').submit(function(){
  $.fn.yiiGridView.update('poll-grid', {
    data: $(this).serialize()
  });
  return false;
});
");
?>


<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
  'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('application.widgets.iGridView', array(
  	'id'=>'poll-grid',
  	'dataProvider'=>$model->search(),
  	'columns'=>array(
	array(
      'header'=>CHtml::checkBox("all",false,array("id"=>"check_all","class"=>"icheck-all")),
      'value'=>'CHtml::checkBox("rad_ID[]",false,array("value"=>$data->id,"class"=>"icheck-blue"))',
      'type'=>'raw'
  ),
	array(
		'name'	=>	'title',
		'value'	=>	'CHtml::link(CHtml::encode($data->title), array("poll/update","id"=>$data->id))',
		'type'	=>	'raw'
	),
    'description',
  array(
          'class'=>'JToggleColumn',
          'name'=>'status', // boolean model attribute (tinyint(1) with values 0 or 1)
          'htmlOptions'=>array('style'=>'text-align:center;min-width:60px;'),
	 	'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
	),
  array(
  	'name'	=>	'id',
  	'htmlOptions'	=>	array('width'=>'30'),
  ),
	array(
		  'class'=>'application.widgets.iButtonColumn',
  		'htmlOptions'=>array('class'=>'actions'),
  	),
 	),
)); ?>
