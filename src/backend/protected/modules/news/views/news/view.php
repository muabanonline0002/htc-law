<?php
Yii::app()->clientScript->registerScript('news-detail', "
$('#delete-all').on('click', function(){
	if(confirm('Bạn chắc chắn muốn xóa bản ghi này?')){
		jQuery.ajax({
			url: '".Yii::app()->createUrl('/ajax/forceDelete')."',
			data: {id:".$model->id.",model: 'BackendNewsModel'},
			type: 'post',
			beforeSend: function(){
			},
			success: function(res){
				window.location.href='".Yii::app()->createUrl('/news/news/admin')."';
			}
		})
	}
})
");
$this->widget('application.widgets.iDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'catid',
		'title',
		'alias',
		'introtext',
		'fulltext',
		'created',
		'created_by',
		'modified',
		'modified_by',
		'ordering',
		'metakey',
		'metadesc',
		'status',
	),
)); 
?>

