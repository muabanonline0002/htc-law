<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
	'htmlOptions'=>array('class'=>'basic-form inline-form'),
)); ?>

	<div class="row">
		<div class="col-md-2">
			<?php echo $form->label($model, 'title'); ?>
		</div>
		<div class="col-md-10">
			<?php echo $form->textField($model, 'title', array('maxlength' => 255)); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<?php echo $form->label($model, 'catid'); ?>
		</div>
		<div class="col-md-10">
		<?php 
			$call_cats =BackendCategoriesModel::model()->findAll('published=:p ORDER BY position ASC', array(':p'=>1));
			$cats = CHtml::listData($call_cats, 'id', 'title_tree');
			echo $form->dropDownList($model,'catid', $cats, array('prompt'=>'&#8212;Tất cả&#8212;'));
		?>
		</div>
	</div>

	<div class="row buttons">
    	<?php echo GxHtml::submitButton(Yii::t('app', 'Search'), array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
