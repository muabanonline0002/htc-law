<?php
Yii::app()->getModule('news')->setTitle('Danh sách bài viết');
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('news-grid', {
		data: $(this).serialize()
	});
	return false;
});
$('#delete-all').on('click', function(){
	var data=[];
	$('input[name=\'rad_ID[]\']').each(function(){
		if(this.checked==true && this.value!='') data.push(this.value);
	})
	if(data.length>0){
		if(confirm('Bạn chắc chắn muốn xóa những đối tượng đã chọn?')){
			jQuery.ajax({
				url: '".Yii::app()->createUrl('/ajax/forceDelete')."',
				data: {id:data.join(','),model: 'BackendNewsModel'},
				type: 'post',
				dataType:'json',
				beforeSend: function(){
					$('#news-grid').addClass('grid-view-loading');
				},
				success: function(res){
					$.fn.yiiGridView.update('news-grid', {
						data: 'r=news%2Fnews%2Fadmin'
					});
				}
			})
		}
	}else{
		alert('Bạn phải chọn ít nhất 1 đối tượng để xóa!');
	}
})
");
?>
<div class="search-form" style="display: none;">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div>
<?php
$this->widget('application.widgets.iGridView', array(
	'id' => 'news-grid',
	'dataProvider' => $model->search(),
	'afterAjaxUpdate'=>"
		$('#filter-form input[type=\"radio\"]').on('ifChecked', function(event){
			$(this).closest('form').submit();
		});
	",
	'columns' => array(
		array(
			'header'=>CHtml::checkBox("all",false,array("id"=>"check_all","class"=>"icheck-all")),
			'value'=>'CHtml::checkBox("rad_ID[]",false,array("value"=>$data->id,"class"=>"icheck-blue"))',
			'type'=>'raw'
		),
		array(
			'name'	=>	'title',
			'type'	=>	'raw',
			'value'	=>	'CHtml::link(CHtml::encode($data->title), array("news/update","id"=>$data->id))'
		),
		array(
			'class'=>'JToggleColumn',
			'name'=>'pin', // boolean model attribute (tinyint(1) with values 0 or 1)
			'filter' => array('0' => 'No', '1' => 'Yes'), // filter
			'htmlOptions'=>array('width'=>'80')
		),
		array(
			'class'=>'JToggleColumn',
			'name'=>'status', // boolean model attribute (tinyint(1) with values 0 or 1)
			'filter' => array('0' => 'No', '1' => 'Yes'), // filter
			'htmlOptions'=>array('width'=>'80')
		),
		array(
				'name'	=>	'id',
				'htmlOptions'	=>	array(
						'width'	=>	'30',
				),
		),
		array(
			'class'=>'application.widgets.iButtonColumn',
			'htmlOptions'=>array('class'=>'actions'),
		),
	),
)); ?>