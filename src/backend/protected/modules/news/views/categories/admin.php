<?php
Yii::app()->getModule('news')->setTitle(Yii::t('main','Categories List'));
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('categories-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->
<?php $this->widget('application.widgets.iGridView', array(
	'id' => 'categories-grid',
	'dataProvider' => $model->search(),
	'columns' => array(
		array(
			'header'=>CHtml::checkBox("all",false,array("id"=>"check_all","class"=>"icheck-all")),
			'value'=>'CHtml::checkBox("rad_ID[]",false,array("value"=>$data->id,"class"=>"icheck-blue"))',
			'type'=>'raw'
		),
		array(
			'name'	=>	'title',
			'type'	=>	'raw',
			'value'	=>	'CHtml::link($data->title_tree,array("categories/update","id"=>$data->id))',
		),
		array(
			'name'	=>	'parent_id',
			'value'	=>	'($data->parent_id>0)?BackendCategoriesModel::model()->findByPk($data->parent_id)->title:""'
		),
		array(
			'header' =>Yii::t('app','Articles Number'),
			'value'=>'$data->newsCount',
			'htmlOptions'	=>	array(
				'width'	=>	'100',
				'align'	=>	'center',
			),
		),
		array(
			'name'	=>	'ordering',
			'htmlOptions'=>array(
				'width'	=>	60,
				'align'	=>	'center'
			)
		),
		array(
				'class'=>'JToggleColumn',
				'name'=>'published', // boolean model attribute (tinyint(1) with values 0 or 1)
				'filter' => array('0' => 'No', '1' => 'Yes'), // filter
				'htmlOptions'=>array('width'=>'80','align'=>'center')
		),
		array(
			'name'	=>	'id',
			'htmlOptions'	=>	array(
					'width'	=>	'30',
			)
		),
		array(
			'class'=>'application.widgets.iButtonColumn',
			'htmlOptions'=>array('class'=>'actions'),
		),
	),
)); ?>