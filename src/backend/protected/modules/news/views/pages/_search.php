<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
	'htmlOptions'=>array('name'=>'search-form','class'=>'basic-form inline-form'),
)); ?>

	<div class="row">
		<div class="col-md-2">
		<?php echo $form->label($model, 'title'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'title', array('maxlength' => 255)); ?>
		</div>
	</div>

	<div class="row buttons">
		<div class="col-md-2">
			<?php echo GxHtml::submitButton(Yii::t('app', 'Search'), array('class'=>'btn btn-primary')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
