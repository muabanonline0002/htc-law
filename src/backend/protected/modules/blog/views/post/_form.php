<div class="form">
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'backend-post-model-form',
	'enableAjaxValidation' => false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data', 'name'=>'post-form','class'=>'basic-form inline-form'),
));
?>
	<?php echo $form->errorSummary($model); ?>
		<div class="row">
		<div class="col-md-2">
			<label>Ảnh thumb</label>
		</div>
		<div class="col-md-10">
		<div id="img_tmp"><img onerror="this.src='/backend/images/no_image.png'" src="<?php echo AvatarHelper::getAvatarUrl($model->id, "s1","blog")?>" /></div>
		<?php $this->widget('ext.EAjaxUpload.EAjaxUpload',
			array(
			        'id'=>'uploadFile',
			        'config'=>array(
			               'action'=>Yii::app()->createUrl('/site/upload'),
			               'allowedExtensions'=>array("jpg"),//array("jpg","jpeg","gif","exe","mov" and etc...
			               'sizeLimit'=>10*1024*1024,// maximum file size in bytes
			               'minSizeLimit'=>1,// minimum file size in bytes
			               'onComplete'=>"js:function(id, fileName, responseJSON){ 
			        			$('#img_tmp').html('<img width=\"200\" src=\"".Yii::app()->params['storage_url']."/tmp/"."'+fileName+'\" />'); 
								$('#BackendPostModel_file').attr('value',fileName)
							}",
			               //'messages'=>array(
			               //                  'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
			               //                  'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
			               //                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
			               //                  'emptyError'=>"{file} is empty, please select files again without it.",
			               //                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
			               //                 ),
			               //'showMessage'=>"js:function(message){ alert(message); }"
			              )
			)); 
		?>
		<?php echo CHtml::hiddenField('BackendPostModel[file]')?>
		</div>
		</div>
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'title'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'title', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'title'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'intro_text'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textArea($model, 'intro_text'); ?>
		<?php echo $form->error($model,'intro_text'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo CHtml::label('Topic', 'topic'); ?>
		</div>
		<div class="col-md-10">
		<?php 
			$data = CHtml::listData(BackendTopicModel::model()->published()->findAll(), 'id', 'name');
			$select = array();
			if(isset($topic)){
				foreach ($topic as $topicId){
					$select[]=$topicId['topic_id'];
				}
			}
			echo CHtml::checkBoxList('checkbox-list', $select, $data, array('class'=>'icheck-blue'));
		?>
		<?php echo $form->error($model,'intro_text'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'content'); ?>
		</div>
		<div class="col-md-10">
		<?php
				$this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
					'model' => $model,
					'attribute' => 'content',
					'options' => array(
						'buttons'=>array('html','formatting','fontfamily','fontcolor',
							'bold', 'italic', 'underline', 'deleted','unorderedlist','orderedlist','outdent','indent','link','alignment'
						,'horizontalrule','image'
						),
						'lang' => 'en',
						'imageUpload'=> Yii::app()->createUrl('/upload/fileUpload'),
						'imageUploadErrorCallback'=>'js:function(json){ alert(json.error); }',
						//'fileUpload'=>$this->createUrl('fileUpload'),
						//'fileUploadErrorCallback'=>'js:function(obj, json){ alert(json.error); }',
						'counterCallback'=>'js:function(data){
						var counter_text = data.words+" words"+", "+data.characters+" characters";
						$("#full_text_counter").html(counter_text);
						}
					',
						'imageManagerJson'=>Yii::app()->createUrl('/upload/fileManager'),
						'minHeight'=>300,
						'placeholder'=>Yii::t('main','Nhập nội dung')
					),
					'plugins' => array(
						'fontfamily'=>array(
							'js'=>array('fontfamily.js')
						),
						'fontcolor'=>array(
							'js'=>array('fontcolor.js')
						),
						'fontsize'=>array(
							'js'=>array('fontsize.js')
						),
						'counter'=>array(
							'js'=>array('counter.js')
						),
						'cleantext'=>array(
							'js'=>array('cleantext.js')
						),
						'clips' => array(
							// You can set base path to assets
							//'basePath' => 'application.components.imperavi.my_plugin',
							// or url, basePath will be ignored.
							// Defaults is url to plugis dir from assets
							//'baseUrl' => '/js/my_plugin',
							'css' => array('clips.css',),
							'js' => array('clips.js',),
							// add depends packages
							'depends' => array('imperavi-redactor',),
						),
						'imagemanager' => array(
							'js' => array('imagemanager.js'),
						),
						'video'=>array(
							'js'=>array('video.js')
						),
						'table'=>array(
							'js'=>array('table.js')
						),
						/*'definedlinks'=>array(
                            'js'=>array('definedlinks.js')
                        ),*/
						'fullscreen' => array(
							'js' => array('fullscreen.js',),
						),
					),
				));
				?>
				<div id="full_text_counter"></div>
			<?php echo $form->error($model,'content'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'tags'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textArea($model, 'tags'); ?>
		<?php echo $form->error($model,'tags'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'status'); ?>
		</div>
		<div class="col-md-10">
		<?php 
		$data = array(
				BackendPostModel::STATUS_PUBLISHED => 'Public',
				BackendPostModel::STATUS_DRAFT => 'Draft',
				BackendPostModel::STATUS_ARCHIVED => 'Archived'
		);
		echo $form->dropDownList($model, 'status', $data);
		?>
		<?php echo $form->error($model,'status'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-2">
		<?php echo $form->labelEx($model,'author_id'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->dropDownList($model, 'author_id', GxHtml::listDataEx(UserWebModel::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'author_id'); ?>
		</div>
		</div><!-- row -->
		<div class="row">
		<div class="col-md-12">
			<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save');?></button>
			<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save & Continue');?></button>
			<button type="button" onclick="window.location.href='<?php echo Yii::app()->createUrl('/news/news/admin');?>'" class="btn btn-sm btn-default" ><i class="fa fa-close"></i>&nbsp;<?php echo Yii::t('main','Close');?></button>
		</div>
	</div>
<?php
$this->endWidget();
?>
</div><!-- form -->