<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
	'htmlOptions'=>array('name'=>'blog-posts-search','class'=>'basic-form inline-form'),
)); ?>

	<div class="row">
		<div class="col-md-2">
		<?php echo $form->label($model, 'title'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->textField($model, 'title', array('maxlength' => 128)); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-2">
		<?php echo $form->label($model, 'author_id'); ?>
		</div>
		<div class="col-md-10">
		<?php echo $form->dropDownList($model, 'author_id', GxHtml::listDataEx(UserWebModel::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
		</div>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('main', 'Search'), array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
