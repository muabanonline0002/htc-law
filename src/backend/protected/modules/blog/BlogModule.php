<?php

class BlogModule extends CWebModule
{
	public $title="Quản lý blog";
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'blog.models.*',
			'blog.components.*',
		));
	}
	public function setTitle($title)
	{
		$this->title = $title;
	}
	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
