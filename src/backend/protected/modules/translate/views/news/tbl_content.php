<?php
$this->renderPartial('..//filterTranslate/_form');
?>
    <div class="row">
        <button class="btn btn-sm btn-danger pull-right"
                onclick="CoreJs.deleteAll('<?php echo Yii::app()->createUrl('/translate/filterTranslate/delMulti'); ?>','<?php echo Yii::t('app', 'Are you sure you want to delete item?'); ?>')">
            <i class="fa fa-trash-o"></i>&nbsp;<?php echo Yii::t('main', 'Delete Translate'); ?></button>
    </div>
<?php

$total = BackendTranslatesModel::getTotalData($paramsTrans['language'], 'tbl_content', 'id', 'type="post"');
$data = BackendTranslatesModel::GetListTransNews($paramsTrans['language']);
$model = new CSqlDataProvider($data, array( //or $model=new CArrayDataProvider($rawData, array(... //using with querAll...
    'keyField' => 'id',
    'totalItemCount' => $total,

    'sort' => array(
        'attributes' => array(
            'id', 'title'
        ),
        'defaultOrder' => array(
            'id' => CSort::SORT_DESC, //default sort value
        ),
    ),
    'pagination' => array(
        'pageSize' => 10,
    ),
));
?>
<?php
$this->widget('application.widgets.iGridView', array(
    'id' => 'news-grid',
    'dataProvider' => $model,
    'columns' => array(
        array(
            'header' => CHtml::checkBox("all", false, array("id" => "check_all", "class" => "icheck-all")),
            'value' => 'CHtml::checkBox("rad_ID[]",false,array("value"=>$data["tid"],"class"=>"icheck-blue"))',
            'type' => 'raw'
        ),
        array(
            'header' => 'ID',
            'name' => 'id',
        ),
        array(
            'header' => 'Title',
            'name' => 'title',
            'value' => '$data["title"]'
        ),
        array(
            'header' => 'Title (' . $languages[$paramsTrans['language']] . ')',
            'name' => 'title',
            'value' => 'BackendTranslatesModel::getContentByLang($data["trans_content"])'
        ),
        array(
            'header' => 'Action',
            'value' => 'BackendTranslatesModel::getActionCreateUpdate($data)',
            'type' => 'raw'
        ),
        array(
            'header' => 'Status',
            'value' => 'BackendTranslatesModel::getStatusTranslate($data)',
            'type' => 'raw'
        ),
    ),
)); ?>