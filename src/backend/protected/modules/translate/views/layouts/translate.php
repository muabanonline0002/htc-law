<?php
$this->beginContent('application.views.layouts.body');
?>
<div class="content">
    <div class="inner">
        <div class="row border-bottom">
        	<div class="col-lg-12">
                <h3><?php echo Yii::app()->getModule('translate')->title;?></h3>
            </div>
        </div>
        <?php echo $content;?>
    </div>
</div>
<?php
$this->endContent();
?>