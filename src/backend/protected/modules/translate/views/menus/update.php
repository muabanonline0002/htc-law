<?php
$params_filter = Yii::app()->user->getState('paramsTrans');
$languages = Yii::app()->params['languages'];
$trans_elements = @unserialize($data['trans_content']);
$trans_elements = is_array($trans_elements)?$trans_elements:false;
?>

<div class="form">
<div><?php echo Yii::app()->user->getFlash('msg');?></div>
<form method="post" action="" id="menus-translate-form" class="basic-form inline-form">
<div class="row">
<div class="col-md-2">
<label><?php echo Yii::t('app','Published')?></label>
</div>
<div class="col-md-10">
<input type="checkbox" name="t_published" class="icheck-blue" <?php if($data && $data['published']==1) echo 'checked';?>/>
</div>
</div>
<div class="row">
<div class="col-md-2">
<label><?php echo Yii::t('app','Title')?></label>
</div>
<div class="col-md-10">
<input class="textField-l" readonly="readonly" disabled="disabled" type="text" name="title" value="<?php echo $data['name'];?>" />
</div>
</div>
<div class="row">
<div class="col-md-2">
<label><?php echo Yii::t('app','Title').'&nbsp('.$languages[$params_filter['language']].')';?></label>
</div>
<div class="col-md-10">
<input class="textField-l" type="text" name="BackendTranslatesModel[name]" value="<?php if($trans_elements) echo $trans_elements['name'];?>" />
</div>
</div>

<div class="row">
<div class="col-md-2">
<label><?php echo Yii::t('app','Alias');?></label>
</div>
<div class="col-md-10">
<input class="textField-l" disabled="disabled" type="text" name="alias" value="<?php echo $data['alias'];?>" />
</div>
</div>
<div class="row">
<div class="col-md-2">
<label><?php echo Yii::t('app','Alias').'&nbsp('.$languages[$params_filter['language']].')'?></label>
</div>
<div class="col-md-10">
<input class="textField-l" type="text" name="BackendTranslatesModel[alias]" value="<?php if($trans_elements) echo $trans_elements['alias'];?>" />
</div>
</div>
<div class="row">
	<div class="col-md-12">
		<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save');?></button>
		<button type="submit" class="btn btn-sm btn-primary" ><i class="fa fa-save"></i>&nbsp;<?php echo Yii::t('main','Save & Continue');?></button>
		<button type="button" onclick="window.location.href='<?php echo Yii::app()->createUrl('/translate/filterTranslate/Filterlayout');?>'" class="btn btn-sm btn-default" ><i class="fa fa-close"></i>&nbsp;<?php echo Yii::t('main','Close');?></button>
	</div>
</div>
<input type="hidden" name="tt_id" value="<?php echo $data['tid'];?>" />
</form>
</div>
