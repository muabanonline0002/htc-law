<?php

Yii::import('common.models.db.PagesModel');

class BackendPagesModel extends PagesModel
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public static function updateTree()
	{
		$binarytree = new BinaryTree('id', 'title', 'parent', 'tbl_content', '','','ordering' );
		$arr_tree = $binarytree->getTreeResult();
		if(count($arr_tree)>0){
			$order = array();
			foreach($arr_tree as $key => $value){
				$order[]=$key;
			}
			foreach($order as $key => $item){
				self::updateLevelCategory($item, $key+1, $arr_tree[$item]['treename']);
			}
		}
	}
	public static function updateLevelCategory($id, $order, $treename)
	{
		$sql="UPDATE tbl_content 
			  SET position = $order, page_tree = :p
			  WHERE id = :id
			";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(':p', $treename, PDO::PARAM_STR);
		$command->bindParam(':id', $id, PDO::PARAM_INT);
		return $command->query();
	}
	public static function getPageByAlias($alias)
	{
		$sql = "SELECT * 
				FROM tbl_content
				WHERE alias = :p AND type='page'
				";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(':p',$alias,PDO::PARAM_STR);
		return $command->queryRow();
	}
	public function beforeSave()
	{
		$this->modified = new CDbExpression('NOW()');
		$this->modified_by = Yii::app()->user->id;
		$this->type = 'page';
		return parent::beforeSave();
	}
	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('catid', $this->catid);
		$criteria->compare('parent', $this->parent);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('alias', $this->alias, true);
		$criteria->compare('alias_en', $this->alias_en, true);
		$criteria->compare('page_tree', $this->page_tree, true);
		$criteria->compare('thumb', $this->thumb, true);
		$criteria->compare('introtext', $this->introtext, true);
		$criteria->compare('fulltext', $this->fulltext, true);
		$criteria->compare('type', $this->type, true);
		$criteria->compare('views', $this->views);
		$criteria->compare('status', $this->status);
		$criteria->compare('params', $this->params, true);
		$criteria->compare('images', $this->images, true);
		$criteria->compare('language', $this->language, true);
		$criteria->compare('translate_key', $this->translate_key, true);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('modified', $this->modified, true);
		$criteria->compare('modified_by', $this->modified_by);
		$criteria->compare('ordering', $this->ordering);
		$criteria->compare('position', $this->position);
		$criteria->compare('metakey', $this->metakey, true);
		$criteria->compare('metadesc', $this->metadesc, true);
		$criteria->compare('comment', $this->comment);
		$criteria->addInCondition('status', array(0,1));
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}