<?php

Yii::import('common.models.db.GalleryItemsModel');

class BackendGalleryItemsModel extends GalleryItemsModel
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function beforeSave()
	{
		$this->updated_time = new CDbExpression('NOW()');
		if($this->isNewRecord){
			$this->created_datetime = new CDbExpression('NOW()');
		}
		return parent::beforeSave();
	}
	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'catid' => Yii::t('app', 'Album'),
			'title' => Yii::t('app', 'Title'),
			'description' => Yii::t('app', 'Description'),
			'image' => Yii::t('app', 'Image'),
			'update_time' => Yii::t('app', 'Update Time'),
		);
	}
	public static function getType($catId)
	{
		switch ($catId) {
			case 1:
				return 'slideshow';
				break;
			case 12:
				return 'partner';
				break;
			default:
				return 'gallery';
				break;
		}
	}
}