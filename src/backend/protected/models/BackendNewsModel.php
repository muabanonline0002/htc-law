<?php

Yii::import('common.models.db.ContentModel');

class BackendNewsModel extends ContentModel
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function rules() {
		return CMap::mergeArray(parent::rules(),array(
				array('alias', 'required'),
		));
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'author' => array(self::BELONGS_TO, 'iUsers', 'created_by'),
				'comments' => array(self::HAS_MANY, 'BackendCommentModel', 'id', 'condition'=>'tbl_comment.status='.BackendCommentModel::STATUS_APPROVED, 'order'=>'tbl_comment.create_time DESC'),
				'commentCount' => array(self::STAT, 'BackendCommentModel', 'post_id', 'condition'=>'status='.BackendCommentModel::STATUS_APPROVED),
		);
	}
	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
	}
	public function beforeSave()
	{
		$this->modified = new CDbExpression('NOW()');
		return parent::beforeSave();
	}
	public function attributeLabels() {
		return array(
			'id' => Yii::t('module_new', 'ID'),
			'catid' => Yii::t('app', 'Category'),
			'parent' => Yii::t('app', 'Parent'),
			'title' => Yii::t('module_news', 'Title'),
			'alias' => Yii::t('app', 'Alias'),
			'alias_en' => Yii::t('app', 'Alias En'),
			'page_tree' => Yii::t('app', 'Page Tree'),
			'thumb' => Yii::t('app', 'Thumb'),
			'introtext' => Yii::t('app', 'Introtext'),
			'fulltext' => Yii::t('app', 'Fulltext'),
			'type' => Yii::t('app', 'Type'),
			'views' => Yii::t('app', 'Views'),
			'status' => Yii::t('app', 'Status'),
			'params' => Yii::t('app', 'Params'),
			'images' => Yii::t('app', 'Images'),
			'language' => Yii::t('app', 'Language'),
			'translate_key' => Yii::t('app', 'Translate Key'),
			'created' => Yii::t('app', 'Created'),
			'created_by' => Yii::t('app', 'Created By'),
			'modified' => Yii::t('app', 'Modified'),
			'modified_by' => Yii::t('app', 'Modified By'),
			'ordering' => Yii::t('app', 'Ordering'),
			'position' => Yii::t('app', 'Position'),
			'metakey' => Yii::t('app', 'Metakey'),
			'metadesc' => Yii::t('app', 'Metadesc'),
			'comment' => Yii::t('app', 'Comment'),
		);
	}
	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('catid', $this->catid);
		$criteria->compare('parent', $this->parent);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('alias', $this->alias, true);
		$criteria->compare('alias_en', $this->alias_en, true);
		$criteria->compare('page_tree', $this->page_tree, true);
		$criteria->compare('thumb', $this->thumb, true);
		$criteria->compare('introtext', $this->introtext, true);
		$criteria->compare('fulltext', $this->fulltext, true);
		$criteria->compare('type', $this->type, true);
		$criteria->compare('views', $this->views);
		$criteria->compare('status', $this->status);
		$criteria->compare('params', $this->params, true);
		$criteria->compare('images', $this->images, true);
		$criteria->compare('language', $this->language, true);
		$criteria->compare('translate_key', $this->translate_key, true);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('modified', $this->modified, true);
		$criteria->compare('modified_by', $this->modified_by);
		$criteria->compare('ordering', $this->ordering);
		$criteria->compare('position', $this->position);
		$criteria->compare('metakey', $this->metakey, true);
		$criteria->compare('metadesc', $this->metadesc, true);
		$criteria->compare('comment', $this->comment);
		$criteria->addInCondition('status', array(0,1));
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}