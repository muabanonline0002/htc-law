<?php
return CMap::mergeArray(
	require_once dirname(__FILE__).'../../../common/config/common.php',
	array(
	    'basePath'=>dirname(__FILE__).DS.'..',
	    'name'=>'Ginseng Console',
	    'import'=>array(
	        'common.models.db.*',
	        'console.components.*',
	    	'console.models.*',
            'common.vendors.utilities.*',
            'common.components.*',
            'common.vendors.yii-mail.YiiMailMessage',
	    ),
	    'components' => array(
	 		'mail' => array(
	 			'class' => 'common.vendors.yii-mail.YiiMail',
	 			'transportType' => 'smtp',
			    'transportOptions' => array(
			        'host' => 'smtp.gmail.com',
			        'username' => 'hongsamokkorea@gmail.com',
			        'password' => 'okkoreahongsam',
			        'port' => '465',
			        'encryption'=>'tls',
			    ),
	 			'viewPath' => 'application.views.mail',
	 			'logging' => true,
	 			'dryRun' => false
	 		),
	 	),
	 	'params'=>array(
	 		'adminEmail'=>'hongsamokkorea@gmail.com'
	 	)
	)
);