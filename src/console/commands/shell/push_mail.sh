#!/bin/bash
SCRIPT=`readlink -f $0`
SCRIPTPATH=`dirname $SCRIPT`

count=`ps axu | grep push_mail.sh | grep -v "grep" |wc -l`
if [ $count -ge 3  ] ; then
	echo "Service is running"
	exit 1
fi
php $SCRIPTPATH/../../../console.php Notification sendMail