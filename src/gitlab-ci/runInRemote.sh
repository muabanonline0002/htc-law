#!/bin/bash
HA_IP="103.98.160.4"
echo "${SSH_HA_PEM}" > $(pwd)/ha.pem
echo "ssh -i $(pwd)/ha.pem centos@${HA_IP}"
chmod 600 $(pwd)/ha.pem
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i $(pwd)/ha.pem centos@${HA_IP} \
	export TARGET_BRANCH="MASTER" \
	"$(cat $(pwd)/src/gitlab-ci/deploy_step/pullCode.sh)"