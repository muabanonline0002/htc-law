<?php

Yii::import('common.models.db._base.BaseShopOrderInfoModel');

class ShopOrderInfoModel extends BaseShopOrderInfoModel
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function getOrdersToPush()
	{
		$crit = new CDbCriteria();
		$crit->condition = "status=0";
		$result = self::model()->findAll($crit);
		return $result;
	}
}