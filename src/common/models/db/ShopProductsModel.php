<?php

Yii::import('common.models.db._base.BaseShopProductsModel');

class ShopProductsModel extends BaseShopProductsModel
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function relations()
	{
		return array(
			'variations' => array(self::HAS_MANY, 'ProductVariation', 'product_id', 'order' => 'position'),
			'orders' => array(self::MANY_MANY, 'Order', 'ShopProductOrder(order_id, product_id)'),
			'category' => array(self::BELONGS_TO, 'ShopCategoryModel', 'category_id'),
			'tax' => array(self::BELONGS_TO, 'Tax', 'tax_id'),
			'images' => array(self::HAS_MANY, 'Image', 'product_id'),
			'imagesCount'=> array(self::STAT, 'Image', 'product_id'),
			'shopping_carts' => array(self::HAS_MANY, 'ShoppingCart', 'product_id'),
			'statistic'=>array(self::BELONGS_TO, 'ShopProductStatisticModel', 'product_id'),
		);
	}
}