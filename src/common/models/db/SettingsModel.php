<?php

Yii::import('common.models.db._base.BaseSettingsModel');

class SettingsModel extends BaseSettingsModel
{
	const SYSTEM = 0;
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function beforeSave()
	{
		$this->updated_time = new CDbExpression('NOW()');
		if($this->isNewRecord){
			$this->created_time = new CDbExpression('NOW()');
		}
		return parent::beforeSave();
	}
	public static function getSetting($key)
	{
		return self::model()->findByAttributes($key)->value_code;
	}
	public static function setSetting($key, $value)
	{
		$model = self::model()->findByPk($key);
		if($model){
			$model->setAttribute('value_code', $value);
			return $model->save(false);
		}
		return false;
	}
	public static function getAll()
	{
		$list = self::model()->findAll();
		$results = array();
		foreach ($list as $item){
			$results[$item->key_code] = $item;
		}
		return $results;
	}
	public static function getSettingAll()
	{
		$list = self::model()->findAll();
		$results = array();
		foreach ($list as $item){
			$results[$item->key_code] = $item->value_code;
		}
		return $results;
	}
}