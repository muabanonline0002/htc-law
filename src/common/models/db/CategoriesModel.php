<?php

Yii::import('common.models.db._base.BaseCategoriesModel');

class CategoriesModel extends BaseCategoriesModel
{
    const PUBLISHED = 1;
    const UNPUBLISHED = 0;
    const DELETEED = 2;

    public function tableName()
    {
        return 'tbl_categories';
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        return array('translate' => 'common.components.STranslateableBehavior');
    }

    public function translate()
    {
        // List of model attributes to translate
        return array('title', 'alias'); //Example
    }

    public function rules()
    {
        $rules = parent::rules();
        return CMap::mergeArray($rules, array(
            array('alias', 'unique', 'message' => 'Alias chủ đề này đã tồn tại! Vui lòng nhập alias khác.'),
        ));
    }

    public function relations()
    {
        return array(
            'PostsCount' => array(self::STAT, 'WebArticleModel', 'catid'),
        );
    }

    public function getNumberPosts()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'category_id=:cid';
        $criteria->params = array('cid' => $this->id);
        return ContentCategoriesModel::model()->count($criteria);
    }

    public static function getIdByAlias($alias, $field = 'alias')
    {
        $categories = self::model()->findByAttributes(array($field => $alias));
        if ($categories) return $categories->id;
        return false;
    }

    public static function getCategoryNamByAlias($alias)
    {
        $categories = self::model()->findByAttributes(array('alias' => $alias));
        if ($categories) return $categories->title;
        return false;
    }

    public function getCountByParent($parentId)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'parent_id=:pid and published=:s';
        $status = self::PUBLISHED;
        $criteria->params = array(':pid' => $parentId, ':s' => $status);
        $dependency = new CDbCacheDependency("SELECT MAX(modified) FROM tbl_categories");
        return self::model()->cache(Yii::app()->params['cache_time'], $dependency)->count($criteria);
    }

    public function getListByParent($parentId, $limit = 10, $offset = 0, $order = "")
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'parent_id=:pid and published=:s';
        $status = self::PUBLISHED;
        $criteria->params = array(':pid' => $parentId, ':s' => $status);
        if (!empty($order)) {
            $criteria->order = "{$order}";
        }
        $criteria->offset = $offset;
        $criteria->limit = $limit;
        $dependency = new CDbCacheDependency("SELECT MAX(modified) FROM tbl_categories");
        return self::model()->cache(Yii::app()->params['cache_time'], $dependency)->findAll($criteria);
    }
}