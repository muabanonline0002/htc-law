<?php

Yii::import('common.models.db._base.BaseGalleryItemsModel');

class GalleryItemsModel extends BaseGalleryItemsModel
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category'	=> 	array(self::BELONGS_TO, 'GalleryCategoryModel', 'catid'),
		);
	}
	public function getCountByCat($catId)
	{
		$crit = new CDbCriteria();
		$crit->condition = 'status=1 AND catid=:id';
		$crit->params = array(':id' => $catId);
		return self::model()->count($crit);
	}
	public function getItemsByCat($catId, $limit=10,$offset=0)
	{
		$crit = new CDbCriteria();
		$crit->condition = 'status=1 AND catid=:id';
		$crit->params = array(':id' => $catId);
		$crit->order=" ordering asc, id desc ";
		$crit->limit = $limit;
		$crit->offset = $offset;
		return self::model()->findAll($crit);
	}
}