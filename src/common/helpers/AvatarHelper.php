<?php
Yii::import('common.vendors.utilities.Image');
class AvatarHelper {

	public static function processAvatar($id, $source, $type = "blog") {

		$fileSystem = new Filesystem();
		$alowSize = Yii::app()->params['imageSize']["$type"];
		$maxSize = max($alowSize);
		$folderMax = "s0";
		$pathDir = Yii::app()->params[$type.'_path'];
		foreach ($alowSize as $folder => $size) {
			// Create folder by ID
			$avatarPath = self::getAvatarPath($id, $folder, true, $pathDir);
			$fileSystem->mkdirs($avatarPath);
			@chmod($avatarPath, 0777);

			// Get link file by ID
			$savePath[$folder] = self::getAvatarPath($id, $folder, false, $pathDir);
			if ($size == $maxSize) {
				$folderMax = $folder;
			}
		}

		// Delete file if exists
		if (file_exists($savePath[$folder])) {
			$fileSystem->remove($savePath);
		}

		if (file_exists($source)) {
			list($width, $height) = getimagesize($source);
			$imgCrop = new ImageCrop($source, 0, 0, $width, $height);

			// aspect ratio for image size
			$aspectRatioW = $aspectRatioH = 1;

			foreach ($savePath as $k => $v) {
				$desWidth = $alowSize[$k];
				$desHeight = round($alowSize[$k] * intval($aspectRatioH) / intval($aspectRatioW));
				if (file_exists($v) && is_file($v)) {
					@unlink($v);
				}

				if ($k == $folderMax) {
					$imgCrop->resizeRatio($v, $desWidth, $desHeight, 100);
				} else {
					$imgCrop->resizeCrop($v, $desWidth, $desHeight, 100);
				}
			}
			//remove file source
			$fileSystem->remove($source);
		}
	}

	public static function getAvatar($type, $id, $size = null) {
		$src = Common::storageSolutionEncode($id) . $id . ".jpg";
		$dir = "s0";
		$alowSize = Yii::app()->params['imageSize'];

		if(is_numeric($size)){
			foreach ($alowSize as $folder => $s) {
				$dir = $folder;
				if ($s >= $size)
					break;
			}
		}else{
			$dir = $size;
		}


		if ($type == "video") {
			$dir = "img/" . $dir;
			$configName = "videoImageUrl";
		} else {
			$configName = $type . "Url";
		}
		return Yii::app()->params['storage'][$configName] . "/" . $dir . "/" . $src;
	}

	public static function getAvatarJs() {
		$rs = 'var avatarPrefixUrl={};';

		foreach (Yii::app()->params['storage'] as $type => $prefix) {
			if (!strpos($type, 'Dir')){
				$rs.='avatarPrefixUrl["' . $type . '"]="' . $prefix . '";';
			}
		}
		$rs.='function avatarObject(type,id){
                    if(type === "video"){
                        type = "videoImage";
                    }
    	            var result="",level= 0;
    	    		while(true){
    	                var shift   = 13*level;
    	                var layerName  = shift<=32?id >> shift:0;
    	        		if(layerName == 0) break;
    	                result = layerName+"/"+result;
    	                level++;
    	    		}
                    return avatarPrefixUrl[type+"Url"]+(type=="videoImage" ? "/img/s3/" : "s3/") + result+id + ".jpg";
        	}';
		return $rs;
	}
	public static function getAvatarPath($id,$size=150,$isFolder = false, $pathDir='')
	{
		if(!isset($id)) $id = 0;
		if($isFolder){
			$savePath = StorageHelper::storageSolutionEncode($id);
		}else{
			$savePath = StorageHelper::storageSolutionEncode($id).$id.".jpg";
		}
		$savePath = StorageHelper::storageSolutionEncode($id).$id.".jpg";
		$path = $pathDir.DS.$size.DS.$savePath;
		return $path;
	}
	
	public static function getAvatarUrl($id=null, $size="s1", $type="blog")
	{
		$pathUrl = Yii::app()->params[$type."_url"];
		if(!isset($id)) $id = 0;
		return $pathUrl.'/'.$size.'/'.$id.'/'.$id.".jpg?v=".time();
	}
	public static  function getSavedName($itemId, $itemPerDir = 1024) {
		$dir1 = floor($itemId / ($itemPerDir * $itemPerDir * $itemPerDir));
		$dir2 = floor(($itemId - $dir1 * $itemPerDir * $itemPerDir * $itemPerDir) / ($itemPerDir * $itemPerDir));
		$temp = floor($itemId / ($itemPerDir * $itemPerDir));
		$dir3 = floor(($itemId - $temp * $itemPerDir * $itemPerDir) / $itemPerDir);
		$path = $dir1 . "/" . $dir2 . "/" . $dir3;
		return $path;
	}
	public static function processThumb($contentId, $filePath, $type='news')
	{
		if(file_exists($filePath)){

			list($width, $height) = getimagesize($filePath);
            $storage = Yii::app()->params['storage'];
			$filePathType = $storage.$type.DS.$contentId;
            $imgSize = Yii::app()->params['posts_images'][$type]['size'];
            $result = array();
            $fileSystem = new Filesystem();
            $v = StorageHelper::storageSolutionEncode($contentId);
            $originPath = $filePathType.DS.'origin'.DS.$v.DS;
            $fileSystem->mkdirs($originPath);
			@chmod($originPath, 0777);
			/*$imageRsOrigin = new Image($filePath);
			$imageRsOrigin->save($originPath.$contentId.'.jpg');*/
			$fileSystem->copy($filePath,$originPath.$contentId.'.jpg');
            foreach ($imgSize as $key => $value) {
                
            	$desWidth = (int) $value['w'];
                $desHeight = $value['h'];
                $quality = Yii::app()->params['posts_images']['quality'];

                $filePathDest = $filePathType.DS.$key.DS.$v.DS;
                $filePathDestFull = $filePathDest.$contentId.'.jpg';
                $fileSystem->mkdirs($filePathDest);
				@chmod($filePathDest, 0777);
                if(file_exists($filePathDestFull)){
                	//unlink($filePathDestFull);
					$fileSystem->remove($filePathDestFull);
                }
                $imageRs = new Image($filePath);
                $imageRs->setQuality(100);
                if($width>$desWidth){
	                //$imageRs->resize($desWidth,$desHeight);
	                $imageRs->resizeToWidth($desWidth);
                }
                $imageRs->save($filePathDestFull);
            }
            return $result;
		}
		return;
	}
	public static function getThumbPath($contentId, $type='news', $size='sm')
	{
		$storagePath = Yii::app()->params['storage'];
		$v = StorageHelper::storageSolutionEncode($contentId);
		$path = $storagePath.$type.DS.$contentId.DS.$size.$v;
		return $path;
	}
	public static function getThumbUrl($contentId, $type='news', $size='sm')
	{
		$storageUrl = Yii::app()->params['storage_url'];
		$v = StorageHelper::storageSolutionEncode($contentId);
		$url = $storageUrl.'/'.$type.'/'.$contentId.'/'.$size.'/'.$v.$contentId.'.jpg';
		return $url;
	}
}