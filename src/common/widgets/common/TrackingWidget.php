<?php
class TrackingWidget extends CWidget{
	public $uniqueId;
	public $cId;
	public function init()
	{
		$cs = Yii::app()->getClientScript();
        $cs->registerScript(__CLASS__.'#'.$this->uniqueId,"
        	setTimeout(function(){
            	$.ajax({
                    type:'POST',
                    data:{cid:".$this->cId."},
                    url:'/ajax/addViews',
                    dataType:'json',
                    success:function(res){
                        console.log('success',res);
                    }
                });
            },3000);
            ", CClientScript::POS_END);
		parent::init();
	}
}