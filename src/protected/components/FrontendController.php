<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class FrontendController extends CController
{
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/main';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    public $activemenu = null;
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    public function init()
    {
        $isPc = Yii::app()->request->getParam('is_pc');
        if ($isPc) {
            Yii::app()->user->setState('device', 'pc');
        }
        $device = Yii::app()->user->getState('device');
        /*if($device!='pc'){
            $isTouch = $this->isTouchLayout();
            if($isTouch){
                Yii::app()->user->setState('device','mobile');
                $uri = (strpos($_SERVER['REQUEST_URI'], '/')==0)?substr($_SERVER['REQUEST_URI'],1):$_SERVER['REQUEST_URI'];
                $this->redirect(WAP_SITE_URL.$uri);
                exit;
            }
        }*/
        $params = $_GET;
        if(Yii::app()->params['multilang']) {
            $cookieName = 'ipic_language_active';
            if(isset($params['lang']) && in_array($params['lang'], Yii::app()->params['languages'])) {
                $langActive = $params['lang'];
            } else {
                if(Common::hasCookie($cookieName)) {
                    $langActive = Common::getCookie($cookieName);
                } else {
                    $langActive = Yii::app()->params['language_default'];
                }
            }
            Yii::app()->language = $langActive;
            Common::setCookie($cookieName, $langActive);
        }
        $settings = Yii::app()->cache->get('setting');
        if ($settings === false) {
            $settings = SettingsModel::getSettingAll();
            $dependency = new CDbCacheDependency('SELECT MAX(updated_time) FROM settings');
            Yii::app()->cache->set('setting', $settings, Yii::app()->params['cache_time'], $dependency);
            Yii::app()->user->setState('setting', 0);
        }
        if ($settings && Yii::app()->user->getState('setting') != 1) {
            foreach ($settings as $key => $value) {
                Yii::app()->user->setState($key, $value);
            }
            Yii::app()->user->setState('setting', 1);
        }
        parent::init();
    }

    private function isTouchLayout()
    {
        $detect = new Mobile_Detect;
        //is mobile or tablet
        if ($detect->isMobile()) {
            return true;
        }
        return false;
    }

    /**
     * @see CController::createUrl()
     */
    public function createUrl($route, $params = array(), $ampersand = '&')
    {
        if ($route === '')
            $route = $this->getId() . '/' . $this->getAction()->getId();
        else if (strpos($route, '/') === false)
            $route = $this->getId() . '/' . $route;
        if ($route[0] !== '/' && ($module = $this->getModule()) !== null)
            $route = $module->getId() . '/' . $route;
        if (Yii::app()->params['multilang'] && !isset($params['lang'])) {
//            if (isset($_GET['lang']) && $_GET['lang'] != '' && $_GET['lang'] != Yii::app()->params['language_default']) {
//                $params['lang'] = $_GET['lang'];
//            }
//            $params['lang'] = 'en';
            $langActive = Yii::app()->language;
            if($langActive !== Yii::app()->params['language_default']) {
                $params['lang'] = $langActive;
            }
        }
        return Yii::app()->createUrl(trim($route, '/'), $params, $ampersand);
    }

    /**
     * @see CController::createUrl()
     */
    public function createUrlHome($params = array(), $ampersand = '&')
    {
        $route = '/site/index';
        if ($route === '')
            $route = $this->getId() . '/' . $this->getAction()->getId();
        else if (strpos($route, '/') === false)
            $route = $this->getId() . '/' . $route;
        if ($route[0] !== '/' && ($module = $this->getModule()) !== null)
            $route = $module->getId() . '/' . $route;
        if (Yii::app()->params['multilang'] && !isset($params['lang'])) {
            if (isset($_GET['lang']) && $_GET['lang'] != '' && $_GET['lang'] != Yii::app()->params['language_default']) {
                $params['lang'] = $_GET['lang'];
            }
        }
        return Yii::app()->createUrl(trim($route, '/'), $params, $ampersand);
    }

    /**
     * Create Url Article
     * @param Int $postId
     * @param String $route
     * @param array $params
     * @param String $ampersand
     */
    public function createUrlArticle($postId, $route = '/posts/view', $params = array(), $ampersand = '&')
    {
        if ($route === '')
            $route = $this->getId() . '/' . $this->getAction()->getId();
        else if (strpos($route, '/') === false)
            $route = $this->getId() . '/' . $route;
        if ($route[0] !== '/' && ($module = $this->getModule()) !== null)
            $route = $module->getId() . '/' . $route;
        //get param for create url
        $post = WebArticleModel::model()->published()->findByPk($postId);
        $params['id'] = $postId;
        $params['cat_url_key'] = CategoriesModel::model()->findByPk($post->catid)->alias;
        $params['url_key'] = $post->alias;
        //$params['year'] 	= date('Y', strtotime($post->created));
        //$params['month'] 	= date('m', strtotime($post->created));
        if (Yii::app()->params['multilang'] && !isset($params['lang'])) {
            $languageActive = Yii::app()->language;
            if($languageActive != Yii::app()->params['language_default']) {
                $params['lang'] = $languageActive;
            }
        }
        return Yii::app()->createUrl(trim($route, '/'), $params, $ampersand);
    }

    public function createUrlProductCategory($id, $route = '/product/list', $params = array(), $ampersand = '&')
    {
        if ($route === '')
            $route = $this->getId() . '/' . $this->getAction()->getId();
        else if (strpos($route, '/') === false)
            $route = $this->getId() . '/' . $route;
        if ($route[0] !== '/' && ($module = $this->getModule()) !== null)
            $route = $module->getId() . '/' . $route;
        //get param for create url
        $title = WebShopCategoryModel::model()->findByPk($id)->title;
        $alias = StringCommon::str_normalizer($title);
        $params['id'] = $id;
        $params['url_key'] = $alias;
        if (Yii::app()->params['multilang'] && !isset($params['lang'])) {
            if (isset($_GET['lang']) && $_GET['lang'] != '' && $_GET['lang'] != Yii::app()->params['language_default']) {
                $params['lang'] = $_GET['lang'];
            }
        }
        return Yii::app()->createUrl(trim($route, '/'), $params, $ampersand);
    }

    /**
     * Create Url Category
     * @param int $postId
     * @param string $route
     * @param array $params
     * @param string $ampersand
     */
    public function createUrlArticleCategory($catId, $route = '/posts/category', $params = array(), $ampersand = '&')
    {
        if ($route === '')
            $route = $this->getId() . '/' . $this->getAction()->getId();
        else if (strpos($route, '/') === false)
            $route = $this->getId() . '/' . $route;
        if ($route[0] !== '/' && ($module = $this->getModule()) !== null)
            $route = $module->getId() . '/' . $route;
        //get param for create url
        $cat = CategoriesModel::model()->findByPk($catId);
        if ($cat) {
            $params['alias'] = $cat->alias;
        }
        //$params['id'] = $catId;
        if (Yii::app()->params['multilang'] && !isset($params['lang'])) {
//            if (isset($_GET['lang']) && $_GET['lang'] != '' && $_GET['lang'] != Yii::app()->params['language_default']) {
//                $params['lang'] = $_GET['lang'];
//            }
            $langActive = Yii::app()->language;
            if($langActive !== Yii::app()->params['language_default']) {
                $params['lang'] = $langActive;
            }
        }
        return Yii::app()->createUrl(trim($route, '/'), $params, $ampersand);
    }

    /**
     * Create Url Page Dynamic
     * @param Int $postId
     * @param String $route
     * @param array $params
     * @param String $ampersand
     */
    public function createUrlPage($pageId, $route = '/page/view', $params = array(), $ampersand = '&')
    {
        if ($route === '')
            $route = $this->getId() . '/' . $this->getAction()->getId();
        else if (strpos($route, '/') === false)
            $route = $this->getId() . '/' . $route;
        if ($route[0] !== '/' && ($module = $this->getModule()) !== null)
            $route = $module->getId() . '/' . $route;

        //get param for create url
        $page = FrontendPagesModel::model()->published()->findByPk($pageId);
        if ($page) {
            $language = Yii::app()->language;
            $field = ($language == Yii::app()->params['language_default']) ? "alias" : "alias_" . $language;
            $params['url_key_page'] = $page->alias;
            if (Yii::app()->params['multilang'] && !isset($params['lang'])) {
                if (isset($_GET['lang']) && $_GET['lang'] != '' && $_GET['lang'] != Yii::app()->params['language_default']) {
                    $params['lang'] = $_GET['lang'];
                }
            }
            return Yii::app()->createUrl(trim($route, '/'), $params, $ampersand);
        }
        return '';
    }

    public function createUrlProductDetail($productId, $route = '/product/detail', $params = array(), $ampersand = '&')
    {
        if ($route === '')
            $route = $this->getId() . '/' . $this->getAction()->getId();
        else if (strpos($route, '/') === false)
            $route = $this->getId() . '/' . $route;
        if ($route[0] !== '/' && ($module = $this->getModule()) !== null)
            $route = $module->getId() . '/' . $route;
        //get param for create url
        $title = FrontendShopProductsModel::model()->findByPk($productId)->title;
        $alias = StringCommon::str_normalizer($title);
        $params['id'] = $productId;
        $params['alias'] = $alias;
        if (Yii::app()->params['multilang'] && !isset($params['lang'])) {
            if (isset($_GET['lang']) && $_GET['lang'] != '' && $_GET['lang'] != Yii::app()->params['language_default']) {
                $params['lang'] = $_GET['lang'];
            }
        }
        return Yii::app()->createUrl(trim($route, '/'), $params, $ampersand);
    }

    public function createUrlProductCat($catId, $route = '/product/category', $params = array(), $ampersand = '&')
    {
        if ($route === '')
            $route = $this->getId() . '/' . $this->getAction()->getId();
        else if (strpos($route, '/') === false)
            $route = $this->getId() . '/' . $route;
        if ($route[0] !== '/' && ($module = $this->getModule()) !== null)
            $route = $module->getId() . '/' . $route;
        //get param for create url
        $title = WebShopCategoryModel::model()->findByPk($catId)->title;
        $alias = StringCommon::str_normalizer($title);
        $params['id'] = $catId;
        $params['alias'] = $alias;
        if (Yii::app()->params['multilang'] && !isset($params['lang'])) {
            if (isset($_GET['lang']) && $_GET['lang'] != '' && $_GET['lang'] != Yii::app()->params['language_default']) {
                $params['lang'] = $_GET['lang'];
            }
        }
        return Yii::app()->createUrl(trim($route, '/'), $params, $ampersand);
    }

    public function createUrlAskDetail($askId, $route = '/ask/detail', $params = array(), $ampersand = '&')
    {
        if ($route === '')
            $route = $this->getId() . '/' . $this->getAction()->getId();
        else if (strpos($route, '/') === false)
            $route = $this->getId() . '/' . $route;
        if ($route[0] !== '/' && ($module = $this->getModule()) !== null)
            $route = $module->getId() . '/' . $route;
        //get param for create url
        $title = FrontendAskModel::model()->findByPk($askId)->title;
        $alias = StringCommon::str_normalizer($title);
        $params['id'] = $askId;
        $params['alias'] = $alias;
        return Yii::app()->createUrl(trim($route, '/'), $params, $ampersand);
    }
}