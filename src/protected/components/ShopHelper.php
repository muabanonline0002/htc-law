<?php 
class ShopHelper{

	public static function getSubcategoryShop($data, $parentCat=0, $activeId=''){
		$rootCat = self::getSubParentCategory($data,$parentCat);
		if(count($rootCat)>0){
			if($parentCat>0){
				echo '<ul class="sub-menu clearfix">';
			}else{
				echo '<ul>';
			}
			
			foreach ($rootCat as $key => $item){
				$link = Yii::app()->controller->createUrl('/product/list',array('url_key'=>$item['alias'],'id'=>$item['category_id']));
				if($activeId==$item['category_id']){
					$actived = 'active';
				}else{
					$actived='';
				}
				if(self::hasSub($data, $item['category_id'])){
					$classHasSub = 'sub-category';
					echo '<li class="'.$actived.' '.$classHasSub.'" ><a href="'.$link.'" >'.$item['title'].'</a>';
					self::getSubcategoryShop($data,$item['category_id'], $activeId);
					echo '<i class="fa fa-angle-right"></i>';
					echo '</li>';
				}else{
					echo '<li class="'.$actived.'"><a href="'.$link.'" >'.$item['title'].'</a>';
					echo '</li>';
				}
				
			}
			echo '</ul>';
		}
	}
	public static function hasSub($data,$parentId=0)
	{
		foreach ($data as $key => $value) {
			if($value->parent_id==$parentId){
				return true;
			}
		}
		return false;
	}
	public static function getSubParentCategory($data, $parentId)
	{
		$result = array();
		foreach ($data as $key => $value) {
			if($value->parent_id==$parentId){
				$result[] = array(
					'title'=>$value->title,
					'alias'=>$value->alias,
					'category_id'=>$value->category_id
				);
			}
		}
		return $result;
	}
}
?>