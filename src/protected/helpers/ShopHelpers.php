<?php
class ShopHelpers{
	public static function getImageProduct($image, $size='full')
	{
		return Yii::app()->params['shop_url'].'/'.$size.'/'.$image;
	}
	public static function priceFormat ($price) {
		$price = sprintf('%.0f', $price);
		//if(Yii::app()->language == 'de')
		$price = str_replace('.', ',', $price);
		while (true) {
			$replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $price);
			if ($replaced != $price) {
				$price = $replaced;
			} else {
				break;
			}
		}
		$price = str_replace(',', '.', $price);
		$price .= ' '.Yii::app()->params['currencySymbol'];
	
		return $price;
	}
	public static function EncryptClientId($id,$num = 8){
		return substr(md5($id), 0, $num).dechex($id);
	}
	
	public static function DecryptClientId($id,$num = 8){
		$md5_8 = substr($id, 0, $num);
		$real_id = hexdec(substr($id, $num));
		return ($md5_8==substr(md5($real_id), 0, $num)) ? $real_id : 0;
	}
}