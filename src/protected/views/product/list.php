<div class="widget-box wg-product-list">
	<div class="wg-box-head">
		<h3 class="wg-box-title"><?php echo CHtml::encode($category->title);?></h3>
	</div>
	<div class="wg-box-body">
		<div class="wg-wrr-box-body">
			<?php 
				if($category && !empty($category->description)){
					echo '<div class="intro pad10">'.$category->description.'</div>';
				}
			?>
			<ul class="product-list">
			<?php 
	            $i=0;
		        foreach($data as $product){
		            $i++;
		            $isNew = $product->is_new;
		            $isHot = $product->is_hot;
		            $category_key = WebShopCategoryModel::model()->findByPk($product->category_id)->alias;
		            $link = Yii::app()->createUrl('/product/view', array(
		            		'category_key'=>$category_key,
		            		'url_key'=>$product->alias, 
		            		'id'=>$product->product_id));
		            $avatar = AvatarHelper::getThumbUrl($product->product_id, "products","sm");
		            $price = ShopHelpers::priceFormat($product->price);
		            $title = CHtml::encode($product->title);
	        ?>
	        <li class="product-item">
	        	<a href="<?php echo $link;?>" title="<?php echo $title;?>">
	        		<img class="product-thumb" src="<?php echo $avatar;?>" />
	        	</a>
	        	<a class="product-title" href="<?php echo $link;?>" title="<?php echo $title;?>"><?php echo CHtml::encode($product->title);?></a>
	        </li>
	        <?php }?>
	        </ul>
	        <div class="clear"></div>
	        <div class="pagination">
		        <div class="page-counter">
		            <p><?php echo 'Trang '.($paging->getCurrentPage()+1).'/'.$paging->pageCount; ?> </p>
		        </div>
		        <div class="pagination-list">
		         <?php 
		        $this->widget('CLinkPager', array(
		                'currentPage'=>$paging->getCurrentPage(),
		                'itemCount'=>$total,
		                'pageSize'=>$limit,
		                'maxButtonCount'=>5,
		                //'nextPageLabel'=>'My text >',
		                'header'=>'',
		            ));
		        ?>
		        </div>
		    </div>
		</div>
	</div>
</div>