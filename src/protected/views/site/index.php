<?php
$this->pageTitle = Yii::app()->user->getState('SYS_SITE_NAME');
$cs = Yii::app()->clientScript;
$cs->registerMetaTag(CHtml::encode(Yii::app()->user->getState('SYS_META_DESCRIPTION')), null, null, array('name' => 'description'));
$cs->registerMetaTag(CHtml::encode(Yii::app()->user->getState('SYS_META_KEYWORDS')), null, null, array('name' => 'keywords'));
?>
<div class="container-fluid">
    <div class="container wow fadeInUp">
        <?php $this->widget('application.widgets.CustomHtml.customHtml', array('_wID' => 18)); ?>
    </div>
</div>
<?php
$this->widget('application.widgets.category.CategoryWidget', array(
    '_id' => 28,
    '_view' => 'gridview',
    '_title' => Yii::t('main', 'Services'),
    ));
?>
<br><br>
<?php
$this->widget('application.widgets.partner.PartnerWidget', array(
    'album_id' => 12,
    'view' => 'default',
    'title' => 'Luật sư Nguyễn Doãn Hùng và các Luật sư cộng sự'
));
?>
<div class="container-fluid target">
    <div class="container">
        <div class="row">
            <h2 class="wow fadeInUp">10 năm kinh nghiệm</h2>
            <h2 class="wow fadeInUp">Tư vấn pháp luật 24/24</h2>
            <h2 class="wow fadeInUp">Chưa tính phí giai đoạn tư vấn chỉ tính phí khi ký hợp đồng</h2>
            <h2 class="wow fadeInUp">Hoàn trả lại phí nếu như không đạt kết quả như mong muốn</h2>
        </div>
    </div>
</div>
<br/>
<div class="container">
    <?php $this->widget('application.widgets.posts.PostsSlideWidget'); ?>
</div>