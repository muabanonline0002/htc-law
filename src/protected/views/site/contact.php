<div class="container">
   <div class="contact">
      <div class="row">
         <div class="col-sm-12 col-xs-12 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
            <div class="cate-tab clearfix">
               <h2 class="cate-title">Liên hệ với chúng tôi</h2>
            </div>
         </div>
      </div>
      <div class="col-xs-12 enuy pad0">
         <p><br><br></p>
         <div class="map wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
         <iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJvV0yIj-rNTERJvaQheUap3U&key=AIzaSyDWlQuC3FJkG43t2PkzKznluaKaBVouiTs" allowfullscreen></iframe>
         </div>
         <div class="col-md-2"><a href="/"><img src="/images/logo.png" alt="logo" class="img-responsive tony"></a></div>
         <div class="col-sm-10 col-xs-12 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
            <div class="address">
               <?php echo Yii::app()->user->getState('SYS_ADDRESS_CONTACT');?>
            </div>
         </div>
      </div>
   </div>
</div>