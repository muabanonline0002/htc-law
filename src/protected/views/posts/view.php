<?php 
$this->pageTitle = CHtml::encode($posts->title);
$url =Yii::app()->createAbsoluteUrl('/posts/view', array('url_key'=>$posts->alias, 'id'=>$posts->id));
$thumb = AvatarHelper::getThumbUrl($posts->id, "articles","med");
$cs = Yii::app()->clientScript;
$cs->registerMetaTag($url,null,null,array('property'=>'og:url'));
$cs->registerMetaTag('website',null,null,array('property'=>'og:type'));
$cs->registerMetaTag(strip_tags($posts->title).'|'.Yii::app()->name,null,null,array('property'=>'og:title'));
$cs->registerMetaTag(strip_tags($posts->metadesc),null,null,array('property'=>'og:description'));
$cs->registerMetaTag($thumb,null,null,array('property'=>'og:image'));
// $cs->registerMetaTag(date('d-m-Y H:i:s',strtotime($posts->modified)),null,null,array('property'=>'og:updated_time'));

$cs->registerMetaTag(CHtml::encode($posts->metakey),null,null,array('name'=>'keywords'));
$cs->registerMetaTag(CHtml::encode($posts->metadesc),null,null,array('name'=>'description'));
?>
    <div class="post">
        <div class="row">
            <div class="col-xs-12 enuy">
                <h2><?php echo CHtml::encode($posts->title);?></h2>
                <div class="control">
                    <?php $this->widget('application.widgets.Facebook.FacebookButton', array('url'=>$url));?>
                </div>
                <div class="gwd">
                <?php echo $posts->fulltext;?>
                </div>
            </div>
        </div>
    </div>
<?php 
$this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'column_right'));
$this->widget('application.widgets.posts.PostsWidget', array(
    '_type'=>'hot',
    '_title'=>Yii::t('main','Xem nhiều'),
    '_id'=>$posts->catid
    ));
$this->widget('application.widgets.category.CategoryWidget', array(
    '_title'=>Yii::t('main','Dịch vụ'),
    '_id'=>28
    ));
$this->endWidget();
?>