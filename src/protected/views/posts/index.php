<?php 
$this->pageTitle = $title;
?>
<div class="container">
    <h2 class="module-title"><?php echo $title;?></h2>
    <?php 
        if($data){
            $i=0;
            
            foreach($data as $item){
                
                if($i==0 || $i%3==0){
                    echo '<div class="row">';
                }
                $link =Yii::app()->createUrl('/posts/view', array('url_key'=>$item->alias, 'id'=>$item->id));
                $thumb = AvatarHelper::getThumbUrl($item->id, "articles","sm");
                $title = CHtml::encode($item->title);
                $intro = $item->introtext;
        ?>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="feature-post">
                <a href="<?php echo $link;?>" title="<?php echo $title;?>">
                    <img class="img-responsive" src="<?php echo $thumb;?>" alt="<?php echo $title;?>"> </a> 
                <h2><a class="smooth" href="<?php echo $link;?>" title="<?php echo $title;?>"><?php echo $title;?></a></h2>
                <p>
                    <?php echo $intro;?>
                </p>
            </div>
        </div>
    <?php 
                if(($i>0 && $i%3==0) || $i==$total-1){
                    echo '</div>';
                }
                $i++;
            }
        }
    ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="pagination posts-pagination">
                <div class="page-counter floatleft">
                    <p><?php echo 'Trang '.($paging->getCurrentPage()+1).'/'.$paging->pageCount; ?> </p>
                </div>
                <div class="pagination-list floatright">
                 <?php 
                $this->widget('CLinkPager', array(
                        'currentPage'=>$paging->getCurrentPage(),
                        'itemCount'=>$total,
                        'pageSize'=>$limit,
                        'maxButtonCount'=>5,
                        //'nextPageLabel'=>'My text >',
                        'header'=>'',
                    ));
                ?>
                </div>
            </div>
        </div>
    </div>
</div>
