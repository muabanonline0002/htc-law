<?php 
$this->pageTitle = $title;
?>
<div class="container">
	<a href="<?php echo $url;?>" title="<?php echo $title;?>">
        <h2 class="module-title"><?php echo $title;?></h2>
    </a>
    <?php 
        if($data){
        	$i=0;
        	if($i==0 || $i%3==0){
        		if($i>3){
        			echo '</div><div class="row">';
        		}else{
        			echo '<div class="row">';
        		}
        		
        	}
	        foreach($data as $item){
	            $link =Yii::app()->createUrl('/posts/view', array('url_key'=>$item->alias, 'id'=>$item->id));
	            $thumb = AvatarHelper::getThumbUrl($item->id, "articles","sm");
	            $title = CHtml::encode($item->title);
	            $intro = $item->introtext;
        ?>
        <div class="col-md-4 col-sm-6 col-xs-12">
			<div class="feature-post">
			    <a href="<?php echo $link;?>" title="<?php echo $title;?>">
			    	<img class="img-responsive" src="<?php echo $thumb;?>" alt="<?php echo $title;?>"> </a> 
			    <h2><a class="smooth" href="<?php echo $link;?>" title="<?php echo $title;?>"><?php echo $title;?></a></h2>
			    <p>
			    	<?php echo $intro;?>
			    </p>
			</div>
		</div>
    <?php 
			}
    	}
    ?>
</div>
