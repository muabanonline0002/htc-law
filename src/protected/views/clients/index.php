<?php 
$this->pageTitle = 'Khách hàng nói gì';
?>
<?php if($data){?>
<div class="container clients">
	<h2 class="module-title"><?=Yii::t('main', 'Clients');?></h2>
	<div class="row">
		<?php 
		foreach($data as $item){
			$thumb = AvatarHelper::getThumbUrl($item->id, "gallery","med");
			$title = $item->title;
			$description = $item->description;
			$url = Yii::app()->createUrl('/clients/view', array('alias'=>$item->alias));
		?>
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="box-cate">
				<a href="<?php echo $url;?>" title="<?php echo $title;?>"> 
					<img class="circle" src="<?php echo $thumb;?>" alt="<?php echo $title;?>">
				</a>
				<div class="clearfix"> 
				<blockquote>
                    <?php echo $description;?>
                </blockquote>
                </div>
			</div>
		</div>
		<?php }?>
	</div>
</div>
<?php }?>