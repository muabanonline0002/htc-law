<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <base href='<?php echo SITE_URL;?>'/>
        <title><?php echo $this->pageTitle;?></title>
        <meta name="copyright" content="" />
        <meta name="author" content="" />
        <meta name="GENERATOR" content="" />
        <meta name="resource-type" content="Document" />
        <meta name="distribution" content="Global" />
        <meta name="revisit-after" content="1 days" />
        <meta http-equiv="audience" content="General" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta http-equiv="content-language" content="vi" />
        <meta name="language" content="vietnamese" />
        <link rel="canonical" href="/mau-hop-dong-" />
        <meta property="og:site_name" content="Mẫu hợp đồng cho thuê văn phòng">
        <meta property="og:locale" content="vi_vn">
        <link rel="shortcut icon" href="/images/favicon.png">
        <?php
        $version = '20181122';
        $url = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile("{$url}/css/bootstrap.min.css");
        $cs->registerCssFile("{$url}/css/font-awesome.css");
        $cs->registerCssFile("{$url}/css/camera.css");
        $cs->registerCssFile("{$url}/css/animate.css");
        $cs->registerCssFile("{$url}/css/owl.carousel.css");
        $cs->registerCssFile("{$url}/css/jquery.fancybox.css");
        $cs->registerCssFile("{$url}/css/cloudzoom.css");
        $cs->registerCssFile("{$url}/css/menu.css");
        $cs->registerCssFile("{$url}/css/headroom.css");
        $cs->registerCssFile("{$url}/css/meanmenu.css");
        $cs->registerCssFile("{$url}/css/style.css?v=".$version);
        $cs->registerCssFile("{$url}/css/script.css");

        Yii::app()->clientScript->scriptMap=array(
            'jquery.min.js'=>false,
        );
        $cs->registerScriptFile("{$url}/js/jquery-2.2.1.min.js",CClientScript::POS_END);
        $cs->registerScriptFile("{$url}/js/bootstrap.min.js",CClientScript::POS_END);
        $cs->registerScriptFile("{$url}/js/jquery.easing.1.3.js",CClientScript::POS_END);
        $cs->registerScriptFile("{$url}/js/camera.min.js",CClientScript::POS_END);
        $cs->registerScriptFile("{$url}/js/owl.carousel.min.js",CClientScript::POS_END);
        $cs->registerScriptFile("{$url}/js/wow.js",CClientScript::POS_END);
        $cs->registerScriptFile("{$url}/js/jquery.fancybox.pack.js",CClientScript::POS_END);
        $cs->registerScriptFile("{$url}/js/cloudzoom.js",CClientScript::POS_END);
        $cs->registerScriptFile("{$url}/js/headroom.min.js",CClientScript::POS_END);
        $cs->registerScriptFile("{$url}/js/jQuery.headroom.min.js",CClientScript::POS_END);
        $cs->registerScriptFile("{$url}/js/jQuery.meanmenu.js",CClientScript::POS_END);
        $cs->registerScriptFile("{$url}/js/menu.js",CClientScript::POS_END);
        $cs->registerScriptFile("{$url}/js/script.js",CClientScript::POS_END);
        $cs->registerScriptFile("{$url}/js/plugin/script.js",CClientScript::POS_END);
        ?>
        <style>
            body{
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            -o-user-select: none;
            user-select: none;
            }
        </style>
        <script type="text/JavaScript">
function killCopy(e){
return false
}
function reEnable(){
return true
}
document.onselectstart=new Function('return false');
if (window.sidebar){
document.onmousedown=killCopy
document.onclick=reEnable
}
</script>
<script type="text/JavaScript">
var message="NoRightClicking"; function defeatIE() {if (document.all) {(message);return false;}} function defeatNS(e) {if (document.layers||(document.getElementById&&!document.all)) { if (e.which==2||e.which==3) {(message);return false;}}} if (document.layers) {document.captureEvents(Event.MOUSEDOWN);document.onmousedown=defeatNS;} else{document.onmouseup=defeatNS;
    document.oncontextmenu=defeatIE;} document.oncontextmenu=new Function('return false') 
</script>
    </head>