<?php 
$url = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerCssFile("{$url}/css/lightbox.min.css");
$cs->registerScriptFile("{$url}/js/lightbox-plus-jquery.min.js",CClientScript::POS_END);
?>
<div class="widget-box wg-product-list wg-head-75">
	<div class="wg-box-head">
		<h3 class="wg-box-title">Công trình đã thi công</h3>
	</div>
	<div class="wg-box-body">
		<div class="wg-wrr-box-body">
			<ul class="product-list">
			<?php 
	            $i=0;
		        foreach($data as $item){
                $title = $item->title;
                //$subtitle = $item->description;
                $image = AvatarHelper::getThumbUrl($item->id, "gallery","sm");
                $imageZoom = AvatarHelper::getThumbUrl($item->id, "gallery","max");
                $link = $item->link;
                $openLink = ($item->openlink=='new')?'target="_blank"':'';
                //$i++;
	        ?>
	        <li class="product-item">
	        	<a data-lightbox="example-set" href="<?php echo $imageZoom;?>" title="<?php echo $title;?>">
	        		<img class="product-thumb" src="<?php echo $image;?>" />
	        	</a>
	        	<a class="product-title" href="<?php echo $link;?>" title="<?php echo $title;?>"><?php echo CHtml::encode($title);?></a>
	        </li>
	        <?php }?>
	        </ul>
	        <div class="clear"></div>
	        <div class="pagination">
		        <div class="page-counter floatleft">
		            <p><?php echo 'Trang '.($paging->getCurrentPage()+1).'/'.$paging->pageCount; ?> </p>
		        </div>
		        <div class="pagination-list">
		         <?php 
		        $this->widget('CLinkPager', array(
		                'currentPage'=>$paging->getCurrentPage(),
		                'itemCount'=>$count,
		                'pageSize'=>$limit,
		                'maxButtonCount'=>5,
		                //'nextPageLabel'=>'My text >',
		                'header'=>'',
		            ));
		        ?>
		        </div>
		    </div>
		</div>
	</div>
</div>