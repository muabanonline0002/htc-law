<?php 
$thumb = AvatarHelper::getThumbUrl($item->id, "partner","med");
$title = $item->title;
$description = $item->description;
?>
<div class="container">
	<div class="page_member">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-12 wow fadeInUp">
				<div class="img-member">
					<div class="clearfix">
						<img src="<?php echo $thumb;?>" alt="<?php echo $title;?>" />
					</div>
				</div>
			</div>
			<div class="col-md-8 col-sm-8 col-xs-12 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
				<?php echo $description;?>
			</div>
		</div>
	</div>
</div>