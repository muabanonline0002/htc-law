<div class="<?php echo $csClass; ?>">
    <?php
    if (!empty($params)):
        $data = @unserialize($params->params);
        $langActive = Yii::app()->language;
        ?>
        <?php if ($params->show_title > 0): ?>
        <h2 class="module-title"><?php echo Yii::t('main', $params->title); ?></h2>
    <?php endif; ?>
        <div class="wg-content">
            <?php
            if ($langActive != Yii::app()->params['language_default'] && isset($data["content_{$langActive}"]) && !empty($data["content_{$langActive}"])) {
                echo $data["content_{$langActive}"];
            } else {
                echo $data['content'];
            }
            ?>
        </div>
    <?php endif; ?>
</div>
