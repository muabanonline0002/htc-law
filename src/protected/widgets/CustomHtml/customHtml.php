<?php

class customHtml extends CWidget
{
    public $_wID = NULL;
    public $title = NULL;
    public $csClass = 'box_hot_top';

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $params = WidgetsModel::model()->findByPk($this->_wID);
        $this->render('default', array(
            'params' => $params,
            'csClass' => $this->csClass
        ));
    }

    public static function buildParams($params = "")
    {
        $data = @unserialize($params);
        ?>
        <label><?php echo Yii::t('admin_widgets', 'Content'); ?></label>
        <?php
        Yii::app()->controller->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
            'name' => 'customHtml[content]',
            'value' => $data['content'],
            'options' => array(
                'buttons' => array('html', 'formatting', 'fontfamily', 'fontcolor',
                    'bold', 'italic', 'underline', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'link', 'alignment'
                , 'horizontalrule', 'image'
                ),
                'lang' => 'en',
                'imageUpload' => Yii::app()->createUrl('/upload/fileUpload'),
                'imageUploadErrorCallback' => 'js:function(json){ alert(json.error); }',
                //'fileUpload'=>$this->createUrl('fileUpload'),
                //'fileUploadErrorCallback'=>'js:function(obj, json){ alert(json.error); }',
                'counterCallback' => 'js:function(data){
					var counter_text = data.words+" words"+", "+data.characters+" characters";
					$("#full_text_counter").html(counter_text);
					}
				',
                'imageManagerJson' => Yii::app()->createUrl('/upload/fileManager'),
                'minHeight' => 300,
                'placeholder' => Yii::t('main', 'Nhập nội dung')
            ),
            'plugins' => array(
                'fontfamily' => array(
                    'js' => array('fontfamily.js')
                ),
                'fontcolor' => array(
                    'js' => array('fontcolor.js')
                ),
                'fontsize' => array(
                    'js' => array('fontsize.js')
                ),
                'counter' => array(
                    'js' => array('counter.js')
                ),
                'cleantext' => array(
                    'js' => array('cleantext.js')
                ),
                'clips' => array(
                    'css' => array('clips.css',),
                    'js' => array('clips.js',),
                    // add depends packages
                    'depends' => array('imperavi-redactor',),
                ),
                'imagemanager' => array(
                    'js' => array('imagemanager.js'),
                ),
                'video' => array(
                    'js' => array('video.js')
                ),
                'table' => array(
                    'js' => array('table.js')
                ),
                'fullscreen' => array(
                    'js' => array('fullscreen.js',),
                ),
            ),
        ));
        ?>
        <div id="full_text_counter"></div>
        <!--<textarea name="customHtml[content]" id="customHtml_content"><?php //echo $data['content']
        ?></textarea>-->
        <label><?php echo Yii::t('admin_widgets', 'Content_En'); ?></label>
        <?php
        Yii::app()->controller->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
            'name' => 'customHtml[content_en]',
            'value' => isset($data['content_en']) ? $data['content_en'] : '',
            'options' => array(
                'buttons' => array('html', 'formatting', 'fontfamily', 'fontcolor',
                    'bold', 'italic', 'underline', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'link', 'alignment'
                , 'horizontalrule', 'image'
                ),
                'lang' => 'en',
                'imageUpload' => Yii::app()->createUrl('/upload/fileUpload'),
                'imageUploadErrorCallback' => 'js:function(json){ alert(json.error); }',
                //'fileUpload'=>$this->createUrl('fileUpload'),
                //'fileUploadErrorCallback'=>'js:function(obj, json){ alert(json.error); }',
                'counterCallback' => 'js:function(data){
					var counter_text = data.words+" words"+", "+data.characters+" characters";
					$("#full_text_counter2").html(counter_text);
					}
				',
                'imageManagerJson' => Yii::app()->createUrl('/upload/fileManager'),
                'minHeight' => 300,
                'placeholder' => Yii::t('main', 'Nhập nội dung')
            ),
            'plugins' => array(
                'fontfamily' => array(
                    'js' => array('fontfamily.js')
                ),
                'fontcolor' => array(
                    'js' => array('fontcolor.js')
                ),
                'fontsize' => array(
                    'js' => array('fontsize.js')
                ),
                'counter' => array(
                    'js' => array('counter.js')
                ),
                'cleantext' => array(
                    'js' => array('cleantext.js')
                ),
                'clips' => array(
                    'css' => array('clips.css',),
                    'js' => array('clips.js',),
                    // add depends packages
                    'depends' => array('imperavi-redactor',),
                ),
                'imagemanager' => array(
                    'js' => array('imagemanager.js'),
                ),
                'video' => array(
                    'js' => array('video.js')
                ),
                'table' => array(
                    'js' => array('table.js')
                ),
                'fullscreen' => array(
                    'js' => array('fullscreen.js',),
                ),
            ),
        ));
        ?>
        <div id="full_text_counter2"></div>
        <input type="hidden" name="widget" value="customHtml"/>
        <?php
    }

    public function getParams($post)
    {
        return serialize($post['customHtml']);
    }
}