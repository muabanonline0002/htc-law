<?php

class LanguageWidget extends CWidget
{
    public function init()
    {
        return parent::init();
    }

    public function run()
    {
        $languageActive = Yii::app()->language;
        $languageOption = array(
            'en' => array(
                'label' => 'English',
                'icon' => '/images/en-gb.png'
            ),
            'vi' => array(
                'label' => 'Tiếng Việt',
                'icon' => '/images/vi-vn.png'
            )
        );
        $this->render('default', compact('languageActive', 'languageOption'));
    }
}