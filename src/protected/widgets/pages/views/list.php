<?php
echo '<ul>';
foreach ($data as $key => $item) {
	$title = $item->title;
	$link = Yii::app()->createUrl('page/view', array('url_key_page'=>$item->alias));
?>
	<li><a href="<?php echo $link;?>" title="<?php echo $title;?>"><i class="fa fa-caret-right"></i><?php echo $title;?></a></li>
<?php
}
echo '</ul>';