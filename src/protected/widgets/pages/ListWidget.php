<?php
class ListWidget extends CWidget
{
	public $catid;
	public function run()
	{
		$data = FrontendPagesModel::model()->getPageByCat($this->catid);
		$this->render('list', compact('data'));
	}
}