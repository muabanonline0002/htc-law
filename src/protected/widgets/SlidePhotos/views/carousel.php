<div class="slider">
    <?php 
        $i=1;
        foreach($data as $item){
                $title = CHtml::encode($item->title);
                //$subtitle = $item->description;
                $image = AvatarHelper::getThumbUrl($item->id, "slideshow","origin");
                $link = $item->link;
                $openLink = ($item->openlink=='new')?'target="_blank"':'';
                //$i++;
        ?>
        <div data-src="<?php echo $image;?>" data-title="<?php echo $title;?>" data-link="">
            <div class="head-status wow fadeInLeft">
                <h3><?php echo $title;?></h3>
            </div>
        </div>
    <?php }?>
</div>