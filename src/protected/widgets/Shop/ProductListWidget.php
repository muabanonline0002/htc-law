<?php
class ProductListWidget extends CWidget
{
	public $_product_id = NULL;
	public $_id = NULL;
	public $_url = NULL;
	public $title = NULL;
	public $type=null;
	public $view='product_list';
	public $class='';
	public function init()
	{
		parent::init();
	}
	public function run()
	{
		$dependency = new CDbCacheDependency('SELECT MAX(updated_datetime) FROM shop_products');
		$cacheId='WEB_ProductListWidget'.$this->_id.$this->type.$this->view;
		if($this->beginCache($cacheId,array('duration'=>Yii::app()->params['cache_time'],'dependency'=>$dependency))){
			if($this->type=='by_cate'){
				$limit=30;
				$offset=0;
				$data = FrontendShopProductsModel::getProductByCat($this->_id,$limit,$offset);
				$title = WebShopCategoryModel::model()->findByPk($this->_id)->title;
			}elseif($this->type=='other'){
				$data = FrontendShopProductsModel::getOtherProductInCat($this->_product_id,$this->_id,30,0);
				$title = $this->title;
			}else{
				$data = FrontendShopProductsModel::model()->getProductsByType($this->type,5);
				$title = $this->title;
			}
			$this->render($this->view, compact('data','title'));
			$this->endCache();
		}
	}
}