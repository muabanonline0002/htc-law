<div class="widget-box wg-product-list <?php echo $this->class;?>">
	<div class="wg-box-head">
		<h3 class="wg-box-title"><?php echo CHtml::encode($title);?></h3>
	</div>
	<div class="wg-box-body">
		<div class="wg-wrr-box-body">
			<ul class="product-list">
			<?php 
	            $i=0;
		        foreach($data as $product){
		            $i++;
		            $isNew = $product->is_new;
		            $isHot = $product->is_hot;
		            $category_key = WebShopCategoryModel::model()->findByPk($product->category_id)->alias;
		            $link = Yii::app()->createUrl('/product/view', array(
		            		'category_key'=>$category_key,
		            		'url_key'=>$product->alias,
		            		'id'=>$product->product_id));
		            $avatar = AvatarHelper::getThumbUrl($product->product_id, "products","sm");
		            $price = ShopHelpers::priceFormat($product->price);
		            $title = CHtml::encode($product->title);
		            //$avatar = '/images/thumb_p1.jpg';
	        ?>
	        <li class="product-item">
	        	<a href="<?php echo $link;?>" title="<?php echo $title;?>">
	        		<img class="product-thumb" data-lazy-src="<?php echo $avatar;?>" src="<?php echo $avatar;?>" />
	        	</a>
	        	<a class="product-title" href="<?php echo $link;?>" title="<?php echo $title;?>"><?php echo CHtml::encode($product->title);?></a>
	        </li>
	        <?php }?>
	        </ul>
	        <div class="clear"></div>
		</div>
	</div>
</div>