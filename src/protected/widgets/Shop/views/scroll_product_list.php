<?php if($data){?>
<div class="widget-box-sidebar marquee-sidebar">
    <div class="wg-box-head">
        <h3 class="wg-box-title"><?php echo $title;?></h3>
    </div>
    <div class="wg-box-body">
        <div class="wg-wrr-box-body">
            <marquee onmouseover="this.stop();" onmouseout="this.start();" direction="up" height="243" behavior="scroll" scrollamount="2" style="height: 243px;">
            <?php 
            $i=0;
            foreach($data as $product){
                $i++;
                $c = ($i%2==0)?'isnew':'ishot';
                $category_key = WebShopCategoryModel::model()->findByPk($product->category_id)->alias;
                $link = Yii::app()->createUrl('/product/view', array(
                    'category_key'=>$category_key,
                    'url_key'=>$product->alias, 
                    'id'=>$product->product_id
                    ));
                $thumb = AvatarHelper::getThumbUrl($product->product_id, "products","sm");
                $title = CHtml::encode($product->title);
            ?>
            <div class="marquee-item">
                <a href="<?php echo $link;?>" title="<?php echo $title;?>"><img width="100%" src="<?php echo $thumb;?>" /></a>
                <div class="mqr-title"><a href="<?php echo $link;?>"><?php echo $title;?></a></div>
            </div>
            <?php }?>
            </marquee>
        </div>
    </div>
</div>
<?php }?>