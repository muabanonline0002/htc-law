<?php
class CartWidget extends CWidget
{
	public function init()
	{
		parent::init();
	}
	public function run()
	{
		$products = Shop::getCartContent();
		$this->render('cart', compact('products'));
	}
}