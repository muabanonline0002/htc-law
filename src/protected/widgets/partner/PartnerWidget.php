<?php
class PartnerWidget extends CWidget
{
	public $title;
	public $album_id=12;
	public $view='scroll_layout';
	public function init()
	{
		parent::init();
	}
	public function run()
	{
		$dependency = new CDbCacheDependency('SELECT MAX(updated_time) FROM tbl_gallery_items WHERE catid='.$this->album_id);
		$cacheId='partner'.$this->album_id.Yii::app()->language;
//		if($this->beginCache($cacheId,array('duration'=>Yii::app()->params['cache_time'],'dependency'=>$dependency))){
			$crit = new CDbCriteria();
			$crit->condition = 'status=1 AND catid=:id';
			$crit->params = array(':id' => $this->album_id);
			$crit->order=" ordering asc ";
			$data = FrontendGalleryItemsModel::model()->findAll($crit);
			$title = $this->title;
			$this->render($this->view, compact('data','title'));
//			$this->endCache();
//		}
	}
}