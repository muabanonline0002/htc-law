<div class="container-fluid luat-su">
    <div class="container">
        <a href="/#" title="Luật sư thành viên">
            <h2 class="module-title" style="font-size: 44px;">Luật sư Nguyễn Doãn Hùng và các Luật sư cộng sự</h2>
        </a>
        <div class="row">
            <?php 
                if($data)
                    foreach ($data as $key => $value) {
                        $url = Yii::app()->createUrl('/law/view',array('alias'=>Common::makeFriendlyUrl($value->title)));
                        $title = CHtml::encode($value->title);
                        $image = AvatarHelper::getThumbUrl($value->id, "partner","med");
            ?>
            <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp">
                <div class="box-avocat">
                    <a href="<?php echo $url;?>" title="<?php echo $title;?>">
                    	<img src="<?php echo $image;?>" alt="<?php echo $title;?>">
                    </a> 
                    <a href="<?php echo $url;?>" title="Luật sư <?php echo $title;?>">
                    <div class="overlay2">
                        <div class="text2"><?php echo Yii::t('main', 'Luật sư');?> <br> <?php echo $title;?></div>
                    </div>
                    </a>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
</div>