<?php if($data){?>
<div class="widget-box-sidebar marquee-sidebar">
    <div class="wg-box-head">
        <h3 class="wg-box-title"><?php echo $title;?></h3>
    </div>
    <div class="wg-box-body">
        <div class="wg-wrr-box-body">
            <marquee onmouseover="this.stop();" onmouseout="this.start();" direction="up" height="243" behavior="scroll" scrollamount="2" style="height: 243px;">
            <?php 
            $i=0;
            foreach($data as $item){
                $title = $item->title;
                //$subtitle = $item->description;
                $image = AvatarHelper::getThumbUrl($item->id, "partner","sm");
                $link = $item->link;
                $openLink = ($item->openlink=='new')?'target="_blank"':'';
                //$i++;
            ?>
            <div class="marquee-item">
                <a href="<?php echo $link;?>" title="<?php echo $title;?>">
                <img width="100%" src="<?php echo $image;?>" /></a>
                <div class="mqr-title"><a href="<?php echo $link;?>"><?php echo $title;?></a></div>
            </div>
            <?php }?>
            </marquee>
        </div>
    </div>
</div>
<?php }?>