<?php
class PostsWidget extends CWidget
{
	public $_title='Tin mới';
	public $_type='category';
	public $_id=9;
	public $_view='posts_sidebar';
	public $_limit=10;
	public function run()
	{
		switch ($this->_type) {
			case 'hot':
				$posts = WebArticleModel::getArticlesHot($this->_id,5);
				break;
			
			default:
				$posts = WebArticleModel::getArticleByCat($this->_id);
				break;
		}
		$title = $this->_title;
		$this->render($this->_view, compact('title','posts'));
	}
}