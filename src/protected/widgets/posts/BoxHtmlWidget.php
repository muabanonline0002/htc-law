<?php
class BoxHtmlWidget extends CWidget
{
	public $_type='posts';
	public $_id=0;
	public function run()
	{
		if($this->_type=='posts'){
			$posts = WebArticleModel::model()->findByPk($this->_id);
			$title = $posts->title;
			$content = $posts->fulltext;
		}
		$this->render('box_html', compact('title','content'));
	}
}