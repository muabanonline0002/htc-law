<?php
class PostsSlideWidget extends CWidget
{
	public $_title='Tin tức';
	public $_type='posts';
	public $_id=9;
	public $_view='slide';
	public $_limit=10;
	public function run()
	{
		//$posts = WebArticleModel::getArticleByCat($this->_id);
		$posts = WebArticleModel::model()->getArticlesCat($this->_id);
		$title = $this->_title;
		$this->render($this->_view, compact('title','posts'));
	}
}