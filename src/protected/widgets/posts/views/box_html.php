<div class="widget-box">
	<div class="wg-box-head">
		<h3 class="wg-box-title"><?php echo $title;?></h3>
	</div>
	<div class="wg-box-body">
		<div class="wg-wrr-box-body">
			<?php echo $content;?>
		</div>
	</div>
</div>