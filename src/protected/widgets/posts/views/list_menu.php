<?php
if($posts){
	foreach ($posts as $key => $value) {
		$link =Yii::app()->createUrl('/posts/view', array('url_key'=>$value['alias'], 'id'=>$value['id']));
		$thumb = AvatarHelper::getThumbUrl($value['id'], "articles","sm");
		$title = CHtml::encode($value['title']);
		?>
            <li><a class="smooth" href="<?php echo $link;?>" title="<?php echo $title;?>"><?php echo $title;?></a></li>
		<?php
	}
}