<div class="other-news"> 
	<div class="cate-blog-title clearfix">
		<h3 class="blog-title"><?php echo CHtml::encode($this->_title)?></h3>
	</div>
<div class="other-list">
	<?php if($data){
		foreach ($data as $key => $value) {
			$url = Yii::app()->createUrl('/posts/category',array('alias'=>$value->alias));
            $title = CHtml::encode($value->title);
            $count = WebArticleModel::model()->getCountArticlesCat($value->id);
			?>
			<div class="other-item clearfix">
				<h2>
					<a class="smooth" href="<?php echo $url;?>" title="<?php echo $title;?>">
					<i class="fa fa-gavel" aria-hidden="true"></i>&nbsp;
					<?php echo $title;?> (<?php echo $count;?>)</a>
				</h2>
			</div>
			<?php
		}
	}?>
</div>
</div>