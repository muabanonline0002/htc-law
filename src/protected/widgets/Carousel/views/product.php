<?php
$cs = Yii::app()->getClientScript();
$cs->registerScript('product','
			$(document).ready(function() 
			{
				$("#lista2").als({
					visible_items: 3,
					scrolling_items: 1,
					orientation: "vertical",
					circular: "yes",
					autoscroll: "no",
					direction: "up"
				});
			})
	');
?>
<div class="widget_box hidden-xs products">
    <div class="widget_box_title">
        <h3><i class="fa fa-reorder"></i>Sản phẩm mới</h3>
    </div>
    <div class="widget_box_content">
<div id="lista2" class="als-container">
	<span class="als-prev"><img src="<?php echo $this->_url;?>/images/thin_bottom_arrow_333.png" alt="next" title="next" /></span>
	<div class="als-viewport">
		<div class="als-wrapper">
		<?php foreach($data as $product){
			$avatar =AvatarHelper::getThumbUrl($product->product_id, "products","sm");
			$title = $product->title;
			$link = Yii::app()->createAbsoluteUrl('/product/view', array('url_key'=>$product->alias, 'id'=>$product->product_id));
			?>
			<div class="als-item">
				<a href="<?php echo $link;?>">
					<div class="pro-item">
						<img width="100px" src="<?php echo $avatar;?>" alt="calculator" title="<?php echo $title?>" />
					</div>
						<h3><?php echo $title;?></h3>
						<p class="price">
							<?php 
							if($product->price>0){
								echo Shop::priceFormat($product->price);
							}else{
								echo 'Liên Hệ';
							}
							?>
						</p>
					<?php if($product->saleoff_percent!=''){?>
					<span class="percentoff"><?php echo $product->saleoff_percent;?></span>
					<p class="old-price"><?php echo Shop::priceFormat($product->old_price);?></p>
	                <?php }?>
					<p class="add-cart2">
						<?php if($product->price==0){?>
						<button onclick="window.location.href='<?php echo $link;?>'" type="button" title="Xem chi tiết"><i class="fa fa-info"></i>&nbsp;Xem chi tiết</button>
						<?php }else{?>
						<button onclick="CoreJs.addCart(<?php echo $product->product_id;?>,1)" type="button" title="Thêm vào giỏ hàng"><i class="fa fa-shopping-cart"></i>&nbsp;Thêm vào giỏ hàng</button>
						<?php }?>
					</p>
				</a>
			</div>
			<?php }?>
		</div>
	</div>
	<span class="als-next"><img src="<?php echo $this->_url;?>/images/thin_top_arrow_333.png" alt="prev" title="previous" /></span>
</div>
</div>
</div>