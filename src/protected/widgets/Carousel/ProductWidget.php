<?php
class ProductWidget extends CWidget{
	public $_url;
	public function init()
	{
		$dir = dirname(__FILE__).DS.'assets';
		$this->_url = Yii::app()->getAssetManager()->publish($dir,false,-1,YII_DEBUG);
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile("{$this->_url}/css/CSSreset.min.css");
		$cs->registerCssFile("{$this->_url}/css/als_demo.css");
		$cs->registerScriptFile("{$this->_url}/js/jquery.alsEN-1.0.min.js",CClientScript::POS_END);
		//$cs->registerScriptFile("{$this->_url}/js/jquery.themepunch.revolution.min.js");
		parent::init();
	}
	public function run()
	{
		/*$data = array();
		for($i=0; $i<=10; $i++){
			$pObject = new stdClass();
			$pObject->id = 1;
			$pObject->title = 'Sản phẩm Trà Nhân Sâm Hồng sâm Hàn Quốc';
			$pObject->alias = 'san-pham-tra-hong-sam-han-quoc';
			$pObject->old_price = '3.500.000&nbsp;₫';
			$pObject->price = '2.999.999&nbsp;₫';
			$pObject->saleoff_percent = '-10%';
			$pObject->avatar = '/img/home-2/product-8.jpg';
			$data[] =  $pObject;
		}*/
		$data = FrontendShopProductsModel::model()->getLastestProducts(8);
		$this->render('product', compact('data'));
	}
}