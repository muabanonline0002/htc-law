<?php

Yii::import('common.models.db.ContentModel');

class WebArticleModel extends ContentModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'tbl_content';
    }

    public function behaviors()
    {
        return array('translate' => 'common.components.STranslateableBehavior');
    }

    public function translate()
    {
        // List of model attributes to translate
        return array('introtext', 'title', 'fulltext', 'alias'); //Example
    }

    public static function getArticleByCatAlias($cat_alias, $limit = null, $offset = 0, $where = '', $order = '')
    {
        $limit = ($limit != null) ? " LIMIT $limit OFFSET $offset" : "";
        $order = ($order != '') ? $order : 'ORDER BY tc.id DESC';
        $sql = "SELECT tc.title, tc.id, tc.images, tc.introtext, tc.alias, tc.catid, tc.*, cc.alias as cat_alias
				FROM tbl_content as tc
				LEFT JOIN  tbl_categories as cc ON tc.catid = cc.id
				WHERE tc.status=" . parent::STATUS_PUBLISHED . " AND tc.type='post' AND cc.alias=:p $where
					$order
					$limit
					";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':p', $cat_alias);
        //return FNews::model()->with('category')->findAllBySql($sql, array(':p'=>$cat_alias));
        return $command->queryAll();
    }

    public static function getArticleByCat($catId, $limit = 10, $offset = 0, $where = '', $order = '')
    {
        $limit = ($limit != null) ? " LIMIT $limit OFFSET $offset" : "";
        $order = ($order != '') ? $order : 'ORDER BY tc.id DESC';
        $sql = "SELECT tc.title, tc.id, tc.images, tc.introtext, tc.alias, tc.catid, tc.*, cc.alias as cat_alias
				FROM tbl_content as tc
				LEFT JOIN  tbl_categories as cc ON tc.catid = cc.id
				WHERE tc.status=" . parent::STATUS_PUBLISHED . " AND tc.type='post' AND cc.id=:p $where
					$order
					$limit
					";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':p', $catId);
        $dependency = new CDbCacheDependency("SELECT MAX(modified) FROM tbl_content WHERE type='post'");
        return $command->cache(Yii::app()->params['cache_time'], $dependency)->queryAll();
    }

    public static function getTotalArticleByCat($catId, $where = '')
    {
        $sql = "SELECT count(*) as total
				FROM tbl_content as tc
				LEFT JOIN  tbl_categories as cc ON tc.catid = cc.id
				WHERE tc.status=" . self::STATUS_PUBLISHED . " AND tc.type='post' AND cc.id=:p $where
				";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':p', $catId);
        $result = $command->queryRow();
        return $result['total'];
    }

    public static function getArticle($limit = 10, $offset = 0, $where = '', $order = '')
    {
        $limit = ($limit != null) ? " LIMIT $limit OFFSET $offset" : "";
        $order = ($order != '') ? $order : 'ORDER BY tc.id DESC';
        $sql = "SELECT tc.*
				FROM tbl_content as tc
				WHERE tc.status=" . parent::STATUS_PUBLISHED . " AND tc.type='post' $where
					$order
					$limit
					";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryAll();
    }

    public function getArticlesByCat($catid, $offset = 0, $limit = 10)
    {
        $criteria = new CDbCriteria();
        $criteria->join = 'left join tbl_categories c2 ON t.catid=c2.id';
        $criteria->condition = '(c2.parent_id=:catid or c2.id=:catid) and t.status=:s';
        $criteria->params = array(':catid' => $catid, ':s' => parent::STATUS_PUBLISHED);
        $criteria->offset = $offset;
        $criteria->limit = $limit;
        return self::model()->findAll($criteria);
    }

    public function getArticlesReleated($postsId, $catId, $limit = 10, $offset = 0)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'id<>:id and catid=:catid and status=:s';
        $criteria->params = array(':id' => $postsId, ':catid' => $catId, ':s' => parent::STATUS_PUBLISHED);
        $criteria->offset = $offset;
        $criteria->limit = $limit;
        return self::model()->findAll($criteria);
    }

    public function getArticlesCat($catId, $limit = 10, $offset = 0)
    {
        $criteria = new CDbCriteria();
        $criteria->distinct = true;
        $criteria->join = "LEFT JOIN tbl_content_categories c2 ON t.id=c2.posts_id";
        $criteria->condition = 't.status=:s AND (c2.category_id=:catid OR t.catid=:catid)';
        $status = self::STATUS_PUBLISHED;
        $criteria->params = array(':catid' => $catId, ':s' => $status);
        $criteria->order = "t.pin DESC, t.id desc";
        $criteria->offset = $offset;
        $criteria->limit = $limit;
        $dependency = new CDbCacheDependency("SELECT MAX(modified) FROM tbl_content WHERE type='post'");
//        return self::model()->cache(Yii::app()->params['cache_time'], $dependency)->findAll($criteria);
        return self::model()->findAll($criteria);
    }

    public function getCountArticlesCat($catId)
    {
        $sql = "SELECT count(distinct c1.id) as total 
				FROM tbl_content c1
				LEFT JOIN tbl_content_categories c2 ON c1.id=c2.posts_id
				WHERE c1.status=" . self::STATUS_PUBLISHED . " AND c1.type='post' AND (c2.category_id=:id OR c1.catid=:id)";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':id', $catId);
        $result = $command->queryScalar();
        return (int)$result;
    }

    public static function getArticlesHot($catId, $limit = 10, $offset = 0)
    {
        $criteria = new CDbCriteria();
        $criteria->distinct = true;
        $criteria->join = "LEFT JOIN tbl_content_categories c2 ON t.id=c2.posts_id";
        $criteria->condition = 't.status=:s AND (c2.category_id=:catid OR t.catid=:catid)';
        $status = self::STATUS_PUBLISHED;
        $criteria->params = array(':s' => $status, ':catid' => $catId);
        $criteria->order = 'views DESC';
        $criteria->offset = $offset;
        $criteria->limit = $limit;
        $dependency = new CDbCacheDependency("SELECT MAX(modified) FROM tbl_content WHERE type='post'");
        return self::model()->cache(Yii::app()->params['cache_time'], $dependency)->findAll($criteria);
    }

    public static function getCountArticlesHot($catId, $limit = 10, $offset = 0)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'status=:s';
        $status = self::STATUS_PUBLISHED;
        $criteria->params = array(':s' => $status);
        $dependency = new CDbCacheDependency("SELECT MAX(modified) FROM tbl_content WHERE type='post'");
        return self::model()->cache(Yii::app()->params['cache_time'], $dependency)->count($criteria);
    }

    public static function updateVisit($id)
    {
        $sql="UPDATE tbl_content 
              SET views = views + 1
              WHERE id = :id
            ";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':id', $id, PDO::PARAM_INT);
        return $command->query();
    }
}