<?php
return array(
    'Giới thiệu HTC Việt Nam' => 'About HTC Viet Nam',
    'Luật sư' => 'Lawyer',
    'Dịch vụ' => 'Services',
    'DÀNH CHO KHÁCH HÀNG' => 'FOR CUSTOMERS',
    'VỀ CHÚNG TÔI' => 'ABOUT US',
    'LUẬT SƯ THÀNH VIÊN' => 'MEMBERS LAWYER',
    'Luật sư Nguyễn Doãn Hùng và các luật sư cộng sự' => 'Lawyer Nguyen Doan Hung and his partner lawyer'
);