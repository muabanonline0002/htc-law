<?php
return array(
    'Unit Price' => 'Giá bán',
    'Product Name' => 'Sản phẩm',
    'Qty' => 'Số lượng',
    'Subtotal' => 'Thành tiền',
    'update cart' => 'Cập nhật giỏ hàng',
    'clear cart' => 'Xoá',
    'Fee Shipping' => 'Phí vận chuyển',
    'Grand Total' => 'Tổng thành tiền',
    'Order Now' => 'Đặt hàng',
    'Services' => 'Dịch vụ chuyên biệt',
    'Clients' => 'Khách hàng'
);