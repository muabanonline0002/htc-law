<?php
class QuoteController extends FrontendController{
	public $layout='2column';
	public $id=29;
	public function actionIndex()
	{
		$catId = $this->id;
		
		$total = WebArticleModel::model()->getCountArticlesCat($catId);
		$paging = new CPagination($total);
		$itemOnPaging = 5;
		$limit = Yii::app()->params['postsPerPage'];
		$paging->pageSize = $limit;

		$data = WebArticleModel::model()->getArticlesCat($catId,$limit,$paging->getOffset());
		$this->render('index', compact('data','paging','total','limit'));
	}
	public function actionView()
	{
		$id = Yii::app()->request->getParam('id');
		$posts = WebArticleModel::model()->findByPk($id);
		$this->render('view', compact('posts'));
	}
}