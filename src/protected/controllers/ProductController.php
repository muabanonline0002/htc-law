<?php
class ProductController extends FrontendController
{
	//public $layout='2column_right';
	public $layout='2column';
	public function actionView()
	{
		$id = Yii::app()->request->getParam('id');
		$product = FrontendShopProductsModel::model()->findByPk($id);
		$catId = $product->category_id;
		$category = WebShopCategoryModel::model()->findByPk($catId);
		$this->render('view', compact('product','category'));
	}
	public function actionList()
	{
		$catId = Yii::app()->request->getParam('id');
		$total = FrontendShopProductsModel::model()->getCountProductsByCatId($catId);
		$paging = new CPagination($total);
		$limit = Yii::app()->params['product_list_category'];
		$paging->pageSize = $limit;
		$data = FrontendShopProductsModel::model()->getProductsByCatId($catId, $limit, $paging->getOffset());
		$category = WebShopCategoryModel::model()->findByPk($catId);
		$this->render('list', compact('data','total','paging','limit','category'));
	}
	public function actionType()
	{
		$url_key = Yii::app()->request->getParam('url_key');
		switch ($url_key) {
			case 'qua-bieu-cao-cap':
				$type='premium';
				break;
			
			default:
				$type='new';
				break;
		}
		$total = FrontendShopProductsModel::model()->getCountProductsByType($type);
		$paging = new CPagination($total);
		$itemOnPaging = 5;
		$limit = Yii::app()->params['product_list_category'];
		$paging->pageSize = $limit;
		$data = FrontendShopProductsModel::model()->getProductsByType($type,$limit, $paging->getOffset());
		$title='Quà Biếu Cao Cấp';
		$this->render('type',compact('data','paging','limit','total','title'));
	}
}