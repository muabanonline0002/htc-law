<?php

class ShoppingCartController extends FrontendController
{
	public $layout = '2column';
	public function actionView()
	{
		$cart = Shop::getCartContent();
		$this->render('view',array(
						'products'=>$cart
						));
	}
	public function actionOrder()
	{
		if(Yii::app()->request->isPostRequest){
			//$this->layout=false;
			$result = new stdClass();
			$errorCode=0;
			$cart = Shop::getCartContent();
			if(!empty($cart)){
				$customer_data = json_encode($cart);
				$data = Yii::app()->request->getParam('data');
				parse_str($data, $params);
				$name = $params['name'];
				$email = $params['email'];
				$phone = $params['phone'];
				$messege = $params['messege'];
				$addressShip = $params['ship_address'];
				$ship_type = $params['ship_type'];
				$total_money = Shop::getPriceTotalRaw();
				if(empty($name) || empty($phone) || empty($addressShip)){
					$errorCode=1;
					$errorMsg = '<div class="errorMsg">Qúy khách cần nhập đủ thông tin thanh toán</div>';
				}else{
					$model = new WebShopOrderInfoModel();
					$model->customer_name = $name;
					$model->customer_email = $email;
					$model->customer_phone = $phone;
					$model->customer_address_ship = $addressShip;
					$model->ship_type = $ship_type;
					$model->customer_comment = $messege;
					$model->customer_data = $customer_data;
					$model->total_money = $total_money;
					$model->created_datetime = date('Y-m-d H:i:s');
					if($model->save(false)){
						$errorCode = 0;
						$errorMsg = "<div class='successMsg'>
									<div>Gửi đơn hàng thành công.</div>
									<div>Cảm ơn quý khách đã mua hàng.</div>
									<div>OKkorea sẽ liên hệ và chuyển hàng đến quý khách trong thời gian sớm nhất.</div>
									</div>
									";
					}else{
						$errorCode = 1;
						$errorMsg = "<div class='errorMsg'>Có lỗi trong quá trình gửi đơn hàng, Mời quý khách thử lại sau ít phút.,/div>";
					}
				}
			}else{
				$errorCode = 1;
				$errorMsg = "<div class='errorMsg'>Giỏ hàng chưa có sản phẩm nào.</div>";
			}
			$result->errorCode = $errorCode;
			$result->errorMsg = $errorMsg;
			echo json_encode($result);
			Yii::app()->end();
		}else{
			throw new Exception(404);
		}
	}
	public function actionGetPriceTotal() {
		echo Shop::getPriceTotal();
	}

	public function actionUpdateAmount() {
		$cart = Shop::getCartContent();
		$data = Yii::app()->request->getParam('data');
		parse_str($data, $params);
		$params = $params['amount'];
		$result = new stdClass;
		$errorCode = 0;
		$errorMsg='Cập nhật thành công';
		if($params){
			foreach ($params as $productId => $am) {
				if (!is_numeric($am) || $am <= 0){
					$errorCode=1;
					$errorMsg='Số lượng sản phẩm không hợp lệ';
				}
				foreach ($cart as $key => $value) {
					if($value['product_id']==$productId){
						$cart[$key]['amount'] = $am;
					}
				}
			}
			if($errorCode==0){
				Shop::setCartContent($cart);
			}
		}
		$result->errorCode = $errorCode;
		$result->errorMsg = $errorMsg;
		echo json_encode($result);
		/*foreach($_GET as $key => $value) {
			if(substr($key, 0, 7) == 'amount_') {
				if($value == '')
					return true;
				if (!is_numeric($value) || $value <= 0)
					throw new CException('Wrong amount');
				$position = explode('_', $key);
				$position = $position[1];
				
				if(isset($cart[$position]['amount']))
					$cart[$position]['amount'] = $value;
					$product = Products::model()->findByPk($cart[$position]['product_id']);
					echo Shop::priceFormat(
							@$product->getPrice($cart[$position]['Variations'], $value));
					return Shop::setCartContent($cart);
			}	
		}*/

}


	// Add a new product to the shopping cart
	public function actionCreate()
	{
		if(Yii::app()->request->isAjaxRequest){
			$post['product_id']=$_POST['pid'];
			if(!is_numeric($_POST['amount']) || $_POST['amount'] <= 0) {
				$post['amount']=1;
			}else{
				$post['amount'] = $_POST['amount'];
			}
			$cart = Shop::getCartContent();
			
			$cart[] = $post;
			Shop::setCartcontent($cart);
			echo '1';
			Yii::app()->end();
		}else{
			if(!is_numeric($_POST['amount']) || $_POST['amount'] <= 0) {
				$_POST['amount']=1;
				/* Shop::setFlash(Shop::t('Illegal amount given'));
				$this->redirect(array( 
								'//products/view', 'id' => $_POST['product_id'])); */
			}
	
	
			$cart = Shop::getCartContent();
	
			// remove potential clutter
			if(isset($_POST['yt0']))
				unset($_POST['yt0']);
			if(isset($_POST['yt1']))
				unset($_POST['yt1']);
			
			$cart[] = $_POST;
			Shop::setCartcontent($cart);
			Yii::app()->user->setFlash('yiishop',Yii::t('main','The product has been added to the shopping cart'));
			
			$url = $this->createUrlProductDetail($_POST['product_id']);
			if(isset($_POST['paynow'])){
				$url = Yii::app()->createUrl('/shoppingCart/view');
			}
			$this->redirect($url);
		}
	}

	public function actionDelete($id)
	{
		$id = (int) $id;
		$cart = json_decode(Yii::app()->user->getState('cart'), true);

		unset($cart[$id]);
		Yii::app()->user->setState('cart', json_encode($cart));

			$this->redirect(array('//shoppingCart/view'));
	}

	public function actionIndex()
	{
		if(isset($_SESSION['cartowner'])) {
			$carts = ShoppingCart::model()->findAll('cartowner = :cartowner', array(':cartowner' => $_SESSION['cartowner']));

			$this->render('index',array( 'carts'=>$carts,));
		} 
	}

	public function actionAdmin()
	{
		$model=new ShoppingCart('search');
		if(isset($_GET['ShoppingCart']))
			$model->attributes=$_GET['ShoppingCart'];
			$model->cartowner = Yii::app()->User->getState('cartowner');

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	public function actionReload()
	{
		$products = Shop::getCartContent();
		$this->renderPartial('_cart', array(
				'products'=>$products
				), false, false);
	}
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=ShoppingCart::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='shopping cart-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
