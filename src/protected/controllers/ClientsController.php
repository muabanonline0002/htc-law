<?php
class ClientsController extends FrontendController{
	public $layout='1column';
	public function actionIndex()
	{
		$catId = 13;
		$criteria = new CDbCriteria;
		$criteria->condition = "catid=:id AND status=:s";
		$status = 1;
		$criteria->params = array(':id'=>$catId,':s'=>$status);
		$criteria->order="ordering DESC, id DESC";
		$data = GalleryItemsModel::model()->findAll($criteria);
		$this->render('index', compact('data'));
	}
	public function actionView()
	{
		$alias = Yii::app()->request->getParam('alias');
		$alias = str_replace('khach-hang', '', $alias);
		$criteria = new CDbCriteria;
		$criteria->condition = "alias=:alias AND status=:s";
		$status = 1;
		$criteria->params = array(':alias'=>$alias,':s'=>$status);
		$item = GalleryItemsModel::model()->find($criteria);
		if(!$item){
			Throw new HttpException('404','Not Found');
		}
		$this->render('view', compact('item'));
	}
}