var GuruCoreJs = {
	goHome: function() {
	    location.href = "/";
	},
	openSearch: function()
	{
		$('#txt-content').toggleClass('show');
	},
    addCart: function(id,amount){
        var self=this;
        $.ajax({
            url: "/shoppingCart/create.html",
            data: {pid:id,amount:amount},
            type: "post",
            success: function(result){
                if(result==1){
                    alert("Đã thêm sản phẩm vào giỏ hàng!")
                    self.reloadCart();
                }else{
                    alert("Số lượng không hợp lệ, mời Quý Khách thử lại!")
                }
            }
            
        })
    },
    reloadCart: function(){
        $.ajax({
            url: "/shoppingCart/reload.html",
            success: function(result){
                if(result){
                    $("#shopping-cart").html(result);
                }
            }
            
        })
    }
};
var Nav = {
    init: function() {
        var _this = this;

        $('.vg_mask').on('click', function() {
            _this.show()
            return false;
        });
        $('.icon_menu').on('click', function() {
            _this.show();
            return false;
        });
        $('.img-show-me').on('click', function(event){
        	event.preventDefault();
        	var h = $(this).attr('href');
        	$('#show-img').attr('src',h);
        })
        $('#ac-info').on('click', function(){
            $('#ac-info-content').toggleClass('show');
        })
        $('#ac-comment').on('click', function(){
            $('#ac-comment-content').toggleClass('show');
        })
    },
    show: function() {
        var nav = $('#vg_nav');
        if (nav.hasClass('none')) {
            nav.removeClass('none');
            var height_w = $(window).height();
            $('#vg_wrapper').addClass('slideshowing').css('height', $("#vg_nav").height() + 'px');
            $('.vg_mask').css('height', $("#vg_nav").height() + 'px');
            $(".vg_menu").css('min-height',height_w+'px')
        } else {
            this.hide();
        }
        return this;
    },
    hide: function() {
        $('#vg_nav').addClass('none');
        $('#vg_wrapper').removeClass('slideshowing').css('height', 'auto');
    }
};
var Popup = {
	title_popup: 'Thông báo',
	position_popup: 'fixed',
    closeHtml:'<a href="javascript:void(0)" class="popup_close">X</a>',
    init: function() {
        $('.popup_inline').on('click', function() {
        	var id = $(this).attr("rel");
        	$("#vg_popup").html($("#"+id).html())
	        Popup.show();
	        return false;
        });
        $('.ajax_popup').on('click', function() {
        	var l = $(this).attr("href");
        	Popup.ajax(l);
        	/*$.ajax({
        		url: l,
        		type: "post",
        		success: function(response){
        			$("#vg_popup").html(response)
        			Popup.show();
        		}
        	})
            return false;*/
        	return false;
        });
    },
    ajax: function(href){
    	$.ajax({
    		url: href,
    		type: "post",
    		success: function(response){
    			$("#vg_popup").html(response);
    			if(href.indexOf("getPopupPlaylist")>0){
    				Popup.position_popup='absolute';
    			}
    			Popup.show();
    		}
    	})
    	return false;
    },
    alert: function(content){
    	var html_alert=
    		'<div id="Popup">'
		    +	Popup.closeHtml
		    +		'<div id="popup_wr">'
		    +			'<div class="popup_title">'
		    +				'<span id="pop_title">'+Popup.title_popup+'</span>'
		    +			'</div>'
		    +			'<div class="popup_content">'
		    +			'<div style="padding: 10px">'
    		+			content
    		+			'</div>'
		    +			'</div>'
		    +		'</div>'
		    +	'</div>'
    	$("#vg_popup").html(html_alert)
        Popup.show();
    },
    close: function() {
        $('#vg_popup').addClass('none');
        $('#vg_popup').html("");
        $('#popup_mask').addClass('none');
        $("#vg_wrapper").css({
			height: 'auto'
		});
    },
    show: function() {
    	$('#vg_popup').removeClass('none');
        $('#popup_mask').removeClass('none');
    	var height_doc = $(document).height();
    	var height_w = $(window).height();
    	var height_popup = $("#vg_popup").height();
    	
    	var width_w = $(window).width();
    	var widthpopup_w = $("#vg_popup").width();
    	
    	var pos_x = (width_w-widthpopup_w)/2
    	var left = (width_w/2)-(widthpopup_w/2);
    	var top = (height_w/2)-(height_popup/2);
    	position = Popup.position_popup;
    	if(height_popup>height_w){
    		top=0;
    		position='absolute';
    		document.body.scrollTop=0;
    	}
    	var p_mask_h='1500';
    	if(height_popup<height_w){
    		var p_mask_h = height_doc;
    		if(height_doc<height_w){
        		p_mask_h = height_w;
        	}
    	}else if(height_popup>height_w){
    		var p_mask_h = height_popup+100;
    	}else{
    		var p_mask_h = height_doc;
    	}
    	
    	$("#vg_wrapper").css({
			height: p_mask_h+'px'
		});
    	//console.log('p_mask_h:'+p_mask_h);
    	 $('#popup_mask').css({
    		 height: p_mask_h+'px'
    	 });
    	 $('#vg_popup').css({
    		 top: top+'px',
    		 position: position,
    		 left: left+'px'
    	 });
    	 $('.popup_close').on('click', function() {
             Popup.close();
         });
    	 $('#popup_mask').on('click', function() {
             Popup.close();
         });
    }
};

$(document).ready(function(){
    Nav.init();
    Popup.init();
});
