<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="margin-top: 0px; transition-property: margin-top; transition-duration: 0.3s;">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="language" content="vi">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="MobileOptimized" content="100">
     
    <!-- <link rel="stylesheet" type="text/css" href="./css/idangerous.swiper.css">
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<link rel="stylesheet" type="text/css" href="./css/main.css"> -->
	<!-- <script type="text/javascript" src="./js/zepto.min.js"></script>
	<script type="text/javascript" src="./js/idangerous.swiper-2.1.min.js"></script>
	<script type="text/javascript" src="./js/common.js"></script> -->
	<title><?php echo Yii::app()->user->getState('SYS_SITE_NAME');?></title> 
    <meta name="title" content="<?php echo Yii::app()->user->getState('SYS_SITE_NAME');?>">
    <meta name="description" content="<?php echo Yii::app()->user->getState('SYS_META_DESCRIPTION');?>">
    <meta name="keywords" content="<?php echo Yii::app()->user->getState('SYS_META_KEYWORDS');?>">
    <link rel="canonical" href="<?php echo WAP_SITE_URL;?>">
    <?php 
        $url = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile("{$url}/css/idangerous.swiper.css");
        $cs->registerCssFile("{$url}/css/style.css");
        $cs->registerCssFile("{$url}/css/main.css");
        $cs->registerScriptFile("{$url}/js/zepto.min.js",CClientScript::POS_HEAD);
        $cs->registerScriptFile("{$url}/js/idangerous.swiper-2.1.min.js",CClientScript::POS_HEAD);
        $cs->registerScriptFile("{$url}/js/common.js",CClientScript::POS_HEAD);
    ?>
</head>