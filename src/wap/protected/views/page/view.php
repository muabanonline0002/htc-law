<div class="item-container fix">
    <div class="item-content">
        <div class="item-header-desc">
            <div class="item-header-title">
                <h2><?php echo $page->title;?></h2>
            </div>
        </div>
        <div class="item-conten-desc">
            <?php echo $page->fulltext;?>
        </div>
    </div>
</div>