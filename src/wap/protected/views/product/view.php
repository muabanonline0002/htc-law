<?php
$thumbMain = AvatarHelper::getThumbUrl($product->product_id, "products","sm");
$thumbMainLarge = AvatarHelper::getThumbUrl($product->product_id, "products","med");
$imagesMore = FrontendShopProductImages::model()->getImagesProduct($product->product_id);
$this->widget('common.widgets.common.TrackingWidget',array('cId'=>$product->product_id));
$urlProduct = Yii::app()->createAbsoluteUrl('/product/view', array(
        'category_key'=>$category->alias,
        'url_key'=>$product->alias,
        'id'=>$product->product_id));
?>
<div class="item-container fix">
    <div class="item-content">
        <div class="item-main-img">
            <div role="tabpanel" class="shop-tab-content tab-pane active" id="first">
                <div class="main-img">
                    <a class="large-img"  href="<?php echo $thumbMainLarge;?>"> 
                    <img id="show-img" src="<?php echo $thumbMainLarge;?>" alt=""></a>
                </div>
                <div class="more-img">
                    <?php 
                        if($imagesMore){
                            $i=1;
                            echo '<ul class="moreimages">';
                            foreach ($imagesMore as $value) {
                                $thumb = AvatarHelper::getThumbUrl($value->id, "productimages","sm");
                                $thumbLarge = AvatarHelper::getThumbUrl($value->id, "productimages","med");
                                ?>
                                <li role="tabpanel" class="shop-tab-content tab-pane" id="s<?php echo $i;?>">
                                    <a class="img-show-me" href="<?php echo $thumbLarge; ?>"> 
                                        <img width="60" id="large-img-<?php echo $i;?>" src="<?php echo $thumb;?>" alt="">
                                    </a>
                                </li>
                    <?php 
                            $i++;
                            }
                            echo '</ul>';
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="item-product-info">
            <h3 class="item-title"><?php echo $product->title;?></h3>
        </div>
        <div class="item-conten-desc">
            <ul class="accord">
                <li>
                    <h3 id="ac-info" class="accord-title">Thông tin sản phẩm<i class="icons icon-arrow-down"></i></h3>
                    <div id="ac-info-content" class="accord-content hide">
                        <?php echo empty($product->description_full)?'Đang cập nhật':$product->description_full;?>
                    </div>
                </li>
                <li><h3 id="ac-comment" class="accord-title">Bình Luận<i class="icons icon-arrow-down"></i></h3>
                    <div id="ac-comment-content" class="accord-content">
                        <div class="add-comment-area">
                            <div class="comment-title">
                                <div class="comment-form">
                                    <div class="fb-comments" data-href="<?php echo $urlProduct;?>" data-width="100%" data-numposts="5"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <?php $this->widget('application.widgets.shop.ProductListWidget', array('title'=>'Sản phẩm liên quan','type'=>'hot'));?>
</div>