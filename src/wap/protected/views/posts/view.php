<?php 
$url =Yii::app()->createAbsoluteUrl('/posts/view', array('url_key'=>$posts->alias, 'id'=>$posts->id));
$thumb = AvatarHelper::getThumbUrl($posts->id, "articles","med");
$cs = Yii::app()->clientScript;
$cs->registerMetaTag($url,null,null,array('property'=>'og:url'));
$cs->registerMetaTag('website',null,null,array('property'=>'og:type'));
$cs->registerMetaTag(CHtml::encode($posts->title).'|HANOIWINDOW',null,null,array('property'=>'og:title'));
$cs->registerMetaTag(CHtml::encode($posts->metadesc),null,null,array('property'=>'og:description'));
$cs->registerMetaTag($thumb,null,null,array('property'=>'og:image'));
?>
<div class="item-container fix">
    <div class="item-content">
        <div class="item-header-desc">
            <div class="item-header-title">
                <h2><?php echo $posts->title;?></h2>
            </div>
        </div>
        <div class="item-conten-desc">
            <div class="control">
                <?php $this->widget('application.widgets.Facebook.FacebookButton', array('url'=>$url));?>
            </div>
            <?php echo $posts->fulltext;?>
            <div class="add-comment-area">
                <div class="comment-title">
                    <h2>Bình Luận</h2>
                    <div class="comment-form">
                        <div class="fb-comments" data-href="<?php echo $url;?>" data-width="100%" data-numposts="5"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>