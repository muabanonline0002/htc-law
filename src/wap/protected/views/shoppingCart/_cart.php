<div id="shopping-cart-content">
<?php if($products):
$total = 0;
foreach ($products as $key => $value){
	$total +=$value['amount'];
}
$price_total = 0;
$tax_total = 0;
foreach(Shop::getCartContent() as $product)  {
	$model = FrontendShopProductsModel::model()->findByPk($product['product_id']);
	$price_total += $model->getPrice(@$product['Variations'], @$product['amount']);
	$tax_total += $model->getTaxRate(@$product['Variations'], @$product['amount']);

}

if($shipping_method = Shop::getShippingMethod())
	$price_total += $shipping_method->price;
?>
<table>
<tr><td>Sản phẩm</td><td><?php echo $total;?></td></tr>
<tr><td>Thành tiền</td><td><?php echo Shop::priceFormat($price_total);?></td></tr>
</table>
<?php endif;?>
</div>
<div id="shopping-cart-footer"><a href="<?php echo Yii::app()->createUrl('/shoppingCart/view');?>">Xem chi tiết</a></div>
