<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return CMap::mergeArray(
	require_once dirname(__FILE__).'../../../../common/config/common.php',
	array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Hồng Sâm Hàn Quốc',
	'theme'=>'default',
	// preloading 'log' component
	'preload'=>array('log'),
	'language'=>'vi',
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.portlets.*',
		'application.helpers.*',
		'application.widgets.GLinkPager',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
	),

	// application components
	'components'=>array(
        'cache' => array(
            'class' => 'system.caching.CFileCache'
        ),
        'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			/* 'rules'=>array(
				''=>'site/index',
				'<action:(login|logout|about|index)>' => 'site/<action>',
				'<controller:(software|pricing|casestudies|testimonials|blog)>' => '<controller>/index',
				'<alias>'=>'site/page',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			), */
			'rules'=>array(
				'home'=>'site/index',
				'<action:(login|logout|about|index)>' => 'site/<action>',
				'tin-tuc-su-kien' => 'posts/index',
				'bao-gia' => 'quote/index',
				'bao-gia/<url_key:[a-zA-Z0-9-]+>,<id:\w+>' => 'quote/view',
				'gio-hang'=>'shoppingCart/view',
				'lien-he' => 'site/contact',
				'cong-trinh-da-thi-cong' => 'page/porfolio',
				'san-pham' => 'product',
				'san-pham/<url_key:[a-zA-Z0-9-]+>' => 'product/type',
				'san-pham/<url_key:[a-zA-Z0-9-]+>,<id:\w+>' => 'product/list',
				'san-pham/<category_key:[a-zA-Z0-9-]+>/<url_key:[a-zA-Z0-9-]+>,<id:\w+>' => 'product/view',
                '<url_key_page:[a-zA-Z0-9-]+>' => 'page/view',
                '<_c:\w+>/<url_key:[a-zA-Z0-9-]+>,<id:\w+>' => '<_c>/view',
                '<_c:\w+>' => '<_c>/index',
				'<_c:\w+>/<_a:\w+>/<id:\w+>' => '<_c>/<_a>',
				'<_c:\w+>/<_a:\w+>' => '<_c>/<_a>',
			),
			'urlSuffix'		=>	'.html',
			'showScriptName'=>false,
		),
		
		// uncomment the following to use a MySQL database
		/* 'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		), */
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning, traces',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
		'postsPerPage'=>15,
		'product_list_category'=>10,
		'tagCloudCount'=>20,
		'currencySymbol'=>'đ',
		'local_mode'=>0
	),
	)
);