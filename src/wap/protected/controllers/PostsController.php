<?php
class PostsController extends WapController{
	public $layout='body';
	public function actionIndex()
	{
		$newsId = 9;
		
		$total = WebArticleModel::model()->getCountArticlesCat($newsId);
		$paging = new CPagination($total);
		$itemOnPaging = 5;
		$limit = Yii::app()->params['postsPerPage'];
		$paging->pageSize = $limit;

		$data = WebArticleModel::model()->getArticlesCat($newsId,$limit,$paging->getOffset());
		$this->render('index', compact('data','paging','total','limit'));
	}
	public function actionView()
	{
		$id = Yii::app()->request->getParam('id');
		$posts = WebArticleModel::model()->findByPk($id);
		$this->render('view', compact('posts'));
	}
}