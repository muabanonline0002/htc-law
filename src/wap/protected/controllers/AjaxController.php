<?php
class AjaxController extends CController{
	public $layout=false;
	public function actionAddViews()
	{
		if(Yii::app()->request->isAjaxRequest){
			$cId = Yii::app()->request->getParam('cid');
			$model = ShopProductStatisticModel::model()->findByPk($cId);
			if($model){
				$model->views +=1;
				$model->updated_datetime = date('Y-m-d H:i:s');
				$res = $model->save(false); 
			}else{
				$model = new ShopProductStatisticModel();
				$model->product_id=$cId;
				$model->views +=1;
				$model->updated_datetime = date('Y-m-d H:i:s');
				$res = $model->save(false); 
			}
		}
	}
}