<?php
class PageController extends WapController{
	public $layout='body';
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionView()
	{
		$urlKey = Yii::app()->request->getParam('url_key_page');
		$page = FrontendPagesModel::model()->findByAttributes(array('alias'=>$urlKey));
		if(!$page){
			throw new CHttpException(404);
		}
		$this->pageTitle = $page->title;
		$this->render('view', compact('page'));
	}
}