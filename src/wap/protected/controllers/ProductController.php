<?php
class ProductController extends WapController
{
	public $layout='body';
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionView()
	{
		$id = Yii::app()->request->getParam('id');
		$product = ShopProductsModel::model()->findByPk($id);
		$catId = $product->category_id;
		$category = WebShopCategoryModel::model()->findByPk($catId);
		$this->render('view', compact('product','category'));
	}
	public function actionList()
	{
		$catId = Yii::app()->request->getParam('id');
		$total = FrontendShopProductsModel::model()->getCountProductsByCatId($catId);
		$paging = new CPagination($total);
		$itemOnPaging = 4;
		$limit = Yii::app()->params['product_list_category'];
		$paging->pageSize = $limit;
		$data = FrontendShopProductsModel::model()->getProductsByCatId($catId, $limit, $paging->getOffset());
		$category = WebShopCategoryModel::model()->findByPk($catId);
		$this->render('list', compact('data','total','paging','limit','category'));
	}
	public function actionType()
	{
		$url_key = Yii::app()->request->getParam('url_key');
		switch ($url_key) {
			case 'qua-bieu-cao-cap':
				$type='premium';
				$title='Quà Biếu Cao Cấp';
				break;
			case 'san-pham-moi':
				$type='new';
				$title='Sản phẩm mới';
				break;
			case 'san-pham-ban-chay':
				$type='hot';
				$title='Sản phẩm bán chạy';
				break;
			case 'san-pham-dang-giam-gia':
				$type='saleoff';
				$title='Sản phẩm đang giảm giá';
				break;
			default:
				$type='new';
				$title='Sản phẩm mới';
				break;
		}
		$total = FrontendShopProductsModel::model()->getCountProductsByType($type);
		$paging = new CPagination($total);
		$itemOnPaging = 5;
		$limit = Yii::app()->params['product_list_category'];
		$paging->pageSize = $limit;
		$data = FrontendShopProductsModel::model()->getProductsByType($type,$limit, $paging->getOffset());
		
		$this->render('type',compact('data','paging','limit','total','title'));
	}
}