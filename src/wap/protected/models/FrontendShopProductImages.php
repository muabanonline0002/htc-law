<?php

Yii::import('common.models.db.ShopProductImages');

class FrontendShopProductImages extends ShopProductImages
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function getImagesProduct($productId)
	{
		$crit = new CDbCriteria();
		$crit->condition = 'product_id=:id';
		$crit->params = array(':id'=>$productId);
		return self::model()->findAll($crit);
	}
}