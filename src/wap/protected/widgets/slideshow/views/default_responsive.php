  <style>
.device {
  width: 100%;
  position: relative;
	height: 235px;
}
.swiper-container {
  width: 100%;
  color: #fff;
  text-align: center;
	overflow: hidden;
}
.swiper-slide .sw-res {
  margin-bottom: 0;
	position: relative;
}
.swiper-slide .sw-res img{
	width: 100%;
	display: block;
}
.pagination {
  position: absolute;
  left: 0;
  text-align: right;
  bottom:10px;
  width: 100%;
	z-index: 9;
	padding-right: 5px;
}
.swiper-pagination-switch {
 	display: inline-block;
  width: 10px;
  height: 10px;
  border-radius: 10px;
  background: #fff;
  box-shadow: 0px 1px 2px #555 inset;
  margin: 0 3px;
  cursor: pointer;
}
.swiper-visible-switch {
  background: #0198ff;
}
.swiper-active-switch {
  background: #0198ff;
}
.swiper-slide .info-nav{
	position: absolute;
	bottom: 10px;
	left: 10px;
}
</style>
<div class="device">
    <div class="swiper-container">
      <div class="swiper-wrapper">
      	<?php 
      	$i=0;
      	foreach ($data as $value){?>
        <div class="swiper-slide">
        	<div class="sw-res">
	        	<img width="100%" id="img_<?php echo $i;?>" src="<?php echo $value['avatar']?>">
	        	<a href="<?php echo $value['link']?>">
	        	<div class="info-nav">
		        	<p class="title"><?php echo CHtml::encode($value['title']);?></p>
		        	<!-- <p class="artist">Slide 1</p>-->
	        	</div>
	        	</a>
        	</div>
        </div>
        <?php 
$i++;
}?>
      </div>
    </div>
    <div class="pagination"></div>
  </div>
  <script>
  var mySwiper = new Swiper('.swiper-container',{
    pagination: '.pagination',
    loop:true,
    grabCursor: true,
    paginationClickable: false
  })
  </script>