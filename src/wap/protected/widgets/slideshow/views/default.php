  <style>
/* Demo Syles */
.device {
  width: 100%;
  position: relative;
}
.device .arrow-left {
  background: url(img/arrows.png) no-repeat left top;
  position: absolute;
  left: 10px;
  top: 50%;
  margin-top: -15px;
  width: 17px;
  height: 30px;
}
.device .arrow-right {
  background: url(img/arrows.png) no-repeat left bottom;
  position: absolute;
  right: 10px;
  top: 50%;
  margin-top: -15px;
  width: 17px;
  height: 30px;
}
.swiper-container {
  max-height: 235px;
  width: 100%;
}
.content-slide {
  padding: 20px;
  color: #fff;
}
.title {
  font-size: 25px;
  margin-bottom: 10px;
}
.pagination {
  position: absolute;
  left: 0;
  text-align: right;
  bottom:15px;
  width: 100%;
	z-index: 9;
	padding-right: 5px;
}
.swiper-pagination-switch {
  display: inline-block;
  width: 10px;
  height: 10px;
  border-radius: 10px;
  background: #fff;
  box-shadow: 0px 1px 2px #555 inset;
  margin: 0 3px;
  cursor: pointer;
}
.swiper-active-switch {
  background: #0198ff;
}
.swiper-slide{
	position: relative;
}
.swiper-slide .info-nav{
	position: absolute;
	bottom: 10px;
	left: 10px;
}
  </style>
<div class="device">
    <div class="swiper-container">
      <div class="swiper-wrapper">
      	<?php foreach ($data as $value){?>
        <div class="swiper-slide">
       	 	<a href="<?php echo $value['link']?>">
        	<img src="<?php echo $value['avatar']?>">
        	<div class="info-nav">
	        	<p class="title"><?php echo $value['title']?></p>
	        	<!-- <p class="artist">Slide 1</p>-->
        	</div>
        	</a>
        </div>
        <?php }?>
      </div>
    </div>
    <div class="pagination"></div>
  </div>
  <script>
  var mySwiper = new Swiper('.swiper-container',{
    pagination: '.pagination',
    loop:true,
    grabCursor: true,
    paginationClickable: false
  })
  $('.arrow-left').on('click', function(e){
    e.preventDefault()
    mySwiper.swipePrev()
  })
  $('.arrow-right').on('click', function(e){
    e.preventDefault()
    mySwiper.swipeNext()
  })
  </script>