<?php if($data){?>
<div class="item-box" id="xwrr-album">	
    <?php if($linkTitle!=''){?>
    <a href="<?php echo $linkTitle;?>">
    <h3 class="item-box-title"><div><?php echo $title;?></div></h3>
    </a>
    <?php }else{?>
    <h3 class="item-box-title-raw"><?php echo $title;?></h3>
    <?php }?>
	<div class="item-content">
		<ul class="items-list">
		<?php 
            $i=0;
        foreach($data as $product){
            
            $c = ($i%2==0)?'isnew':'ishot';
            $category = ShopCategoryModel::model()->findByPk($product->category_id);
            $link = Yii::app()->createUrl('/product/view', array(
                'category_key'=>$category->alias,
                'url_key'=>$product->alias, 'id'=>$product->product_id));
            $title = $product->title;
            $price = Shop::priceFormat($product->price);
            $thumb = AvatarHelper::getThumbUrl($product->product_id, "products","sm");
        ?>
		 	<li class="item">
	 			<a href="<?php echo $link;?>">
		 			<div class="item-img"><img src="<?php echo $thumb;?>" /></div>
		 			<h3><?php echo $title;?></h3>
                    <div><span class="btn btn-small btn-green">Chi tiết</span></div>
        		</a>
		    </li>
		<?php }?>
		</ul>
	</div>
</div>
<?php }?>