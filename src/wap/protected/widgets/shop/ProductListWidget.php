<?php
class ProductListWidget extends CWidget
{
	public $_url = NULL;
	public $title = NULL;
	public $type=null;
	public $data;
	public $id;
	public function init()
	{
		parent::init();
	}
	public function run()
	{
		$dependency = new CDbCacheDependency('SELECT MAX(updated_datetime) FROM shop_products');
		$cacheId='WAP_ProductListWidget'.$this->id.$this->type;
		if($this->beginCache($cacheId,array('duration'=>Yii::app()->params['cache_time'],'dependency'=>$dependency))){
			$title = $this->title;
			switch ($this->type) {
				case 'new':
					$data = FrontendShopProductsModel::model()->getProductsByType($this->type,5);
					$url_key = 'san-pham-moi';
					break;
				case 'hot':
					$data = FrontendShopProductsModel::model()->getProductsByType($this->type,5);
					$url_key = 'san-pham-hot';
					break;
				case 'saleoff':
					$data = FrontendShopProductsModel::model()->getProductsByType($this->type,5);
					$url_key = 'san-pham-dang-giam-gia';
					break;
				case 'category':
					$catId = $this->id;
					$cat = ShopCategoryModel::model()->findByPk($catId);
					$url_key=$cat->alias;
					$title=$cat->title;
					$data = FrontendShopProductsModel::getProductByCat($catId,5,0);
					break;
				default:
					$data = $this->data;
					$url_key='';
					break;
			}
			if(!empty($url_key)){
				$linkTitle = Yii::app()->createUrl('/product/type', array('url_key'=>$url_key));
			}else{
				$linkTitle='';
			}
			
			$this->render('product_list', compact('data','title','linkTitle'));
		$this->endCache();
		}
	}
}